/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq

import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.exceptions.MessageParsingException
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.synaptix.core.GenericService
import com.rabbitmq.client._
import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 04/03/2021
  */

trait RabbitMQConsumer extends Consumer with GenericService {

  implicit val client: RabbitMQClient
  implicit val conn: Connection
  implicit val channel: Channel

  var cTag: String = _
  var queueName: String = _

  override def init(): Unit = {
    logger.debug("Queue binding...")
    // queueName = channel.queueDeclare(serviceName, durable, exclusive, autoDelete, args.asJava).getQueue
    // channel.queuePurge(serviceName)
    queueName = channel.queueDeclare().getQueue
    channel.basicQos(AmqpClientConfiguration.prefetchCount) // limits the number of unacknowledged messages revceived at once (0 for unlimited)
  }

  override def shutdown(): Unit = {
    channel.basicCancel(cTag)
    channel.close()
  }

  override def handleConsumeOk(consumerTag: String): Unit = {}

  override def handleCancelOk(consumerTag: String): Unit ={}

  override def handleCancel(consumerTag: String): Unit = {}

  override def handleShutdownSignal(consumerTag: String, sig: ShutdownSignalException): Unit = {}

  override def handleRecoverOk(consumerTag: String): Unit = {}

  override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
    val validation = Json.parse(body).validate[AmqpMessage]
    if (validation.isSuccess) {
      val message = validation.get
      onMessage(message, consumerTag, envelope, properties)
    }
    else onError(body, consumerTag, envelope, properties)
  }

  def start() = consume(true)

  def consume(autoAck: Boolean = true) = {
    logger.debug("Consuming...")
    cTag = channel.basicConsume(queueName, autoAck, this)
  }

  def onMessage(message:AmqpMessage, consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties)

  def onError(body: Array[Byte], consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties) = {
    throw MessageParsingException(s"Unable to parse message : $body")
  }
}
