/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api

import akka.Done
import akka.actor.ActorSystem
import akka.stream.KillSwitches
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.amqp.api.exceptions.ConnectionClosedException
import com.mnemotix.amqp.api.models.{AmqpMessage, MessageProcessResult}
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.synaptix.core.SynaptixException
import com.rabbitmq.client._
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsNumber, JsString, JsValue, Json}

import java.util.UUID
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 08/03/2021
  */

trait SynaptixRPCTask[T] extends LazyLogging {

  implicit val client: RabbitMQClient

  val topic: String
  val exchangeName: String
  val sender: String = UUID.randomUUID().toString
  val serviceName: String = s"$exchangeName.$topic.task"
  var queueName: String = _

  val durable = AmqpClientConfiguration.durable
  val args: Map[String, AnyRef] = Map.empty
  val exclusive: Boolean = true
  val autoDelete: Boolean = false
  val noReply: Boolean = false

  var isVerbose:Boolean=true

  def unmute() = isVerbose=true
  def mute() = isVerbose=false

  var connection: Option[Connection] = None
  var channel: Option[Channel] = None
  var cTag: String = _

//  def connect(): Connection = Try(client.getConnection()) match {
//    case Success(conn: Connection) => {
//      logger.info("Connection successful.")
//      conn
//    }
//    case Failure(err) => {
//      logger.error("No connection available. Retrying to connect in 5 sec...", err)
//      Thread.sleep(5000)
//      connect
//    }
//  }

  def createChannel(conn: Connection): Channel = Try(conn.createChannel()) match {
    case Success(chan: Channel) => {
      logger.info("Channel creation success.")
      chan
    }
    case Failure(err) => {
      logger.error("Channel creation failure. Retrying to connect in 5 sec...", err)
      Thread.sleep(5000)
      createChannel(conn)
    }
  }

  def consumer()(implicit ec: ExecutionContext): Consumer = new Consumer {
    override def handleConsumeOk(consumerTag: String): Unit = {}

    override def handleCancelOk(consumerTag: String): Unit = {}

    override def handleCancel(consumerTag: String): Unit = {}

    override def handleShutdownSignal(consumerTag: String, sig: ShutdownSignalException): Unit = {}

    override def handleRecoverOk(consumerTag: String): Unit = {}

    override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
      val replyProps: AMQP.BasicProperties = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).replyTo(properties.getReplyTo).build()
      val validation = Json.parse(body).validate[AmqpMessage]
      if (validation.isSuccess) {
        val message = validation.get.copy(correlationId = Some(replyProps.getCorrelationId()), replyTo=Some(replyProps.getReplyTo()))
        val context = beforeProcess(message)
        val process = processMessage(message, context)
        process.onComplete {
          case Success(result: MessageProcessResult) => {
            if (!noReply) {
              val response = getResponseMessage(result.content, result.format, result.status).copy(correlationId = Some(replyProps.getCorrelationId()), replyTo=Some(replyProps.getReplyTo()))
              logger.info(Json.prettyPrint(response.toJson()))
              logger.debug(s"CONSUMER : CorrelationId : ${replyProps.getCorrelationId()}")
              logger.debug(s"CONSUMER : ReplyTo : ${replyProps.getReplyTo()}")
              // send response to reply queue with proper correlationId
              channel.get.basicPublish("", properties.getReplyTo(), replyProps, response.toString.getBytes("UTF-8"))
            }
            // acknowledge broker that message was correctly processed
            channel.get.basicAck(envelope.getDeliveryTag, false)
          }
          case Failure(error) => {
            if (!noReply) {
              val response = getErrorMessage(error)
              // send response to reply queue with proper correlationId
              channel.get.basicPublish("", properties.getReplyTo, replyProps, response.toString.getBytes("UTF-8"))
            }
            // acknowledge broker that message was correctly processed
            channel.get.basicAck(envelope.getDeliveryTag, false)
          }
        }
      }
    }
  }

  /* ------------------------
      TASK RUNNING
  ------------------------ */

  var runnableGraph: Option[Future[Done]] = None
  val killSwitch = KillSwitches.shared(s"$serviceName-kill-switch")

  def init()(implicit conn:Option[Connection], ec: ExecutionContext) = {
//    logger.debug("Connecting to broker...")
//    connection = Some(conn)
    logger.debug("Channel creation...")
    channel = Some(createChannel(conn.get))
    logger.debug("Queue binding...")
    channel.get.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, durable)
    // queueName = channel.queueDeclare(serviceName, durable, exclusive, autoDelete, args.asJava).getQueue
    // channel.queuePurge(serviceName)
    queueName = channel.get.queueDeclare().getQueue
    channel.get.queueBind(queueName, exchangeName, topic)
    channel.get.basicQos(AmqpClientConfiguration.prefetchCount) // limits the number of unacknowledged messages revceived at once (0 for unlimited)
    logger.debug(s"Queue binding OK with [${AmqpClientConfiguration.prefetchCount}] prefetch count.")
    logger.debug("Consuming...")
    cTag = channel.get.basicConsume(queueName, false, consumer())
  }

  def start()(implicit conn:Option[Connection], system: ActorSystem, ec: ExecutionContext): Future[Done] = {
    init()
    // We create a parallel monitoring thread to make the consuming blocking and being able ton control it in an akka stream environment
//    runnableGraph = Some(Source.repeat(true).via(killSwitch.flow).via(Flow[Boolean].map { _ => monitor() }).runWith(Sink.ignore))
    runnableGraph = Some(Source.tick(
      1.second, // delay of first tick
      5.second, // delay of subsequent ticks
      true // element emitted each tick
    ).via(killSwitch.flow).via(Flow[Boolean].map { _ => monitor() }).runWith(Sink.ignore))
    runnableGraph.get
  }

  def monitor()(implicit ec: ExecutionContext) = {
    if (connection.isDefined && !connection.get.isOpen) {
      logger.error("Connection was closed.")
      connection = None
      channel = None
      throw ConnectionClosedException("Connection to broker was closed.")
//      init()
    }
  }

  def shutdown() = {
    // First kill the monitoring thread
    if (runnableGraph.isDefined) {
      killSwitch.shutdown() // send termination signal
      Await.result(runnableGraph.get, Duration.Inf) // await the last consume iteration
    }
    close() // release connection and channel
  }

  def close() = {
    if(channel.isDefined) {
      channel.get.basicCancel(cTag)
      channel.get.close()
    }
    if(connection.isDefined) connection.get.close()
  }

  @throws(classOf[SynaptixException])
  def beforeProcess(msg: AmqpMessage): Option[T] = None

  @throws(classOf[SynaptixException])
  def processMessage(msg: AmqpMessage, context: Option[T] = None): Future[MessageProcessResult]

  @throws(classOf[SynaptixException])
  def afterProcess(context: Option[T] = None) = {}

  def getResponseMessage(body: JsValue, format: String, status: String): AmqpMessage = {
    val head = Map(
      "command" -> JsString(topic),
      "sender" -> JsString(sender),
      "timestamp" -> JsNumber(System.currentTimeMillis()),
      "format" -> JsString(format),
      "status" -> JsString(status)
    )
    AmqpMessage(head, body)
  }

  def getErrorMessage(err: Throwable): AmqpMessage = {
    val body = Json.obj(
      "errorClass" -> JsString(err.getClass.getSimpleName),
      "errorMessage" -> JsString(err.getMessage)
    )
    getResponseMessage(body, "JSON", "ERROR")
  }

}
