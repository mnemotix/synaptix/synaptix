/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.amqp.api.models

import play.api.libs.json.{Format, JsObject, JsValue, Json}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-01-31
  */

case class AmqpMessage(headers: Map[String, JsValue], body: JsValue, correlationId: Option[String]=None, replyTo: Option[String]=None) {

  override def toString: String = Json.stringify(toJson())

  def toJson(): JsObject = Json.obj(
    "headers" -> Json.toJson(headers),
    "body" -> body,
    "correlationId" -> correlationId.fold("")(identity),
    "replyTo" -> replyTo.fold("")(identity)
  )
}

object AmqpMessage {

  implicit val format: Format[AmqpMessage] = Json.format[AmqpMessage]

  def apply(s: String): AmqpMessage = parse(s)

  def parse(s: String): AmqpMessage = Json.parse(s).as[AmqpMessage]

//  def apply(im: ReadResult): AmqpMessage = {
//    val parsing = Json.parse(im.bytes.utf8String).validate[AmqpMessage]
//    if (parsing.isSuccess) parsing.get
//    else throw MessageParsingException("Unable to parse the message")
//  }

}
