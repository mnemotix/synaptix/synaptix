/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq.restapi

import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.rabbitmq.restapi.models._
import com.mnemotix.synaptix.core.utils.StringUtils
import com.softwaremill.sttp._
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 13/03/2021
  */

class RabbitMQRestApiClient extends LazyLogging {

  implicit val backend = HttpURLConnectionBackend()

  lazy val accessPointUri: String = s"http://${AmqpClientConfiguration.host}:15672/api"

  def listExchanges():Seq[Exchange] = {
    try{
      val uri = if(AmqpClientConfiguration.vhost == "/") s"$accessPointUri/exchanges" else StringUtils.removeTrailingSlashes(s"$accessPointUri/exchanges/${AmqpClientConfiguration.vhost}")
      val request = sttp.auth.basic(AmqpClientConfiguration.user, AmqpClientConfiguration.pwd).headers(Map("Accept" -> "application/json")).get(uri"$uri")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RabbitMQListExchangesException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get).as[Seq[Exchange]]
      }
    }
    catch {
      case t:Throwable => throw RabbitMQListExchangesException(t.getMessage, Some(t.getCause))
    }
  }

  def listConnections() = {
    try{
      val uri = if(AmqpClientConfiguration.vhost == "/") s"$accessPointUri/connections" else s"$accessPointUri/vhosts/${AmqpClientConfiguration.vhost}/connections"
      val request = sttp.auth.basic(AmqpClientConfiguration.user, AmqpClientConfiguration.pwd).headers(Map("Accept" -> "application/json")).get(uri"$uri")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RabbitMQListConnectionsException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get)
      }
    }
    catch {
      case t:Throwable => throw RabbitMQListConnectionsException(t.getMessage, Some(t.getCause))
    }
  }

  def listChannels() = {
    try{
      val uri = if(AmqpClientConfiguration.vhost == "/") s"$accessPointUri/channels" else s"$accessPointUri/vhosts/${AmqpClientConfiguration.vhost}/channels"
      val request = sttp.auth.basic(AmqpClientConfiguration.user, AmqpClientConfiguration.pwd).headers(Map("Accept" -> "application/json")).get(uri"$uri")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RabbitMQListChannelsException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get)
      }
    }
    catch {
      case t:Throwable => throw RabbitMQListChannelsException(t.getMessage, Some(t.getCause))
    }
  }

  def listConsumers() = {
    try{
      val uri = if(AmqpClientConfiguration.vhost == "/") s"$accessPointUri/consumers" else s"$accessPointUri/vhosts/${AmqpClientConfiguration.vhost}/consumers"
      val request = sttp.auth.basic(AmqpClientConfiguration.user, AmqpClientConfiguration.pwd).headers(Map("Accept" -> "application/json")).get(uri"$uri")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RabbitMQListConsumersException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get)
      }
    }
    catch {
      case t:Throwable => throw RabbitMQListConsumersException(t.getMessage, Some(t.getCause))
    }
  }

  def listQueues() = {
    try{
      val uri = if(AmqpClientConfiguration.vhost == "/") s"$accessPointUri/queues" else s"$accessPointUri/vhosts/${AmqpClientConfiguration.vhost}/queues"
      val request = sttp.auth.basic(AmqpClientConfiguration.user, AmqpClientConfiguration.pwd).headers(Map("Accept" -> "application/json")).get(uri"$uri")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RabbitMQListQueuesException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get)
      }
    }
    catch {
      case t:Throwable => throw RabbitMQListQueuesException(t.getMessage, Some(t.getCause))
    }
  }

  def listBindings() = {
    try{
      val uri = if(AmqpClientConfiguration.vhost == "/") s"$accessPointUri/bindings" else s"$accessPointUri/vhosts/${AmqpClientConfiguration.vhost}/bindings"
      val request = sttp.auth.basic(AmqpClientConfiguration.user, AmqpClientConfiguration.pwd).headers(Map("Accept" -> "application/json")).get(uri"$uri")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RabbitMQListBindingsException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get)
      }
    }
    catch {
      case t:Throwable => throw RabbitMQListBindingsException(t.getMessage, Some(t.getCause))
    }
  }
}
