/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq

import akka.stream.Materializer
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.models.AmqpMessage
import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.{AMQP, Channel, Connection, Envelope}

import java.util.UUID
import scala.concurrent.ExecutionContext

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 04/03/2021
  */

abstract class RabbitMQRPCCall(val topic: String, val exchangeName: String)(implicit override val client: RabbitMQClient, override val conn: Connection, override val channel: Channel) extends RabbitMQConsumer with RabbitMQProducer {

  val serviceName: String = s"$exchangeName.$topic.call"
  var replyQueue: String = _
  val corrId = UUID.randomUUID.toString

  //  override def handleDelivery(consumerTag: String, envelope: Envelope, properties: BasicProperties, body: Array[Byte]): Unit = {
  //    if (isVerbose) logger.debug(s"Task [$serviceName] handleDelivery ${consumerTag}")
  //    val routingKey = envelope.getRoutingKey
  //    val contentType = properties.getContentType
  //    val deliveryTag = envelope.getDeliveryTag
  //    val correlationId = properties.getCorrelationId
  //    logger.info(routingKey)
  //    logger.info(contentType)
  //    logger.info(deliveryTag.toString)
  //    logger.info(correlationId)
  ////    onMessage(Json.parse(body).as[AmqpMessage]).onComplete{
  ////      case Success(result:MessageProcessResult) => {
  ////        if (isVerbose) logger.debug(s"Message processed : ${result.content}, ${result.format}, ${result.status}")
  ////      }
  ////      case Failure(error) => {
  ////        if (isVerbose) logger.error(s"An error occured during the message processing", error)
  ////      }
  ////    }
  //  }
  //
  //  override def onMessage(message: AmqpMessage): Future[MessageProcessResult] = {
  //    try{
  //      onReply(message)
  //      Future(OkMessage(JsString("OK")))
  //    }
  //    catch{
  //      case t:Throwable =>Future(ErrorMessage(t))
  //    }
  //  }

  override def init(): Unit = {
    logger.debug("Queue binding...")
    // queueName = channel.queueDeclare(serviceName, durable, exclusive, autoDelete, args.asJava).getQueue
    // channel.queuePurge(serviceName)
    queueName = channel.queueDeclare().getQueue
    replyQueue = queueName
    channel.basicQos(AmqpClientConfiguration.prefetchCount) // limits the number of unacknowledged messages revceived at once (0 for unlimited)
  }

  def onReply(message: AmqpMessage)

  def call(messages: Vector[AmqpMessage])(implicit ec: ExecutionContext, mat: Materializer) = {
    if (isVerbose) logger.debug(s" [$serviceName] publishing messages : ${messages.mkString("\n")}")
    val props = new BasicProperties.Builder().correlationId(corrId).replyTo(replyQueue).build
    publish(topic, exchangeName, messages, props)
  }

  override def onMessage(message: AmqpMessage, consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties): Unit = {}

  override def consume(autoAck: Boolean = true) = {
    logger.debug("Consuming...")
    cTag = channel.basicConsume(replyQueue, autoAck, this)
  }

}