/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq.restapi.models

import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 13/03/2021
  */

case class Exchange(name: String, `type`: String, vhost: String, durable: Boolean, auto_delete: Boolean, internal: Boolean)

object Exchange {
  implicit lazy val format = Json.format[Exchange]
}

/*
{
  "auth_mechanism" : "PLAIN",
  "channel_max" : 2047,
  "channels" : 1,
  "client_properties" : {
    "capabilities" : {
      "authentication_failure_close" : true,
      "basic.nack" : true,
      "connection.blocked" : true,
      "consumer_cancel_notify" : true,
      "exchange_exchange_bindings" : true,
      "publisher_confirms" : true
    },
    "copyright" : "Copyright (c) 2007-2021 VMware, Inc. or its affiliates.",
    "information" : "Licensed under the MPL. See https://www.rabbitmq.com/",
    "platform" : "Java",
    "product" : "RabbitMQ",
    "version" : "5.11.0"
  },
  "connected_at" : 1615633083580,
  "frame_max" : 131072,
  "garbage_collection" : {
    "fullsweep_after" : 65535,
    "max_heap_size" : 0,
    "min_bin_vheap_size" : 46422,
    "min_heap_size" : 233,
    "minor_gcs" : 12
  },
  "host" : "192.168.0.2",
  "name" : "192.168.0.1:59174 -> 192.168.0.2:5672",
  "node" : "rabbit@d61aeedeca68",
  "peer_cert_issuer" : null,
  "peer_cert_subject" : null,
  "peer_cert_validity" : null,
  "peer_host" : "192.168.0.1",
  "peer_port" : 59174,
  "port" : 5672,
  "protocol" : "AMQP 0-9-1",
  "recv_cnt" : 13,
  "recv_oct" : 686,
  "recv_oct_details" : {
    "rate" : 1.6
  },
  "reductions" : 15154,
  "reductions_details" : {
    "rate" : 193.8
  },
  "send_cnt" : 11,
  "send_oct" : 698,
  "send_oct_details" : {
    "rate" : 1.6
  },
  "send_pend" : 0,
  "ssl" : false,
  "ssl_cipher" : null,
  "ssl_hash" : null,
  "ssl_key_exchange" : null,
  "ssl_protocol" : null,
  "state" : "running",
  "timeout" : 20,
  "type" : "network",
  "user" : "admin",
  "user_who_performed_action" : "admin",
  "vhost" : "/"
}
 */
case class Connection()

object Connection {

}