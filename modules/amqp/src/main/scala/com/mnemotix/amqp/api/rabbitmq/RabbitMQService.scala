/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq

import akka.Done
import akka.stream.KillSwitches
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.synaptix.core.GenericService
import com.rabbitmq.client._

import java.util.UUID
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 04/03/2021
  */

abstract class RabbitMQService(val topic: String, val exchangeName: String)(implicit val client: RabbitMQClient) extends GenericService {

  val sender: String = UUID.randomUUID().toString
  val serviceName: String
  val durable = AmqpClientConfiguration.durable
  val args: Map[String, AnyRef] = Map.empty
  val exclusive: Boolean = true
  val autoDelete: Boolean = true

  var connection: Option[Connection] = None
  var channel: Option[Channel] = None
  var thread: Option[Future[Done]] = None

  val killSwitch = KillSwitches.shared(s"$serviceName-kill-switch")

  lazy val queueName: String = queueDeclare()

  def queueBind() = client.queueBind(queueName, exchangeName, topic)(channel.get)

  def queueDeclare(): String = client.queueDeclare(serviceName, durable, exclusive, autoDelete, args)(channel.get)

  def connect(withQueue: Boolean = false): Boolean = {
    connection = Some(client.getConnection())
    if (connection.isDefined && connection.get.isOpen) {
      channel = Some(client.createTopicChannel(exchangeName, durable)(connection.get))
      channel.get.basicQos(1) // limits the number of unacknowledged messages revceived at once (0 for unlimited)
      //      channel.get.basicQos(AmqpClientConfiguration.prefetchCount) // limits the number of unacknowledged messages revceived at once (0 for unlimited)
      if (withQueue) {
        queueBind(); true
      } else true
    }
    else {
      logger.error("No connection available. Retrying to connect in 5 sec...")
      Thread.sleep(5000)
      connect(withQueue)
    }
  }

  def shutdown() = {
    if (thread.isDefined) {
      killSwitch.shutdown() // send termination signal
      Await.result(thread.get, Duration.Inf) // await the last consume iteration
    }
    close() // release connection and channel
  }

  def close() = {
    if (channel.isDefined) channel.get.close()
    if (connection.isDefined) connection.get.close()
  }
}
