/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.amqp.api

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-01-30
  */

object AmqpClientConfiguration {

  lazy val conf: Config = Option(ConfigFactory.load().getConfig("amqp")).getOrElse(ConfigFactory.empty())
  lazy val user: String = Option(conf.getString("user")).getOrElse("admin")
  lazy val pwd: String = Option(conf.getString("password")).getOrElse("Pa55w0rd")
  lazy val host: String = Option(conf.getString("broker.host")).getOrElse("localhost")
  lazy val vhost: String = Option(conf.getString("broker.vhost")).getOrElse("/")
  lazy val port: Int = Option(conf.getInt("broker.port")).getOrElse(5672)
  lazy val durable: Boolean = Option(conf.getBoolean("durable.messages")).getOrElse(true)
  lazy val fairDispatch: Boolean = Option(conf.getBoolean("qos.fairdispatch")).getOrElse(false)
  lazy val prefetchCount: Int = Option(conf.getInt("qos.prefetch.count")).getOrElse(10)
  lazy val automaticRecoveryEnabled: Boolean = Option(conf.getBoolean("automatic.recovery.enabled")).getOrElse(false)
  lazy val topologyRecoveryEnabled: Boolean = Option(conf.getBoolean("topology.recovery.enabled")).getOrElse(false)
  lazy val exchangeName: String = Option(conf.getString("exchange.name")).getOrElse("default")
  lazy val threadPoolSize: Int = Option(conf.getInt("threadpool.size")).getOrElse(20)
}
