/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq

import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.synaptix.core.GenericService
import com.rabbitmq.client.{AMQP, Channel, Connection}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 04/03/2021
 */

trait RabbitMQProducer extends GenericService {

  implicit val client: RabbitMQClient
  implicit val conn:Connection
  implicit val channel:Channel

  def publish(topic: String, exchangeName: String, messages:Vector[AmqpMessage], props:AMQP.BasicProperties=null) = {
    messages.foreach(msg => channel.basicPublish(exchangeName, topic, props, msg.toString.getBytes("UTF-8")))
  }

  override def shutdown(): Unit = {
    channel.close()
    conn.close()
  }
}