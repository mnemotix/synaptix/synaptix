/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api

import akka.Done
import akka.actor.ActorSystem
import akka.stream.AbruptTerminationException
import com.mnemotix.amqp.api.exceptions.BrokerOfflineException
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.synaptix.core._
import com.rabbitmq.client.Connection

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 08/03/2021
  */

class SynaptixRPCController(override val serviceName: String)(implicit implicit val client: RabbitMQClient, val system: ActorSystem, val ec: ExecutionContext) extends MonitoredService {

  implicit var connection: Option[Connection] = None

  val tasks = mutable.HashMap[String, mutable.ArrayBuffer[SynaptixRPCTask[_]]]()

  var threadPool: Future[Seq[Done]] = _

  override def init() = connection = Some(connect())

  def connect(): Connection = Try(client.getConnection()) match {
    case Success(conn: Connection) =>
      logger.info("Connection successful.")
      conn
    case Failure(err) =>
      logger.error("No connection available. Retrying to connect in 5 sec...", err)
      Thread.sleep(5000)
      connect
  }

  def disconnect() = {
    if (connection.isDefined && connection.get.isOpen) connection.get.close()
    true
  }

  def registerTask(topic: String, task: SynaptixRPCTask[_]) = {
    if (isVerbose) logger.info(s"""Service "$serviceName" registered a task (${task.getClass.getSimpleName}).""")
    val buffer = if (tasks.contains(topic)) tasks(topic) else mutable.ArrayBuffer[SynaptixRPCTask[_]]()
    buffer.append(task)
    tasks.put(topic, buffer)
  }

  def unregisterTask(topic: String) = {
    if (isVerbose) logger.info(s"""Service "$serviceName" unregistered all tasks on topic [$topic].""")
    tasks -= topic
  }

  def start(): Future[Seq[Done]] = {
    if (connection.isEmpty) init()
    if (isVerbose) logger.info(s"""Service "$serviceName" listening on: [rabbitmq://${AmqpClientConfiguration.user}:${AmqpClientConfiguration.pwd.map(_ => "*").mkString("")}@${AmqpClientConfiguration.host}:${AmqpClientConfiguration.port}]""")
    notify(NotificationValues.STARTUP)
    threadPool = Future.sequence(tasks.values.flatten.map(t => t.start()).toSeq)
    if (isVerbose) logger.info(s"""Service "$serviceName" startup is complete.""")
    threadPool onComplete {
      case Success(_) =>
      case Failure(error) =>
        error match {
          case sse: com.rabbitmq.client.ShutdownSignalException =>
            // broker was disconnected
            if (isVerbose) logger.error(s"""Broker is offline. Trying to reconnect...""", sse)
            throw BrokerOfflineException(s"""Broker is offline. Trying to reconnect...""", Some(sse))
          case ate: AbruptTerminationException =>
            if (isVerbose) logger.error("A thread has been halt abruptly.", ate)
            throw ThreadPoolException("A thread has been halt abruptly.", Some(ate))
          case se: SynaptixException =>
            if (isVerbose) logger.error(s"""Service "$serviceName" an error occured during the processing of a message.""", se)
            throw se
          case t: Throwable =>
            if (isVerbose) logger.error(s"""Service "$serviceName" an unknown error has occured.""", t)
            throw t
        }
    }
    threadPool
  }

  def shutdown() = {
    if (isVerbose) logger.info(s"""Service "$serviceName" is shutting down...""")
    listeners.foreach { case (_, buffer: mutable.ArrayBuffer[ServiceListener]) => buffer.foreach(_.onShutdown()) }
    notify(NotificationValues.SHUTDOWN)
    // Trigger all kill switches & await termination for each task
    tasks.values.flatten.foreach(_.shutdown)
    // Await for proper thread termination
    Await.result(threadPool, Duration.Inf)
  }

}
