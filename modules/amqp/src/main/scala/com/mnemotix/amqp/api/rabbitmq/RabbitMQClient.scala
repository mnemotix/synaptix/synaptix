/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq

import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.rabbitmq.restapi.{RabbitMQRestApiClient, models}
import com.rabbitmq.client.AMQP.{Exchange, Queue}
import com.rabbitmq.client.{BuiltinExchangeType, _}
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import java.io.IOException
import java.util.concurrent.{ExecutorService, Executors}
import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 03/03/2021
  */

class RabbitMQClient extends LazyLogging {

  lazy val factory = new ConnectionFactory
  factory.setRequestedHeartbeat(20)

  var isVerbose: Boolean = true

  def verbose() = isVerbose = true

  def mute() = isVerbose = false

  def safeExec[T](unsafeBlock: Try[T]): T = {
    unsafeBlock match {
      case Success(res) => res
      case Failure(err) => {
        err match {
          case _: ShutdownSignalException => {
            logger.warn("Broker was shut down. Trying to reconnect in 5 seconds...")
            Thread.sleep(5000)
            safeExec(unsafeBlock)
          }
          case ioe: IOException => {
            logger.error("Connection error, broker can be offline. Trying to reconnect in 5 seconds...", ioe)
            Thread.sleep(5000)
            safeExec(unsafeBlock)
          }
          case t: Throwable => {
            logger.error(t.getMessage, t.getCause)
            Thread.sleep(5000)
            safeExec(unsafeBlock)
          }
        }
      }
    }
  }

  def getConnection(): Connection = safeExec[Connection](Try {
    val es: ExecutorService = Executors.newFixedThreadPool(AmqpClientConfiguration.threadPoolSize)
    factory.setHost(AmqpClientConfiguration.host)
    factory.setVirtualHost(AmqpClientConfiguration.vhost)
    factory.setPort(AmqpClientConfiguration.port)
    factory.setUsername(AmqpClientConfiguration.user)
    factory.setPassword(AmqpClientConfiguration.pwd)
    factory.setExceptionHandler(new RabbitMQExceptionHandler) // default https://github.com/rabbitmq/rabbitmq-java-client/blob/master/src/main/java/com/rabbitmq/client/impl/ForgivingExceptionHandler.java
    return factory.newConnection(es)
  })

  def createChannel()(implicit conn: Connection): Channel = safeExec[Channel](Try(conn.createChannel))

  def createTopicChannel(exchangeName: String = "default", durable: Boolean)(implicit conn: Connection): Channel = safeExec[Channel](Try {
    val channel: Channel = conn.createChannel
    channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, durable)
    channel
  })

  def exchangeDeclare(exchangeName: String, exchangeType: BuiltinExchangeType = BuiltinExchangeType.TOPIC, durable: Boolean)(implicit channel: Channel): Exchange.DeclareOk = safeExec[Exchange.DeclareOk](
    Try(channel.exchangeDeclare(exchangeName, exchangeType, durable))
  )

  def exchangeDelete(exchangeName: String)(implicit channel: Channel): Exchange.DeleteOk = safeExec[Exchange.DeleteOk](
    Try(channel.exchangeDelete(exchangeName))
  )

  /**
    * Declare a queue
    *
    * @param channel    - the channel object
    * @param queueName  - the name of the queue
    * @param durable    - true if we are declaring a durable queue (the queue will survive a server restart)
    * @param exclusive  - true if we are declaring an exclusive queue (restricted to this connection)
    * @param autoDelete - true if we are declaring an autodelete queue (server will delete it when no longer in use)
    * @param args       - other properties (construction arguments) for the queue
    * @return the name of the queue
    */
  def queueDeclare(queueName: String = null, durable: Boolean = true, exclusive: Boolean = true, autoDelete: Boolean = true, args: Map[String, AnyRef] = Map.empty)(implicit channel: Channel): String = safeExec[String](Try {
    if (queueName == null) channel.queueDeclare().getQueue
    else channel.queueDeclare(queueName, durable, exclusive, autoDelete, args.asJava).getQueue
  })

  def queueBind(queueName: String, exchangeName: String, routingKey: String)(implicit channel: Channel): Queue.BindOk = safeExec[Queue.BindOk](
    Try(channel.queueBind(queueName, exchangeName, routingKey))
  )

  def queueUnbind(queueName: String, exchangeName: String, routingKey: String)(implicit channel: Channel): Queue.UnbindOk = safeExec[Queue.UnbindOk](
    Try(channel.queueUnbind(queueName, exchangeName, routingKey))
  )

  def queueDelete(queueName: String)(implicit channel: Channel): Queue.DeleteOk = safeExec[Queue.DeleteOk](
    Try(channel.queueDelete(queueName))
  )

  def messageCount(queueName: String)(implicit channel: Channel): Int = safeExec[Int](Try {
    val response = channel.queueDeclarePassive(queueName)
    // returns the number of messages in Ready state in the queue
    response.getMessageCount
  })

  def consumerCount(queueName: String)(implicit channel: Channel): Int = safeExec[Int](Try {
    val response = channel.queueDeclarePassive(queueName)
    // returns the number of consumers the queue has
    response.getConsumerCount
  })

  /**
    * Purges the contents of the given queue.
    * Params:
    * queue – the name of the queue
    * Returns:
    * a purge-confirm method if the purge was executed successfully
    *
    * @param channel
    * @param queueName
    * @return
    */
  def queuePurge(queueName: String)(implicit channel: Channel): Queue.PurgeOk = safeExec[Queue.PurgeOk](Try {
    channel.queuePurge(queueName)
  })


  /*
  REST API METHODS
   */
  def listExchanges(): Seq[models.Exchange] = {
    val restApiClient = new RabbitMQRestApiClient
    restApiClient.listExchanges()
  }

  def listQueues(): Seq[JsObject] = {
    val restApiClient = new RabbitMQRestApiClient
    val result = restApiClient.listQueues()
    result.as[Seq[JsObject]]
  }

  def listChannels(): Seq[JsObject] = {
    val restApiClient = new RabbitMQRestApiClient
    val result = restApiClient.listChannels()
    println(Json.prettyPrint(result))
    result.as[Seq[JsObject]]
  }

  def listBindings(): Seq[JsObject] = {
    val restApiClient = new RabbitMQRestApiClient
    val result = restApiClient.listBindings()
    result.as[Seq[JsObject]]
  }

  def getExchange(exchangeName: String): Option[models.Exchange] = listExchanges().find(ex => ex.name == exchangeName)

  def exchangeExists(exchangeName: String): Boolean = getExchange(exchangeName).isDefined

  def getChannel(channelNumber: Int): Option[JsObject] = listChannels().find(ch => (ch \ "id").as[Int] == channelNumber)

  def channelExists(channelNumber: Int): Boolean = getChannel(channelNumber).isDefined

  def getQueue(queueName: String): Option[JsObject] = listQueues().find(q => (q \ "name").as[String] == queueName)

  def queueExists(queueName: String): Boolean = getQueue(queueName).isDefined

  def getBinding(exchangeName: String, queueName: String): Option[JsObject] = listBindings().find(b => (b \ "source").as[String] == exchangeName && (b \ "destination").as[String] == queueName)

  def bindingExists(exchangeName: String, queueName: String): Boolean = getBinding(exchangeName, queueName).isDefined

}
