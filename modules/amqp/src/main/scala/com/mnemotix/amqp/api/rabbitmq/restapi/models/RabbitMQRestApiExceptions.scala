/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq.restapi.models

import com.mnemotix.synaptix.core.SynaptixException

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 13/03/2021
  */

abstract class RabbitMQRestApiException(message:String, source:Option[Throwable]) extends SynaptixException(message, source)

case class RabbitMQListExchangesException(message:String, source:Option[Throwable]) extends RabbitMQRestApiException(message, source)

case class RabbitMQListConnectionsException(message:String, source:Option[Throwable]) extends RabbitMQRestApiException(message, source)

case class RabbitMQListChannelsException(message:String, source:Option[Throwable]) extends RabbitMQRestApiException(message, source)

case class RabbitMQListConsumersException(message:String, source:Option[Throwable]) extends RabbitMQRestApiException(message, source)

case class RabbitMQListQueuesException(message:String, source:Option[Throwable]) extends RabbitMQRestApiException(message, source)

case class RabbitMQListBindingsException(message:String, source:Option[Throwable]) extends RabbitMQRestApiException(message, source)
