/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq

import com.rabbitmq.client.impl.ForgivingExceptionHandler
import com.rabbitmq.client.{Channel, Consumer}
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 12/03/2021
  */

class RabbitMQExceptionHandler extends ForgivingExceptionHandler with LazyLogging {
  override def handleConsumerException(channel: Channel, exception: Throwable, consumer: Consumer, consumerTag: String, methodName: String): Unit = {
    handleChannelKiller(channel, exception, "Consumer " + consumer + " (" + consumerTag + ")" + " method " + methodName + " for channel " + channel)
  }

  override def handleChannelKiller(channel: Channel, exception: Throwable, what: String): Unit = {
    logger.error(exception.getMessage)
  }
}
