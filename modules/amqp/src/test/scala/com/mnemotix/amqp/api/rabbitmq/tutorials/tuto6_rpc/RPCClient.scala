/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto6_rpc

import com.rabbitmq.client.{AMQP, CancelCallback, ConnectionFactory, DeliverCallback, Delivery}

import java.util.UUID
import java.util.concurrent.{ArrayBlockingQueue, BlockingQueue}
import java.util.concurrent.TimeoutException
import java.io.IOException


/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

class RPCClient() extends Runnable with AutoCloseable {
  val RPC_QUEUE_NAME = "rpc_queue"

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  val connection = factory.newConnection
  val channel = connection.createChannel

  def run()= {
    try{
      val fibonacciRpc = new RPCClient()
      (1 to 32).map(i => {
        val i_str = Integer.toString(i)
        System.out.println(" [x] Requesting fib(" + i_str + ")")
        val response = fibonacciRpc.call(i_str)
        System.out.println(" [.] Got '" + response + "'")
      })
    }
    catch {
      case ioe : IOException => ioe.printStackTrace()
      case toe : TimeoutException => toe.printStackTrace()
      case ie : InterruptedException => ie.printStackTrace()
    }
  }

  def call(message:String) = {
    val corrId = UUID.randomUUID().toString()
    val replyQueueName = channel.queueDeclare().getQueue()
    val props = new AMQP.BasicProperties.Builder().correlationId(corrId).replyTo(replyQueueName).build()
    channel.basicPublish("", RPC_QUEUE_NAME, props, message.getBytes("UTF-8"))
    val response:BlockingQueue[String] = new ArrayBlockingQueue[String](1)

    val deliverCallback:DeliverCallback = (consumerTag: String, delivery: Delivery) => {
      if (delivery.getProperties.getCorrelationId.equals(corrId)) response.offer(new String(delivery.getBody, "UTF-8"))
    }
    val cancelCallback:CancelCallback = (consumerTag:String) => {}

    val ctag = channel.basicConsume(replyQueueName, true, deliverCallback, cancelCallback)

    val result = response.take()
    channel.basicCancel(ctag)
    result
  }

  override def close() = {
    connection.close()
  }
}
