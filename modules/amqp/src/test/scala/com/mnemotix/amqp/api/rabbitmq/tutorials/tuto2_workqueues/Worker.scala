/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto2_workqueues

import com.rabbitmq.client.{CancelCallback, ConnectionFactory, DeliverCallback, Delivery}

import java.util.UUID

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

class Worker(id:Int) extends Runnable {

  private val TASK_QUEUE_NAME = "task_queue"

//  val id: String = UUID.randomUUID().toString

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  def run(): Unit ={
    val connection = factory.newConnection
    val channel = connection.createChannel

    channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null)
    println(s" [*] [Worker.$id] Waiting for messages. To exit press CTRL+C")

    channel.basicQos(1)

    val deliverCallback:DeliverCallback = (consumerTag: String, delivery: Delivery) => {
      val message = new String(delivery.getBody, "UTF-8")
      println(s" [x] [Worker.$id] Received '" + message + "'")
      try {
        doWork(message)
      }
      finally {
        println(s" [x] [Worker.$id] Done")
        channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
      }
    }
    val cancelCallback:CancelCallback = (consumerTag:String) => {}
    channel.basicConsume(TASK_QUEUE_NAME, false, deliverCallback, cancelCallback)
  }

  def doWork(task: String): Unit = {
    for (ch <- task.toCharArray) {
      if (ch == '.') try Thread.sleep(50)
      catch {
        case _ignored: InterruptedException =>
          Thread.currentThread.interrupt()
      }
    }
  }
}
