/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should
import org.scalatest.wordspec.AnyWordSpec
import play.api.libs.json.{JsNumber, JsValue}

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 08/03/2021
 */

class SynaptixRPCSpec extends AnyWordSpec with should.Matchers with BeforeAndAfterAll with ScalaFutures with LazyLogging {
  override implicit val patienceConfig: PatienceConfig = PatienceConfig(30.seconds)

  implicit val system = ActorSystem("SynaptixRPC-" + System.currentTimeMillis())
  implicit val ec = system.dispatcher
  implicit val rabbit = new RabbitMQClient()

  def messages(nb: Int = 100) = {
    val headers = Map.empty[String, JsValue]
    (1 to nb).map(i => AmqpMessage(headers, JsNumber(i))).toVector
  }


}
