/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto7_synaptix

import akka.Done
import akka.stream.KillSwitches
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.models.AmqpMessage
import com.rabbitmq.client._
import org.joda.time.DateTime
import play.api.libs.json.{JsNumber, JsString, JsValue, Json}

import java.util.UUID
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

class SynaptixTaskNoFuture(id: Int, bindingKey: String) {

  val sender: String = UUID.randomUUID().toString

  private val EXCHANGE_NAME = "synaptix"
  val RPC_QUEUE_NAME = s"rpc_queue-$bindingKey"

  var connection:Option[Connection] = None
  var channel:Option[Channel] = None
  var runnableGraph:Option[Future[Done]] = None

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  val killSwitch = KillSwitches.shared(s"$sender-kill-switch")

  def shutdown(): Unit ={
    killSwitch.shutdown
    if(runnableGraph.isDefined) Await.result(runnableGraph.get, Duration.Inf)
  }

  def close()={
    if(channel.isDefined) channel.get.close()
    if(connection.isDefined) connection.get.close()
  }

  def run(): String = {
    connection = Some(factory.newConnection)
    channel = Some(connection.get.createChannel)
    channel.get.exchangeDeclare(EXCHANGE_NAME, "topic")
    channel.get.queueDeclare(RPC_QUEUE_NAME, false, false, false, null)
    channel.get.queuePurge(RPC_QUEUE_NAME)
    channel.get.queueBind(RPC_QUEUE_NAME, EXCHANGE_NAME, bindingKey)
    channel.get.basicQos(AmqpClientConfiguration.prefetchCount)

    println(s" [*] [SynaptixTask.$id] Waiting for messages with binding key [$bindingKey]. To exit press CTRL+C")

    val deliverCallback: DeliverCallback = (consumerTag: String, delivery: Delivery) => {

      val message = new String(delivery.getBody, "UTF-8")
      println(s" [x] [SynaptixTask.$id][$bindingKey] Received '" + delivery.getEnvelope().getRoutingKey() + "':'" + message + "'")
      val replyProps: AMQP.BasicProperties = new AMQP.BasicProperties.Builder().correlationId(delivery.getProperties().getCorrelationId()).build()

      try {
        val response = processMessage(message)
        // send response to reply queue with proper correlationId
        channel.get.basicPublish("", delivery.getProperties.getReplyTo, replyProps, response.toString.getBytes("UTF-8"))
      }
      catch {
        case t: Throwable => {
          val response = getErrorMessage(t)
          // send response to reply queue with proper correlationId
          channel.get.basicPublish("", delivery.getProperties.getReplyTo, replyProps, response.toString.getBytes("UTF-8"))
        }
      }
      finally {
        // acknowledge broker that message was correctly processed
        channel.get.basicAck(delivery.getEnvelope.getDeliveryTag, false)
      }
    }
    val cancelCallback: CancelCallback = (consumerTag: String) => {}
    val ctag = channel.get.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, cancelCallback)
    println("consumed")
    ctag
  }

  def processMessage(message: String): AmqpMessage = {
    val time = DateTime.now().getMillis
    if (time % 10 == 0) throw new RuntimeException("[SynaptixTask.$id][$bindingKey] Ooops something bad happened !")
    Thread.sleep(1000)
    getResponseMessage(JsString(s"processed message $message"))
  }

  def getResponseMessage(body: JsValue, format: String = "JSON", status: String = "OK"): AmqpMessage = {
    val head = Map(
      "command" -> JsString(bindingKey),
      "sender" -> JsString(sender),
      "timestamp" -> JsNumber(System.currentTimeMillis()),
      "format" -> JsString(format),
      "status" -> JsString(status)
    )
    AmqpMessage(head, body)
  }

  def getErrorMessage(err: Throwable): AmqpMessage = {
    val body = Json.obj(
      "errorClass" -> JsString(err.getClass.getSimpleName),
      "errorMessage" -> JsString(err.getMessage)
    )
    getResponseMessage(body, "JSON", "ERROR")
  }
}
