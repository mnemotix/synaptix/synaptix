/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq

import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.rabbitmq.restapi.models
import com.rabbitmq.client.AMQP.{Exchange, Queue}
import com.rabbitmq.client.{BuiltinExchangeType, Connection}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 03/03/2021
 */

class RabbitMQClientSpec extends RabbitMQSpec {

  val exchangeName = "test"
  val topic = "graph.select"
  implicit lazy val conn = rabbit.getConnection()
  implicit lazy val channel = rabbit.createChannel()
  var queueName = ""

  override protected def afterAll() = {
    channel.close()
    conn.close()
  }

  "RabbitMQClient" should {
    "get connection from broker" in {
      logger.info(conn.toString)
      conn.isInstanceOf[Connection] shouldBe true
      conn.isOpen shouldBe true
    }
    "get channel from connection" in {
      val channel = rabbit.createChannel()
      channel.getChannelNumber shouldBe > (0)
      channel.getConnection.toString shouldEqual conn.toString
    }
    "declare an exchange from a channel" in {
      val exch = rabbit.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, AmqpClientConfiguration.durable)
      exch.isInstanceOf[Exchange.DeclareOk] shouldBe true
      rabbit.exchangeExists(exchangeName) shouldBe true
      val exchanges:Seq[models.Exchange] = rabbit.listExchanges()
      val isDurable:Option[Boolean] = exchanges.find(ex => ex.name == exchangeName).map(ex => ex.durable)
      isDurable.isDefined shouldBe true
      isDurable.get shouldBe true
    }
    "declare a queue from channel" in {
      queueName = rabbit.queueDeclare()
      rabbit.queueExists(queueName) shouldBe true
    }
    "bind a queue to an exchange" in {
      val isOk = rabbit.queueBind(queueName, exchangeName, topic)
      isOk.isInstanceOf[Queue.BindOk] shouldBe true
      rabbit.bindingExists(exchangeName, queueName) shouldBe true
    }

    "delete a queue from channel" in {
      rabbit.queueDelete(queueName)
      rabbit.queueExists(queueName) shouldBe false
    }
    "delete an exchange from a channel" in {
      rabbit.exchangeDelete(exchangeName)
      rabbit.exchangeExists(exchangeName) shouldBe false
    }
  }
}
