/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto7_synaptix

import com.rabbitmq.client._

import java.io.IOException
import java.util.UUID
import java.util.concurrent.{ArrayBlockingQueue, BlockingQueue, TimeoutException}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random


/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

class SynaptixClient(id: Int, routingKey: String)(implicit val ec:ExecutionContext) extends Runnable with AutoCloseable {

  private val EXCHANGE_NAME = "synaptix"

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  val connection = factory.newConnection
  val channel = connection.createChannel

  def run() = Future{
    try {
      val client = new SynaptixClient(id, routingKey)
//      val txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sit amet turpis egestas, accumsan purus in, vulputate justo. Phasellus quam eros, vulputate a ante vitae, aliquam facilisis libero. Aliquam erat volutpat. Etiam posuere quam a auctor aliquam. Praesent viverra euismod nisi, sed consequat magna mollis eget. Suspendisse vel venenatis magna. Etiam aliquet nec felis eu hendrerit. In massa justo, molestie sit amet dolor ut, tristique aliquet nulla. Cras tristique tellus eget volutpat blandit. Fusce tempus dapibus ornare. Donec at nulla auctor, accumsan eros et, semper nunc. Cras varius augue a augue gravida, eu suscipit mi rhoncus. Aliquam iaculis felis arcu, laoreet cursus neque fringilla sit amet. Pellentesque facilisis mollis metus vitae vestibulum."
      while(true){
//        val tokens = Random.shuffle(txt.split(" ").toList)
//        val message = tokens.head
        println(s" [*] [SynaptixClient.$id] Requesting [$routingKey] with [$routingKey]")
        val response = client.call(routingKey, routingKey)
        System.out.println(" [.] Got '" + response + "'")
//        Thread.sleep(1000)
      }
    }
    catch {
      case ioe: IOException => ioe.printStackTrace()
      case toe: TimeoutException => toe.printStackTrace()
      case ie: InterruptedException => ie.printStackTrace()
    }
  }

  def call(routingKey: String, message: String) = {
    // Create a correlation ID
    val corrId = UUID.randomUUID().toString()
    // Create a replay queue
    val replyQueueName = channel.queueDeclare().getQueue()
    val props = new AMQP.BasicProperties.Builder().correlationId(corrId).replyTo(replyQueueName).build()
    // send the call
    println(s" [*] [SynaptixClient.$id] posting message [$routingKey] with routing key [$routingKey]")
    channel.basicPublish(EXCHANGE_NAME, routingKey, props, message.getBytes("UTF-8"))

    // await for response
    val response: BlockingQueue[String] = new ArrayBlockingQueue[String](1) // single entry FIFO array which refuses overwrite of its value as long as it is not consumed
    // handle dellivery callback
    val deliverCallback: DeliverCallback = (consumerTag: String, delivery: Delivery) => if (delivery.getProperties.getCorrelationId.equals(corrId)) response.offer(new String(delivery.getBody, "UTF-8"))
    // empty cancel callback
    val cancelCallback: CancelCallback = (consumerTag: String) => {}

    val ctag = channel.basicConsume(replyQueueName, true, deliverCallback, cancelCallback)
    val result = response.take() // Retrieves and removes the head of this queue, waiting if necessary until an element becomes available.
    // remove the temporary consumer
    channel.basicCancel(ctag)
    result
  }

  override def close() = {
    connection.close()
  }
}
