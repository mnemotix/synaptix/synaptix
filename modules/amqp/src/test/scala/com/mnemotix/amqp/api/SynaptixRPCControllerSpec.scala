/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api

import com.mnemotix.amqp.api.listeners.{FakeGraphDeleteListener, FakeGraphSelectListener}
import com.mnemotix.amqp.api.tasks.{FakeGraphDeleteTask, FakeGraphSelectTask}
import org.scalatest.exceptions.TestFailedException

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 08/03/2021
 */

class SynaptixRPCControllerSpec extends SynaptixRPCSpec {

  override implicit val patienceConfig: PatienceConfig = PatienceConfig(10.seconds)

  val controller = new SynaptixRPCController("TestingPurposeControllerSpec")

  "SynaptixRPCController" should {
    "register tasks" in {
      controller.registerTask("graph.select", new FakeGraphSelectTask())
      controller.registerTask("graph.select", new FakeGraphSelectTask())
      controller.registerTask("graph.delete", new FakeGraphDeleteTask())
      controller.registerTask("graph.delete", new FakeGraphDeleteTask())
      controller.tasks.size shouldEqual 2
      controller.tasks("graph.select").size shouldEqual 2
      controller.tasks("graph.delete").size shouldEqual 2
    }
    "register listeners" in {
      controller.registerListener("graph.select", new FakeGraphSelectListener())
      controller.registerListener("graph.delete", new FakeGraphDeleteListener())
      controller.registerListener("graph.select", new FakeGraphSelectListener())
      controller.registerListener("graph.delete", new FakeGraphDeleteListener())
      controller.listeners.size shouldEqual 2
      controller.listeners("graph.select").size shouldEqual 2
      controller.listeners("graph.delete").size shouldEqual 2
    }
    "unregister tasks" ignore {
      controller.unregisterTask("graph.delete")
      controller.tasks.size shouldEqual 1
      controller.tasks("graph.select").size shouldEqual 2
      controller.tasks.contains("graph.delete") shouldBe false
    }
    "unregister listeners" ignore {
      controller.unregisterListener("graph.delete")
      controller.listeners.size shouldEqual 1
      controller.listeners("graph.select").size shouldEqual 2
      controller.listeners.contains("graph.delete") shouldBe false
    }
    "run tasks" in {
      val thread = controller.start()
      try {
        thread.futureValue
      } catch {
        case _:TestFailedException => logger.info("No exception before timeout, everything's OK...")
        case t:Throwable =>
          logger.error("Oops! Something went wrong.", t)
          println(t.getClass.getName)
          t.printStackTrace()
      }
    }
    "stop tasks" in {
      controller.shutdown
    }
  }
}
