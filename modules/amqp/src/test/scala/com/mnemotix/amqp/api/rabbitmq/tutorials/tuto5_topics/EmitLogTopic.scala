/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto5_topics

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

import com.rabbitmq.client.ConnectionFactory

import scala.util.Random


object EmitLogTopic extends App {

  private val EXCHANGE_NAME = "topic_logs"

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  val connection = factory.newConnection
  val channel = connection.createChannel

  val routingKeys = List("kern.trivial", "kern.critical", "kern.fatal", "app.trivial", "app.critical", "app.fatal")
  channel.exchangeDeclare(EXCHANGE_NAME, "topic")

  (1 to 10000).map(i => {
    val routingKey = Random.shuffle(routingKeys).head
    val message = s"Hello World! $i"
    channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes)
    println(s" [x] Sent '$routingKey':'$message'")
    Thread.sleep(50)
  })
}
