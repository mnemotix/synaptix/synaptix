/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto5_topics

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

object ReceiveLogsConcurrentRunner extends App {
  val nbReceivers = 20

  val workers:Seq[ReceiveLogsTopic] = Seq(
    new ReceiveLogsTopic(1,"#"),
    new ReceiveLogsTopic(2, "kern.*"),
    new ReceiveLogsTopic(3, "*.critical"),
    new ReceiveLogsTopic(4, "kern.fatal"),
  )

  workers.map(_.run())
}
