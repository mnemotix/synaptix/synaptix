/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto6_rpc

import com.rabbitmq.client._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

class RPCServer(id: Int) extends Runnable {

  val RPC_QUEUE_NAME = "rpc_queue"

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  def run() = {
    val connection = factory.newConnection
    val channel = connection.createChannel

    channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null)
    channel.queuePurge(RPC_QUEUE_NAME)
    channel.basicQos(1)

    println(" [x] Awaiting RPC requests")

    val deliverCallback: DeliverCallback = (consumerTag: String, delivery: Delivery) => {

      val replyProps: AMQP.BasicProperties = new AMQP.BasicProperties.Builder().correlationId(delivery.getProperties().getCorrelationId()).build()
      var response = ""

      try {
        val message = new String(delivery.getBody, "UTF-8")
        val n = message.toInt
        System.out.println(" [.] fib(" + message + ")")
        response += fib(n)
      }
      catch {
        case e: RuntimeException => println(" [.] " + e.toString)
      }
      finally {
        channel.basicPublish("", delivery.getProperties.getReplyTo, replyProps, response.getBytes("UTF-8"))
        channel.basicAck(delivery.getEnvelope.getDeliveryTag, false)
      }

      val message = new String(delivery.getBody, "UTF-8")
      println(s" [x] [RPCServer.$id] Received '" + delivery.getEnvelope().getRoutingKey() + "':'" + message + "'")
    }
    val cancelCallback: CancelCallback = (consumerTag: String) => {}

    channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, cancelCallback)

  }

  private def fib(n: Int): Int = {
    if (n == 0) return 0
    if (n == 1) return 1
    fib(n - 1) + fib(n - 2)
  }
}
