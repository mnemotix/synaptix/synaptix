/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto4_routing

import com.rabbitmq.client.{CancelCallback, ConnectionFactory, DeliverCallback, Delivery}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

class ReceiveLogsDirect(id:Int, severity:String) extends Runnable {

  private val EXCHANGE_NAME = "direct_logs"

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  def run()={
    val connection = factory.newConnection
    val channel = connection.createChannel

    channel.exchangeDeclare(EXCHANGE_NAME, "direct")
    val queueName:String = channel.queueDeclare().getQueue()
    channel.queueBind(queueName, EXCHANGE_NAME, severity)

    println(s" [*] [ReceiveLogsDirect.$id] Waiting for messages. To exit press CTRL+C")

    val deliverCallback:DeliverCallback = (consumerTag: String, delivery: Delivery) => {
      val message = new String(delivery.getBody, "UTF-8")
      println(s" [x] [ReceiveLogsDirect.$id] Received '" + delivery.getEnvelope().getRoutingKey() + "':'" + message + "'")
    }
    val cancelCallback:CancelCallback = (consumerTag:String) => {}

    channel.basicConsume(queueName, true, deliverCallback, cancelCallback)
  }
}
