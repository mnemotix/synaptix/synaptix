/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq.tutorials.tuto2_workqueues

import com.rabbitmq.client.{ConnectionFactory, MessageProperties}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/03/2021
 */

object NewTask extends App {
  private val TASK_QUEUE_NAME = "task_queue"

  lazy val factory = new ConnectionFactory
  factory.setHost("localhost")
  factory.setVirtualHost("/")
  factory.setUsername("admin")
  factory.setPassword("Pa55w0rd")

  val connection = factory.newConnection
  val channel = connection.createChannel

  channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null)

  (1 to 10).map(i => {
    val message = s"Hello World! $i"
    channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"))
    System.out.println(" [x] Sent '" + message + "'")
    Thread.sleep(50)
  })

  sys addShutdownHook{
    channel.queueDelete(TASK_QUEUE_NAME)
    channel.close()
    connection.close()
  }
}
