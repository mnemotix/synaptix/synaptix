/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq

class RabbitMQServiceSpec extends RabbitMQSpec {

  val exchangeName = "default"
  val topic = "graph.select"

  val consumer = new RabbitMQService(topic, exchangeName){
    override def init(): Unit = {}
    override val serviceName: String = s"$exchangeName.$topic.service"
  }

  "RabbitMQService" should {
    "connect to broker" in {
      consumer.connect()
    }
    "close connection & channel to broker" in {
      // Send the termination signal
      consumer.shutdown()
    }
  }
}
