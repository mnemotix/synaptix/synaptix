/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.amqp.api.rabbitmq.restapi

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should
import org.scalatest.wordspec.AnyWordSpec
import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 13/03/2021
  */

class RabbitMQRestApiClientSpec extends AnyWordSpec with should.Matchers with BeforeAndAfterAll with ScalaFutures with LazyLogging {
  val client = new RabbitMQRestApiClient
  "RabbitMQRestApiClient" should {
    "list exchanges for a given vhost" in {
      val ex = client.listExchanges()
      println(Json.prettyPrint(Json.toJson(ex)))
    }
    "list connections for a given vhost" in {
      val connections = client.listConnections()
      println(Json.prettyPrint(connections))
    }
    "list channels for a given vhost" in {
      val channels = client.listChannels()
      println(Json.prettyPrint(channels))
    }
    "list consumers for a given vhost" in {
      val cons = client.listConsumers()
      println(Json.prettyPrint(cons))
    }
    "list queues for a given vhost" in {
      val queues = client.listQueues()
      println(Json.prettyPrint(queues))
    }
    "list bindings for a given vhost" in {
      val bindings = client.listBindings()
      println(Json.prettyPrint(bindings))
    }
  }
}
