/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq

import com.mnemotix.amqp.api.models.AmqpMessage
import com.rabbitmq.client.{Channel, Connection}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 04/03/2021
 */

class RabbitMQRPCCallSpec extends RabbitMQSpec {

  val exchangeName = "default"
  val topic = "graph.select"
  implicit val conn: Connection = rabbit.getConnection()
  implicit val channel: Channel = rabbit.createChannel()

  val nbMessages = 10000
  val rpc = new RabbitMQRPCCall(topic, exchangeName) {
    override def onReply(message: AmqpMessage): Unit = {}
  }
  "RabbitMQRPCCall" should {
    "start listening on its reply queue" in {
      rpc.init()
      rpc.start()
    }
    "execute an RPC call" in {
      rpc.call(messages(10))
      Thread.sleep(5000)
    }
    "close connection & channel to broker" in {
      // Send the termination signal
      rpc.shutdown()
    }
  }
}
