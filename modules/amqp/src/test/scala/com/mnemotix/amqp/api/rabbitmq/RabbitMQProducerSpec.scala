/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.amqp.api.rabbitmq
import com.rabbitmq.client.{Channel, Connection}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 04/03/2021
 */

class RabbitMQProducerSpec extends RabbitMQSpec {

  val exchangeName = "default"
  val topic = "graph.select"

  val producer = new RabbitMQProducer{

    override def init(): Unit = ???

    override implicit val client: RabbitMQClient = rabbit
    override implicit val conn: Connection = rabbit.getConnection()
    override implicit val channel: Channel = rabbit.createChannel()
  }

  val nbMessages = 1000

  "RabbitMQProducer" should {
    "publish messages to queue" in {
      producer.publish(topic, exchangeName, messages(nbMessages))
    }
    "close connection & channel to broker" in {
      producer.shutdown()
    }
  }
}
