# With max-open-requests = 32

| nbr | download one after another and disk write | download bulk and disk write | download file and put in a cache | bulk download file and bulk insert in a cache | bulk download file and insert in a cache |
| --- | ----------------------------------------- | ---------------------------- | -------------------------------- | --------------------------------------------- | ---------------------------------------- |
| 199 |			110 s							|		28 s				   |			87 s				  |			24 s						  		  | Indisponible |
| 299 |			148 s							|		47 s				   |			181 s				  |			34 s								  | Indisponible |
| 1000|			516 s							|		118 s				   |			418 s				  |			107 s								  | 117 s |
| 4000|			Indisponible					|		479 s				   |			Indisponible	  	  |			553 s								  | 457 s |

# max-open-requests = 64

| nbr | download one after another and disk write | download bulk and disk write | download file and put in a cache | bulk download file and bulk insert in a cache | bulk download file and insert in a cache |
| --- | ----------------------------------------- | ---------------------------- | -------------------------------- | --------------------------------------------- | ---------------------------------------- |
| 1000|			Indisponible						|		96 s			   |			Indisponible			  |			Indisponible						|  96 s                                  |
| 4000|			Indisponible					|			336 s			   |			Indisponible	  	  |			Indisponible							| 370 s                                 |


# max-open-requests = 128

| nbr | download one after another and disk write | download bulk and disk write | download file and put in a cache | bulk download file and bulk insert in a cache | bulk download file and insert in a cache |
| --- | ----------------------------------------- | ---------------------------- | -------------------------------- | --------------------------------------------- | ---------------------------------------- |
| 1000|			Indisponible						|					   |			Indisponible			  |			Indisponible						|                                |
| 4000|			Indisponible					|			314 s			   |			Indisponible	  	  |			Indisponible							|                                 |

# max-open-requests = 256

| nbr | download one after another and disk write | download bulk and disk write | download file and put in a cache | bulk download file and bulk insert in a cache | bulk download file and insert in a cache |
| --- | ----------------------------------------- | ---------------------------- | -------------------------------- | --------------------------------------------- | ---------------------------------------- |
| 1000|			Indisponible						|					   |			Indisponible			  |			Indisponible						    |                   98 s                   |
| 4000|			Indisponible					|			322  s			   |			Indisponible	  	  |			Indisponible							|                     s                  |
| 8000|                                         |                              |                                  |                                                 |                                          |           