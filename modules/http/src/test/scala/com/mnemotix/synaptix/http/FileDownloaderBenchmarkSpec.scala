/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.http

import akka.stream.scaladsl.{FileIO, Sink, Source}
import akka.util.ByteString
import com.mnemotix.synaptix.cache.RocksDBStore
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import java.io.File
import java.nio.file.Paths
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}

class FileDownloaderBenchmarkSpec extends HttpSpec with BeforeAndAfterEach with BeforeAndAfterAll {

  lazy val muscatUrl = (s: String) => s"https://www.sudoc.fr/$s.rdf"
  lazy val testDir = "modules/http/src/test/resources/"
  lazy val dbPath = s"${testDir}cache/test.db"


  def extractor = {
    val bufferedSource = io.Source.fromFile(s"${testDir}MNHN.tsv")
    bufferedSource.getLines.drop(1)
  }

  def deleteDatabase = {
    val store = new RocksDBStore(dbPath)
    store.init()
    store.shutdown()
    store.drop()
  }

  override def beforeEach(): Unit = {
    Thread.sleep(5000)
    deleteDatabase
    val rdfDir = new File(s"${testDir}rdf/")
    rdfDir.listFiles.foreach(_.delete())
  }

  override def afterAll() = {
    deleteDatabase
    val rdfDir = new File(s"${testDir}rdf/")
    rdfDir.listFiles.foreach(_.delete())
  }

  it should "download all file on after on" in {
    val fileDownloader = new FileDownloader
    val listFiles = extractor
    val startTimeMillis = System.currentTimeMillis()
    listFiles.toSeq.map { id =>
      Await.result(fileDownloader.downloadFileToDisk(muscatUrl(id), s"${testDir}rdf/$id.rdf"), 2.minute)
    }
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"the `download all file on after on` process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

  it should "bulk download" in {
    val paral = 256
    val fileDownloader = new FileDownloader
    val listFiles = extractor.slice(0, 4000)
    val map = listFiles.map(id => (muscatUrl(id), s"${testDir}rdf/${id}.rdf"))
    val startTimeMillis = System.currentTimeMillis()
    val result = fileDownloader.downloadFileToDisk(map.toMap, paral)
    Await.result(result, Duration.Inf)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"the `bulk download` process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

  it should "download file with flow one by one" ignore  {
    val fileDownloader = new FileDownloader
    val listFiles = extractor
    val startTimeMillis = System.currentTimeMillis()
    /*
    listFiles.toSeq.map { id =>
      Await.result(fileDownloader.downloadFileAsync(muscatUrl(id)).map(_.runWith(FileIO.toPath(Paths.get(s"${testDir}rdf/$id.rdf")))), Duration.Inf)
    }
     */
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"the `download file with flow one by one` process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

  it should "download file with flow on a bulk way" ignore  {
    case class Testit(byteString: Source[ByteString, Any], id: String)
    val fileDownloader = new FileDownloader
    val listFiles = extractor
    val paral = 32
    val startTimeMillis = System.currentTimeMillis()
    /*
    val r = Source.fromIterator(() => listFiles.iterator).mapAsync(paral) { id =>
      Await.result(fileDownloader.downloadFileAsync(muscatUrl(id)).map(_.runWith(FileIO.toPath(Paths.get(s"${testDir}rdf/$id.rdf")))), Duration.Inf)
    }.runWith(Sink.ignore)
    Await.result(r, Duration.Inf)
     */
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"the `download file with flow on a bulk way` process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

  it should "download file and put content in a Cache" in {
    val store = new RocksDBStore(dbPath)
    store.init

    val fileDownloader = new FileDownloader
    val listFiles = extractor
    val paral = 32
    val startTimeMillis = System.currentTimeMillis()
    listFiles.toSeq.map { id =>
      Await.result(fileDownloader.downloadFileToCache(muscatUrl(id), store), Duration.Inf)
    }
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    store.shutdown()
    println(s"the `download file and put content in a Cache` process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

  it should "download file and put content in a Cache in a bulk way" in {
    val store = new RocksDBStore(dbPath)
    store.init
    val fileDownloader = new FileDownloader
    val listUri = extractor.slice(0, 1000).map(muscatUrl(_))
    val paral = 256
    val startTimeMillis = System.currentTimeMillis()
    val r = fileDownloader.downloadFileToCache(listUri.toSeq, store, paral)
    Await.result(r, Duration.Inf)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    store.shutdown()
    println(s"the `download file and put content in a Cache in a bulk way` process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

}
