/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.http

import java.io.File

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class FileDownloaderSpec extends HttpSpec {
  behavior of "FileDownloader"

  val dest = "/tmp/test-file.txt"
  val uriString = "https://gitlab.com/mnemotix/synaptix/synaptix-http-toolkit/-/raw/0.1.9/src/test/resources/test-file.txt"

  it should "download a file" in {
    val downloader = new FileDownloader
    val future = downloader.downloadFileToDisk(uriString, dest)
    Await.result(future, Duration.Inf)
    val file = new File("/tmp/test-file.txt")
    file.exists() shouldBe(true)
    io.Source.fromFile(file).getLines().next() shouldEqual "this is a test file"
  }

  it should "download a file on a disk" in {
    val downloader = new FileDownloader()
    val future = downloader.downloadFileAsyncToFile("https://www.idref.fr/031232213.rdf",  ("/Users/prlherisson/Documents/data/mnhn/home/idref/031232213.rdf"))
    Await.result(future, Duration.Inf)
  }
}