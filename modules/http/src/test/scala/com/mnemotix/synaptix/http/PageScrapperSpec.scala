/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.http

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class PageScrapperSpec extends HttpSpec {
  behavior of "PageScrapper"

  it should "fetch the content of a page" in {
      val pageScapper = new PageScrapper()
      val uriString = "https://gitlab.com/mnemotix/synaptix/synaptix-http-toolkit/-/raw/0.1.9/src/test/resources/test-file.txt"
      val future = pageScapper.pageContent(uriString)
      val res = Await.result(future, Duration.Inf)
      res.trim shouldEqual "this is a test file".trim
    }
}
