/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.http

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.GET
import akka.http.scaladsl.model._
import akka.stream.scaladsl.{Flow, Framing, Sink, Source}
import akka.util.ByteString

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class PageScrapper {
  def pageContent(pageUri: String)(implicit system: ActorSystem, ec: ExecutionContext): Future[String] = {
    try {
      val delimiter: Flow[ByteString, ByteString, NotUsed] =
        Framing.delimiter(
          ByteString("\r\n"),
          maximumFrameLength = 100000,
          allowTruncation = true)

      val homeUri = Uri(s"$pageUri")
      val req = HttpRequest(GET, uri = homeUri)

      val fut: Future[Source[String, _]] = Http().singleRequest(req).map { resp =>
        resp.entity.dataBytes.via(delimiter)
          .map(_.utf8String)
      }
      val result: Source[String, _] = Await.result(fut, Duration.Inf)
      val sink = Sink.fold[String, String]("")(_ + "\n" + _)
      result.runWith(sink)
    }
    catch {
      case t: IllegalRequestException => throw ScappingException(s"Illegal Request exception, Error ${t.status.intValue}", Some(t))
      case t: IllegalResponseException => throw ScappingException("Illegal response", Some(t))
      case t: IllegalUriException => throw ScappingException("Illegal Uri", Some(t))
      case t: Exception => throw ScappingException("An error occured during the download process", Some(t))
    }
  }
}
