/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.http.scaladsl.model.HttpMethods.{GET, POST}
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{FormData, HttpMethod, HttpMethods, HttpRequest, HttpResponse, Uri}
import akka.pattern.after
import com.mnemotix.synaptix.core.{MonitoredService, NotificationValues}
import com.mnemotix.synaptix.core.utils.RandomNameGenerator
import play.api.libs.json.JsObject

import java.util.concurrent.{Callable, Executors, TimeUnit}
import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

class RestClient(pServiceName: Option[String]=None)(implicit val system: ActorSystem, ec: ExecutionContext) extends MonitoredService{

  override val serviceName: String = pServiceName.getOrElse("rest-client-" + RandomNameGenerator.haiku)

  override def init(): Unit = {}

  override def shutdown(): Unit = {}

  def get(accessPointUri: String): Future[HttpResponse] = {
    Http().singleRequest(Get(accessPointUri))
  }

  def get(accessPointUri: String, header: Map[String, String], params: Map[String, String]): Future[HttpResponse] = {
    val rawHeader = header.map(kv => RawHeader(kv._1, kv._2)).toSeq

    val response = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.GET,
        uri = Uri(s"$accessPointUri").withQuery(Query(params)),
        entity = params.toString()
      ).withHeaders(rawHeader)
    )

    val statusProcessing = showStatus(response, accessPointUri, params = Some(params), header = Some(header), httpMethod = "get")

    statusProcessing.onComplete {
      case Success(_) => notify(NotificationValues.UPDATE, Some(s"get $accessPointUri completed"))
      case Failure(err) => {
        logger.error(s"There was a problem while requesting $serviceName.", Some(err))
        notifyError(s"There was a problem while requesting $serviceName.", Some(err))

      }
    }
    statusProcessing
  }

  def post(url: String, formData: Map[String,String]): Future[HttpResponse] = {
    val part = FormData(formData).toEntity
    val response = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        uri = url,
      ).withEntity(part)
    )

    val statusProcessing = showStatus(response, url, formData = Some(formData), httpMethod = "post")

    statusProcessing.onComplete {
      case Success(_) => notify(NotificationValues.UPDATE, Some(s"post $url completed"))
      case Failure(err) => {
        logger.error(s"There was a problem while requesting $serviceName.", Some(err))
        notifyError(s"There was a problem while requesting $serviceName.", Some(err))
      }
    }

    statusProcessing
  }

  def showStatus(response: Future[HttpResponse], uri: String, params:  Option[Map[String, String]] = None, header: Option[Map[String, String]] = None,
                 formData: Option[Map[String, String]] = None, httpMethod: String): Future[HttpResponse] = {
    httpMethod.toUpperCase() match {
      case "GET" => response.map(r => showStatusGet(r, uri, params, header)).flatten
      case "POST" => response.map(r => showStatusPost(uri, r, formData)).flatten
      case _ => response
    }
  }

  protected def showStatusPost(url: String, response: HttpResponse, formData: Option[Map[String, String]]) = {
    response.status.intValue() match {
      case 429 => {
        val retryAfter = response.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        notify(NotificationValues.UPDATE, Some(s"code ${response.status.value}/${response.status.reason()}...Waiting for $retryAfter ms before retry"))
        akka.pattern.after(retryAfter.millisecond)(post(url, formData.get))

      }
      case 401 => {
        notifyError(s"code ${response.status.intValue()}/${response.status.reason()}...we will retry-after ${10000}")
        akka.pattern.after(10000.millisecond)(post(url, formData.get))
      }
      case 400 => {
        notifyError(s"code ${response.status.intValue()}/${response.status.reason()}...we will retry-after ${60000 * 5}")
        akka.pattern.after((60000 * 5).millisecond)(post(url, formData.get))
      }
      case _ => {
          notify(NotificationValues.UPDATE, Some(s"Code ${response.status.intValue()}/${response.status.reason()}. Token access success"))
          Future(response)
      }
    }
  }

  protected def showStatusGet(response: HttpResponse, uri: String, params:  Option[Map[String, String]], header: Option[Map[String, String]]) = {
    response.status.intValue() match {
      case 200 => {
        notify(NotificationValues.UPDATE, Some(s"code : 200"))
        Future(response)
      }
      case 401 => {
        logger.error(s"code ${response.status.intValue()}/${response.status.reason()}")
        notifyError(s"code ${response.status.intValue()}/${response.status.reason()}")
        Future(response)
      }
      case 429 => {
        val retryAfter = response.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        logger.info(s"code ${response.status.value}/${response.status.reason()}...Waiting for $retryAfter ms before retry")
        notify(NotificationValues.UPDATE, Some(s"code ${response.status.value}/${response.status.reason()}...Waiting for $retryAfter ms before retry"))
         akka.pattern.after(retryAfter.millisecond)(get(uri, params.getOrElse(Map.empty), header.getOrElse(Map.empty)))
      }
      case 400 => {
        logger.error(s"code ${response.status.intValue()}/${response.status.reason()}")
        notifyError(s"code ${response.status.intValue()}/${response.status.reason()}")
        Future(response)
      }
      case nbr if (nbr.toString.contains("3")) => {
        val newurl : String = response.headers.filter(_.is("location")).map(_.value()).mkString
        if (!newurl.isEmpty) {
          logger.info(s"code ${response.status.intValue()}/${response.status.reason()}")
          notify(NotificationValues.UPDATE, Some(s"code ${response.status.value}/${response.status.reason()}...redirecting $newurl"))
          get(newurl)
        }
        else {
          logger.error(s"code ${response.status.intValue()}/${response.status.reason()}")
          notifyError(s"code ${response.status.intValue()}/${response.status.reason()}...url empty")
          Future(response)
        }
      }
      case _ => {
        logger.error(s"code ${response.status.intValue()}/${response.status.reason()}")
        notifyError(s"code ${response.status.intValue()}/${response.status.reason()}")
        Future(response)
      }
    }
  }
}
