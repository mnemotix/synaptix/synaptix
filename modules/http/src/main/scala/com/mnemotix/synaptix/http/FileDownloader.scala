/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.http

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.http.scaladsl.model.HttpEntity.ChunkStreamPart
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpRequest, HttpResponse, IllegalUriException, StatusCodes, Uri}
import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.scaladsl.{FileIO, Sink, Source}
import akka.util.ByteString
import com.mnemotix.synaptix.cache.RocksDBStore
import akka.http.javadsl.model.headers.ContentDisposition

import java.io.File
import java.net.URL
import java.nio.file.{Path, Paths}
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.Try
import sys.process._

class FileDownloader()(implicit system: ActorSystem, ec: ExecutionContext) {

  def downloadFileToPath(uri: String, path: String): String = {
    val urlObject = new URL(uri)
    val filePath = path + "/" + urlObject.getPath().replaceAll("/", "")
    urlObject #> new File(filePath)!!
  }

  def downloadFileAsync(uri: String): Future[HttpResponse] = {
    val request = Get(Uri(uri))
    val responseFuture: Future[HttpResponse] = Http().singleRequest(request).map {
      case okResponse @ HttpResponse(StatusCodes.OK, _, _, _) =>
        HttpResponse(
          entity = HttpEntity.Chunked(ContentTypes.`application/octet-stream`,
            okResponse
              .entity
              .dataBytes
              .map(ChunkStreamPart.apply)
          ))

      case nokResponse @ HttpResponse(_, _, _, _) =>
        nokResponse
    }
    responseFuture
  }

  def downloadFileAsyncToFile(uri: String, fileLocation: String): Future[IOResult] = {
    val httpResponse = downloadFileAsync(uri)
    httpResponse.flatMap { response =>
      val file = destinationFile(fileLocation)
      val source = response.entity.dataBytes
      source.runWith(FileIO.toPath(file))
    }
  }

  def downloadFileAsyncToCache[A <: RocksDBStore](uri: String, store: A): Future[Done] = {
    val httpResponse = downloadFileAsync(uri)
    httpResponse.flatMap { response =>
       response.entity.dataBytes.map(bs => store.put(uri, bs.toArray)).runWith(Sink.ignore)
    }
  }

  private def destinationFile(fileLoc: String): Path = {
    val file = new File(fileLoc)
    file.toPath
  }

  @deprecated
  def downloadFileToCache[A <: RocksDBStore](fileURI: String, store: A)(implicit system: ActorSystem, mat:ActorMaterializer, ec: ExecutionContext): Future[Done] = {
    try {
      val request = HttpRequest(uri = Uri(fileURI))
      val source = Source.single((request, ()))
      val requestResponseFlow = Http().superPool[Unit]()

      val parallelism = 1000000

      source.via(requestResponseFlow)
        .map(responseOrFail)
        .map(_._1)
        .mapAsyncUnordered(parallelism)(writeToStore(store, fileURI))
        .runWith(Sink.ignore)

    }
    catch {
      case t: IllegalUriException => throw DownloadException("Illegal Uri", Some(t))
      case t: DownloadException => throw DownloadException("An error occurred during the download process", Some(t))
    }
  }

  @deprecated
  def downloadFileToCache[A <: RocksDBStore](fileURI: Seq[String], store: A, parallelism: Int)(implicit system: ActorSystem, mat:ActorMaterializer, ec: ExecutionContext): Future[Done] = {
    try {
      val insertNbr = parallelism * 2
      Source.fromIterator(() => fileURI.iterator).mapAsync(parallelism) { uri =>
        val request = HttpRequest(uri = Uri(uri))
        val source = Source.single((request, ()))
        val requestResponseFlow = Http().superPool[Unit]()
        val dwnldPar = 1000000
        source.via(requestResponseFlow)
          .map(responseOrFail)
          .map(_._1)
          .mapAsyncUnordered(dwnldPar)(writeToStore(store, uri))
          .runWith(Sink.ignore)
      }.runWith(Sink.ignore)

    }
    catch {
      case t: IllegalUriException => throw DownloadException("Illegal Uri", Some(t))
      case t: DownloadException => throw DownloadException("An error occurred during the download process", Some(t))
    }
  }

  @deprecated
  def downloadFileToDisk(fileURI: String, saveToFile: String)(implicit system: ActorSystem, mat:ActorMaterializer, ec: ExecutionContext): Future[Done] = {
    try {
      val request = HttpRequest(uri = Uri(fileURI))
      val source = Source.single((request, ()))
      val requestResponseFlow = Http().superPool[Unit]()

      val parallelism = 1000000

      source.via(requestResponseFlow)
        .map(responseOrFail)
        .map(_._1)
        .mapAsyncUnordered(parallelism)(writeFile(saveToFile))
        .runWith(Sink.ignore)
    }
    catch {
      case t: IllegalUriException => throw DownloadException("Illegal Uri", Some(t))
      case t: DownloadException => throw DownloadException("An error occurred during the download process", Some(t))
    }
  }

  @deprecated
  def downloadFileToDisk(fileUriLoc: Map[String, String], parallelism: Int)(implicit system: ActorSystem, mat:ActorMaterializer, ec: ExecutionContext) = {
    try {
      Source.fromIterator(() => fileUriLoc.iterator).mapAsync(parallelism) { couple =>
        val request = HttpRequest(uri = Uri(couple._1))
        val source = Source.single((request, ()))
        val requestResponseFlow = Http().superPool[Unit]()
        val dwnldPar = 1000000
        source.via(requestResponseFlow)
          .map(responseOrFail)
          .map(_._1)
          .mapAsyncUnordered(dwnldPar)(writeFile(couple._2))
          .runWith(Sink.ignore)
      }.runWith(Sink.ignore)
    }
    catch {
      case t: IllegalUriException => throw DownloadException("Illegal Uri", Some(t))
      case t: DownloadException => throw DownloadException("An error occurred during the download process", Some(t))
    }
  }

  private def responseOrFail[T](in: (Try[HttpResponse], T)): (HttpResponse, T) = in match {
    case (responseTry, context) => (responseTry.get, context)
  }

  @deprecated
  private def writeFile(saveToFile: String)(httpResponse : HttpResponse)(implicit mat:ActorMaterializer): Future[IOResult] = {
    val file = Paths.get(saveToFile)
    httpResponse.entity.dataBytes.runWith(
      FileIO.toPath(file)
    )
  }

  @deprecated
  private def writeToStore[A <: RocksDBStore](store: A, id: String)(httpResponse : HttpResponse)(implicit ec: ExecutionContext,mat:ActorMaterializer)  = {
  httpResponse.entity.dataBytes.map(bs => store.put(id, bs.toArray)).runWith(Sink.ignore)
  }

  @deprecated
  private def writeToStore[A <: RocksDBStore](store: A, id: String, groupNbr: Int)(httpResponse : HttpResponse)(implicit ec: ExecutionContext,mat:ActorMaterializer)  = {
    httpResponse.entity.dataBytes.map(bs => (id, bs)).grouped(groupNbr).map{ grp:Seq[(String, ByteString)] =>
      store.bulkInsert(
        grp.map {
          entry => (entry._1, entry._2.toArray)
        }
      )
    }.runWith(Sink.ignore)
  }

}

