/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.index

import com.mnemotix.synaptix.index.elasticsearch.models.MnxConcept

object PercolationTestUtils {

  val conceptIdxMapng ="""{
                         |      "properties": {
                         |        "altLabel": {
                         |          "type": "text",
                         |          "fields": {
                         |            "keyword": {
                         |              "type": "keyword",
                         |              "ignore_above": 256
                         |            }
                         |          }
                         |        },
                         |        "broader": {
                         |          "type": "keyword"
                         |        },
                         |        "entityId": {
                         |          "type": "keyword"
                         |        },
                         |        "hiddenLabel": {
                         |          "type": "keyword",
                         |          "ignore_above": 256
                         |        },
                         |        "prefLabel": {
                         |          "type": "text",
                         |          "fields": {
                         |            "keyword": {
                         |              "type": "keyword",
                         |              "ignore_above": 256
                         |            }
                         |          }
                         |        },
                         |        "percoLabel": {
                         |          "type": "text",
                         |          "fields": {
                         |            "keyword": {
                         |              "type": "keyword",
                         |              "ignore_above": 256
                         |            }
                         |          }
                         |        },
                         |        "query": {
                         |          "type": "percolator"
                         |        },
                         |        "related": {
                         |          "type": "keyword"
                         |        }
                         |      }
                         |    }""".stripMargin

  val concept = MnxConcept(
    Some("www.example.com/data#1A"),
    Some("www.example.com/data#5"),
    Some(Seq("www.example.com/data#1B")),
    Some(Seq("Chargé d'affaires en environnement et énergie")),
    Some(Seq(""""+"chargé d'affaires environnement énergie"~3"""")),
    Some(Seq("Chargé d'affaires en environnement et énergie"))
  )

  val concept2 =
    MnxConcept(
      Some("www.example.com/data#4B"),
      Some("www.example.com/data#4"),
      None,
      Some(Seq("Architecte d'intérieur")),
      Some(Seq(""""+"Architecte d'intérieur"~3"""")),
      Some(Seq("Architecte d'intérieur"))
    )

  val concept3 =
    MnxConcept(
      Some("www.example.com/data#5"),
      None,
      None,
      Some(Seq("Informatique windows")),
      Some(Seq(""""+"Informatique windows"~3"""")),
      Some(Seq("Informatique window"))
    )
}
