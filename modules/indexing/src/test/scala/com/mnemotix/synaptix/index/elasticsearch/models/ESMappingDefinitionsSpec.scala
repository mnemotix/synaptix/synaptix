/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.index.elasticsearch.models

import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 09/03/2020
 */

class ESMappingDefinitionsSpec extends AnyWordSpec with Matchers {
  "ESMappingDefinitions" should {
    "parse JSON and transform it to MappingDefinition" in {
      val esmap = ESMappingDefinitions(
        Json.parse(
          """
            |{
            |    "properties": {
            |      "bio": {
            |        "type": "text",
            |        "analyzer": "standard"
            |      },
            |      "birthdate": {
            |        "type": "date"
            |      },
            |      "email": {
            |        "type": "keyword"
            |      },
            |      "fullName": {
            |        "type": "text",
            |        "analyzer": "standard"
            |      }
            |    }
            |  }
            |""".stripMargin))
      val mappingDef = esmap.toMappingDefinition()
      mappingDef.isInstanceOf[MappingDefinition] shouldBe true
    }
  }

}
