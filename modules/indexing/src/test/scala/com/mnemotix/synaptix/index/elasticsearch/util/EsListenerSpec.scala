/**
  * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.index.elasticsearch.util

import com.mnemotix.synaptix.SynaptixTestSpec

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class EsListenerSpec extends SynaptixTestSpec {

  "EsListener" should {
    implicit val executionContext: ExecutionContext = ExecutionContext.global

    "create an index" in {
      val esListener = new EsListener("1", "testIndexJob", "synaptixjob", "synaptix")
      val rs = Await.result(esListener.init(), Duration.Inf)
    }

    "create a startup message" in {
      val esListener = new EsListener("1", "testIndexJob", "synaptixjob", "synaptix")
      esListener.onStartup()
    }

    "create an update message" in {
      val esListener = new EsListener("1", "testIndexJob", "synaptixjob", "synaptix")
      esListener.onUpdate("test index Job is Updated")
    }

    "create an progress message" in {
      val esListener = new EsListener("1", "testIndexJob", "synaptixjob", "synaptix")
      esListener.onProgress(90, Some(100), Some("updated with progress"))
    }

    "create an done message" in {
      val esListener = new EsListener("1", "testIndexJob", "synaptixjob", "synaptix")
      esListener.onDone()
    }
  }

}
