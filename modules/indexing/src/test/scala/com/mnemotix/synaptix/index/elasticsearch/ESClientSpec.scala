/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.index.elasticsearch

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions, RawQuery, User}
import com.mnemotix.synaptix.index.{Fixtures, IndexClient, PercolationTestUtils}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import play.api.libs.json.{JsObject, Json}
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}


/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-05-13
 */

class ESClientSpec extends SynaptixTestSpec {

  val indexName = "person-" + System.currentTimeMillis

  val percolatorIdxName = "percolator-" + System.currentTimeMillis

  implicit val executionContext: ExecutionContext = ExecutionContext.global

  "ESClient" should {

    "connect to its server without error" in {
      IndexClient.init()
    }
    "list indices" in {
      val response = Await.result(IndexClient.listIndices(), 1.minutes)
      println(response.body)
      response.isSuccess shouldBe true
    }
    "check existence of an index" in {
      Await.result(IndexClient.indexExists(indexName), 1.minutes).isSuccess shouldBe true
    }
    "drop an index without error" in {
      if (Await.result(IndexClient.indexExists(indexName), 1.minutes).result.exists) Await.result(IndexClient.deleteIndex(indexName), 1.minutes).isSuccess shouldBe true
    }
    "create an index with proper parameters extracted from configuration" in {
      val mappngDef = ESMappingDefinitions(
        Json.parse(
          """
            |{
            |    "properties": {
            |      "bio": {
            |        "type": "text",
            |        "analyzer": "standard"
            |      },
            |      "birthdate": {
            |        "type": "date"
            |      },
            |      "email": {
            |        "type": "keyword"
            |      },
            |      "fullName": {
            |        "type": "text",
            |        "analyzer": "standard"
            |      }
            |    }
            |  }
            |""".stripMargin)
      )
      Await.result(IndexClient.createIndex(indexName, mappngDef.toMappingDefinition()), 1.minutes).isSuccess shouldBe true
      Thread.sleep(2000)
      Await.result(IndexClient.indexExists(indexName), 1.minutes).result.exists shouldBe true
    }
    "index a document according to its mapping" in {
      Await.result(IndexClient.insert(indexName, Fixtures.getPerson("person:0", "John Doe", "john.doe@fake.com")), 1.minutes).isSuccess shouldBe true
    }
    "bulk insert documents" in {
      Await.result(IndexClient.bulkInsert(indexName, Fixtures.getPersons(200): _*), 1.minutes).isSuccess shouldBe true
    }
    "count number of documents of a type" in {
      Thread.sleep(3000)
      Await.result(IndexClient.count(indexName = indexName), 1.minutes).result.count shouldBe 201
    }
    "update a document already indexed without error" in {
      Await.result(IndexClient.updateById(indexName, Fixtures.getPerson("person:0", "John Doe", "john.doe@fakemail.com")), 1.minutes).isSuccess shouldBe true
    }
    "query documents" in {
      val resp = Await.result(IndexClient.queryString(indexName, "Doe"), 1.minutes)
      resp.isSuccess shouldBe true
      resp.result.hits.hits.size shouldEqual 1
    }
    "raw query documents" in {
      val resp:Response[SearchResponse] = IndexClient.rawQuery(RawQuery(Seq(indexName), Json.parse(
        """
          |{
          | "query": {
          |  "match_all": {}
          | },
          | "_source": {
          |  "includes": "*"
          | },
          | "from": 0,
          | "size": 11,
          | "sort": [
          |  {
          |   "birthdate": "asc"
          |  },
          |  "_score"
          | ]
          |}
          |""".stripMargin).as[JsObject])).value.get.get

      println(resp)
      resp.isSuccess shouldBe true
      resp.result.hits.hits.size shouldBe > (0)
    }
    "add an alias" in {
      IndexClient.addAlias(indexName, "myalias").value.get.get
    }
    "a scroll" in {
      val resp1: Response[SearchResponse] = IndexClient.scrollQuery(Seq(indexName), Some("1m"),2, "email").value.get.get
      val users =resp1.result.hits.hits.map{h =>
        val json = Json.parse(h.sourceAsString)
        json.as[User]
      }
      users.foreach(println(_))
      users.size shouldEqual 2
      resp1.result.hits.size shouldEqual(2)

      val resp2 = IndexClient.searchScroll(resp1.result.scrollId.get, None).value.get.get
      println(resp2)
      resp1.result.hits.size shouldEqual(2)
    }
    "remove an alias" in {
      IndexClient.removeAlias(indexName, "myalias").value.get.get
    }
    //    "drop an index without error" ignore {
    //      client.deleteIndex().futureValue shouldBe true
    //      client.indexExists().futureValue shouldBe false
    //    }
    "create an index for the percolator query" in {
      IndexClient.createIndex(percolatorIdxName, ESMappingDefinitions(Json.parse(PercolationTestUtils.conceptIdxMapng)).toMappingDefinition()).value.get.get.isSuccess shouldBe true
      Thread.sleep(2000)
      IndexClient.indexExists(percolatorIdxName).value.get.get.result.exists shouldBe true
    }
    "insert concept in the percolator index" in {
      IndexClient.insert(percolatorIdxName,PercolationTestUtils.concept.entityId.get, Json.toJson(PercolationTestUtils.concept).as[JsObject]).value.get.get.isSuccess shouldBe true
      IndexClient.insert(percolatorIdxName,PercolationTestUtils.concept2.entityId.get, Json.toJson(PercolationTestUtils.concept2).as[JsObject]).value.get.get.isSuccess shouldBe true
      IndexClient.insert(percolatorIdxName,PercolationTestUtils.concept3.entityId.get, Json.toJson(PercolationTestUtils.concept3).as[JsObject]).value.get.get.isSuccess shouldBe true
    }
    "bulk update concept in the percolator index" in {
      Thread.sleep(1000)
      val indexables = Seq(ESIndexable(PercolationTestUtils.concept.entityId.get, Some(Json.parse("""{"broader":"www.example.com/data#b5"}""").as[JsObject])),
        ESIndexable(PercolationTestUtils.concept2.entityId.get, Some(Json.parse("""{"broader":"www.example.com/data#4broader"}""").as[JsObject]))
      )
      IndexClient.bulkUpdate(percolatorIdxName,indexables:_*).value.get.get.isSuccess shouldBe true
    }
    "it should register a percolate query string" in {
      Thread.sleep(3000)
      IndexClient.registerQueryString(percolatorIdxName,
        PercolationTestUtils.concept2.entityId.get,
        """("+"architecte d'intérieur"~3") OR (architecte d'intérieur) OR (architecte d'intérieur)""",
        Some("percoLabel")).value.get.get.isSuccess shouldBe true

      IndexClient.registerQueryString(percolatorIdxName,
        PercolationTestUtils.concept3.entityId.get,
        """(Informatique window)""",
        Some("percoLabel")).value.get.get.isSuccess shouldBe true

      IndexClient.registerQueryString(percolatorIdxName,
        PercolationTestUtils.concept.entityId.get,
        """(Chargé d'affaires en environnement et énergie)""",
        Some("percoLabel")).value.get.get.isSuccess shouldBe true
    }
    "it should register a percolate raw query" in {
      Thread.sleep(3000)
      IndexClient.registerRawQuery(percolatorIdxName, RawQuery(
        Seq(percolatorIdxName),
        Json.parse(
          """
            |{
            | "query": {
            |   "query_string": {
            |     "query": "(architecte) AND (intérieur)",
            |     "default_field": "percoLabel"
            |   }
            | }
            |}
            |""".stripMargin).as[JsObject]
      ))
    }
    "it should do a raw to get percolator documents" in {
      Thread.sleep(3000)
      val percolatorQuery =
        """
          |{
          | "query": {
          |  "match_all": {}
          | }
          |}
          |""".stripMargin

      val obj = Json.parse(percolatorQuery).as[JsObject]
      val fut = IndexClient.rawQuery(RawQuery(Seq(percolatorIdxName), obj)).value.get.get

      fut.result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        //println(s"${hit.id} --- $json --- ${hit.score}")
      }
      //println(fut.result.totalHits)
      fut.isSuccess shouldBe true
    }
    "it should do a multi raw search" in {
      /*   val rawQuery = RawQuery(Seq(percolatorIdxName), Json.parse(
           """
             |{
             |"query": {
             |    "bool": {
             |      "should": [
             |        { "match": { "prefLabel": { "query": "shay", "_name": "first" } } }
             |      ],
             |      "filter": {
             |        "terms": {
             |          "prefLabel": [ "Chargé d'affaires en environnement et énergie" ],
             |          "_name": "test"
             |        }
             |      }
             |    }
             |  }
             |}
             |""".stripMargin).as[JsObject]) */

      val rawQuery2 = RawQuery(Seq(percolatorIdxName), Json.parse(
        """
          |{
          | "query": {
          |  "match": {
          |   "prefLabel": {
          |      "query" : "Architecte d'intérieur",
          |      "_name": "second"
          |     }
          |  }
          | }
          |}
          |""".stripMargin).as[JsObject])

      val seqRwQry = Seq(rawQuery2)
      val fut = IndexClient.multiRawQuery(seqRwQry).value.get.get
      fut.result.items.map { item =>
        if (item.response.isLeft) {
          println("is Error")
        }
        else {
          item.response.right.get.hits.hits.map { hit =>
            hit.matchedQueries.toSeq.flatten.foreach(println(_))
            val json = Json.parse(hit.sourceAsString)
            println(s"${hit.id} --- $json --- ${hit.score}")
          }
        }
      }
      fut.result.size should be > 0
    }

    "it should test a percolator query" in {
      val percolatorQuery =
        """
          |{
          | "from": 0,
          | "size": 20,
          | "query": {
          |   "percolate": {
          |     "field": "query",
          |     "document": {
          |       "percoLabel": "Nous recherchons un architecte pour l'intérieur dans notre agence"
          |     },
          |     "name": "id_324"
          |   }
          | }
          |}
          |""".stripMargin


      val obj = Json.parse(percolatorQuery).as[JsObject]
      val fut = IndexClient.rawQuery(RawQuery(Seq(percolatorIdxName), obj)).value.get.get
      fut.result.hits.hits.map { hit =>
        hit.matchedQueries.toSeq.flatten.foreach(println(_))
        val json = Json.parse(hit.sourceAsString)
        println(s"${hit.id} --- $json --- ${hit.score}")
      }
    }

    "it should do a percolate query" in {
      val fut = Await.result(IndexClient.percolate(Seq(percolatorIdxName),
        Map("percoLabel" -> "Nous recherchons un architecte d'intérieur dans notre agence. L'architecte aura pour mission de réaliser l'intérieur des gens"), Some(0), Some(2)), 2.minutes )
      fut.result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        println(s"${hit.id} --- $json --- ${hit.score}")
      }
      fut.isSuccess shouldBe true
      fut.result.totalHits should be > 0L
    }
    "it should do a multiple percolate query" in {
      val documents = Seq(Map("percoLabel" -> "Nous recherchons un architecte d'intérieur dans notre agence. L'architecte aura pour mission de réaliser l'intérieur des gens"),
        Map("percoLabel" ->  "Nous sommes à la recherche active d'un chargé d'affaires en environnement et énergie"),
        Map("percoLabel" -> "chargé d'affaires en environnement et énergie et architecte d'intérieur"),
        Map("percoLabel" -> "informatique windows")
      )

      val fut = Await.result( IndexClient.multiPercolate(Seq(percolatorIdxName),
        documents, Some(0), Some(20)), 2.minutes )

      fut.result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        println(hit.fields("_percolator_document_slot"))
        val h = hit.fields("_percolator_document_slot").asInstanceOf[Seq[Int]]

        h.foreach(println(_))

        println(s"${hit.id} --- $json --- ${hit.score}")
      }

      fut.isSuccess shouldBe true

    }
    "it should do a multiple documents percolate raw query" in {
      val percolatorQuery =
        """
          |{
          | "from": 0,
          | "size": 20,
          |"query": {
          |    "percolate": {
          |      "field": "query",
          |      "documents": [
          |        {
          |          "percoLabel": "Nous recherchons un architecte d'intérieur dans notre agence. L'architecte aura pour mission de réaliser l'intérieur des gens"
          |        },
          |        {
          |          "percoLabel": "Nous sommes à la recherche active d'un chargé d'affaires en environnement et énergie"
          |        },
          |        {
          |          "percoLabel": "chargé d'affaires en environnement et énergie et architecte d'intérieur"
          |        },
          |        {
          |          "percoLabel": "informatique windows"
          |        }
          |      ]
          |    }
          |  }
          |}
          |""".stripMargin

      val obj = Json.parse(percolatorQuery).as[JsObject]
      val fut = Await.result(  IndexClient.rawQuery(RawQuery(Seq(percolatorIdxName), obj)), 2.minutes )

      fut.result.hits.hits.map { hit =>
        println(hit.fields("_percolator_document_slot"))
        hit.matchedQueries.toSeq.flatten.foreach(println(_))
        val json = Json.parse(hit.sourceAsString)
        println(s"${hit.id} --- $json --- ${hit.score}")

      }
    }
    "it should specify multiple percolate queries" in {
      val percolatorQuery =
        """
          |{
          |  "query": {
          |    "bool": {
          |      "should": [
          |        {
          |          "percolate": {
          |            "field": "query",
          |            "document": {
          |             "percoLabel": "Nous recherchons un architecte d'intérieur dans notre agence. L'architecte aura pour mission de réaliser l'intérieur des gens"
          |            },
          |            "name": "query1"
          |          }
          |        },
          |        {
          |          "percolate": {
          |            "field": "query",
          |            "document": {
          |               "percoLabel": "Nous sommes à la recherche active d'un Chargé d'affaires en environnement et énergie"
          |            },
          |            "name": "query2"
          |          }
          |        },
          |        {
          |          "percolate": {
          |            "field": "query",
          |            "document": {
          |               "percoLabel": "chargé d'affaires en environnement et énergie et architecte d'intérieur"
          |            },
          |            "name": "query3"
          |          }
          |        },
          |        {
          |        "percolate": {
          |            "field": "query",
          |            "document": {
          |               "percoLabel": "informatique windows"
          |            },
          |            "name": "query4"
          |          }
          |        }
          |      ]
          |    }
          |  }
          |}
          |""".stripMargin

      val obj = Json.parse(percolatorQuery).as[JsObject]
      val fut = Await.result(IndexClient.rawQuery(RawQuery(Seq(percolatorIdxName), obj)), 2.minutes)

      fut.result.hits.hits.map { hit =>
        println(hit.fields.foreach(println(_)))
        hit.matchedQueries.toSeq.flatten.foreach(println(_))
        val json = Json.parse(hit.sourceAsString)
        println(s"${hit.id} --- $json --- ${hit.score}")
      }
    }

    "disconnect without error" in {
      IndexClient.shutdown()
    }
  }
}
