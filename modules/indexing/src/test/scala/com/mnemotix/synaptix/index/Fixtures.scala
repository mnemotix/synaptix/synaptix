/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.index

import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import org.joda.time.LocalDate
import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-15
  */

object Fixtures {

  def getPerson(id: String, fullName: String, email: String): ESIndexable = ESIndexable(id, Some(Json.obj(
    "fullName" -> fullName,
    "bio" -> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam lobortis congue sodales. Sed sed dui in odio convallis congue at in arcu. \nNunc imperdiet ligula metus. Nam odio lorem, condimentum ac nulla ut, imperdiet pulvinar augue. Fusce nec laoreet ex. Nullam congue vel nibh id porttitor. \nProin et magna tempor ex sodales vehicula. Cras maximus, neque quis aliquam mattis, odio lectus gravida mauris, et posuere orci erat id ligula. Donec eu purus semper, pharetra nunc eu, cursus dui. Phasellus in porta justo. Nunc non lectus bibendum, vulputate nulla non, tincidunt sem. Aenean non convallis ipsum. Maecenas ultrices vitae ante nec scelerisque. Mauris nec posuere ipsum. Integer efficitur libero ac ante tempor fermentum.",
    "email" -> email,
    "birthdate" -> LocalDate.now.minusDays(Math.round(Math.random() * 200).toInt).toString
  )))

  def getPersons(nbPerson: Int) = (1 to nbPerson).map { i => getPerson(s"person:$i", s"Person $i", s"person$i@fake.com") }

}
