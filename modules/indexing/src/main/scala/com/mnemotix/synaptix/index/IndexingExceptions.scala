/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.index

import com.mnemotix.synaptix.core.SynaptixException

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/02/2021
 */

abstract class IndexingException(message: String, cause: Option[Throwable]=None) extends SynaptixException(message, cause)

case class IndexSearchException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexCountException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexScrollException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexDeleteException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexUpdateException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexInsertException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexAliasException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexExistsException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class IndexCreateException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class CatIndicesException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)

case class PercoRegisterException(message: String, cause: Option[Throwable]=None) extends IndexingException(message, cause)
