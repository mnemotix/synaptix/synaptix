/**
  * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.index.elasticsearch.util

import akka.Done
import com.mnemotix.synaptix.core.ServiceListener
import com.mnemotix.synaptix.core.utils.CryptoUtils
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions, ListenerMessage}
import play.api.libs.json.{JsObject, Json}

import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
import java.time.{Instant, LocalDate, LocalDateTime, ZoneId}
import java.util.Date
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class EsListener(jobId: String, jobServiceName: String, indexName: String, project: String, categoy: Option[String]=None)(implicit ec: ExecutionContext) extends ServiceListener {

  val docID = CryptoUtils.md5sum(jobId+"_"+jobServiceName+"_"+project)

  /*
  val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern(""yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"").withZone(ZoneId.systemDefault())
  val date = DATE_TIME_FORMATTER.format(new Date().toInstant())
  val started_at: LocalDate = LocalDate.parse(date, DATE_TIME_FORMATTER)
   */

  val zid = ZoneId.of("Europe/Paris")
  val DATE_TIME_FORMATTER: DateTimeFormatter = ISO_LOCAL_DATE_TIME
  val started_at: String = DATE_TIME_FORMATTER.format(LocalDateTime.now())
  //val started_at: LocalDate = LocalDate.parse(date, DATE_TIME_FORMATTER)

  val categoryToInsert = if (categoy.isDefined) Some(categoy.get) else None

  val esmap: ESMappingDefinitions = ESMappingDefinitions(
    Json.parse(
      """
        |{
        |    "properties": {
        |      "id": {
        |        "type": "keyword"
        |      },
        |      "name": {
        |        "type": "text",
        |        "analyzer": "standard"
        |      },
        |      "started_at": {
        |        "type": "date",
        |        "format": "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        |      },
        |      "ended_at": {
        |        "type": "date",
        |        "format": "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        |      },
        |      "updated_at": {
        |        "type": "date",
        |        "format": "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        |      },
        |      "project": {
        |        "type": "text",
        |        "analyzer": "standard"
        |      },
        |      "status": {
        |         "type": "keyword"
        |      },
        |      "message": {
        |         "type": "text",
        |         "analyzer": "standard"
        |      },
        |      "processed": {
        |         "type": "integer"
        |      },
        |      "total": {
        |         "type": "integer"
        |      },
        |      "category": {
        |         "type": "text",
        |         "analyzer": "standard"
        |      }
        |    }
        |  }
        |""".stripMargin))

  def init(): Future[Done] = {
    IndexClient.init()
    IndexClient.indexExists(indexName).flatMap { response =>
      if (response.isSuccess) {
        if (!response.result.exists) IndexClient.createIndex(indexName,esmap.toMappingDefinition()).map { _ => Done.done()}
        else Future(Done.done())
      }
      else {
        logger.info(s"Unable to start EsListener. Retrying...")
        init()
      }
    }
  }

  override def onStartup(): Unit = {
    if (!IndexClient.connect()) IndexClient.init()
    val listenerMessage = ListenerMessage(jobServiceName, started_at , None, jobId, project, "started", Some(s"${jobServiceName} has started"), None, None, None, categoryToInsert)
    IndexClient.insert(indexName, docID, Json.toJson(listenerMessage).as[JsObject]).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onTerminate(): Unit = {
    if (!IndexClient.connect()) IndexClient.init()
    val listenerMessage = ListenerMessage(jobServiceName, started_at, None, jobId, project, "terminated", Some(s"Termination signal was sent to ${jobServiceName}"), None, None, None, categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onShutdown(): Unit = {
    if (!IndexClient.connect()) IndexClient.init()
    val listenerMessage = ListenerMessage(jobServiceName, started_at, None, jobId, project, "shutdown", Some(s"Shutdown signal was sent to ${jobServiceName}"), None, None, None, categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onAbort(): Unit = {
    if (!IndexClient.connect()) IndexClient.init()
    val ended_at: String = DATE_TIME_FORMATTER.format(LocalDateTime.now())
    val listenerMessage = ListenerMessage(jobServiceName, started_at, Some(ended_at), jobId, project, "aborted", Some(s"Abortion signal was sent to job system"), None, None, None, categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onUpdate(message: String): Unit = {
    val updated_at: String = DATE_TIME_FORMATTER.format(LocalDateTime.now())
    if (!IndexClient.connect()) IndexClient.init()
    val listenerMessage = ListenerMessage(jobServiceName, started_at, None, jobId, project, "updated", Some(s"$message"), None, None, Some(updated_at), categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onProgress(processedItems: Int, totalItems: Option[Int], message: Option[String]): Unit = {
    val updated_at: String = DATE_TIME_FORMATTER.format(LocalDateTime.now())
    if (!IndexClient.connect()) IndexClient.init()
    val listenerMessage = ListenerMessage(jobServiceName, started_at, None, jobId, project, "updated", message, Some(processedItems), totalItems, Some(updated_at), categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onDone(): Unit = {
    if (!IndexClient.connect()) IndexClient.init()
    //val ended_at: LocalDate = LocalDate.parse(date, DATE_TIME_FORMATTER)
    val ended_at: String = DATE_TIME_FORMATTER.format(LocalDateTime.now())

    val listenerMessage = ListenerMessage(jobServiceName, started_at, Some(ended_at), jobId, project, "done", Some(s"${jobServiceName} has been completed."), None, None,None, categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) =>  {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

  override def onError(errorMessage: String, cause: Option[Throwable]): Unit = {
    if (!IndexClient.connect()) IndexClient.init()
    val listenerMessage = ListenerMessage(jobServiceName, started_at, None, jobId, project, "error", Some(s"$errorMessage}"), None, None, None, categoryToInsert)
    IndexClient.updateById(indexName, ESIndexable(docID, Some(Json.toJson(listenerMessage).as[JsObject]))).onComplete {
      case Success(value) => {
        Thread.sleep(2000)
        logger.info(s"$value")
      }
      case Failure(exception) => logger.error("error", exception)
    }
  }

}
