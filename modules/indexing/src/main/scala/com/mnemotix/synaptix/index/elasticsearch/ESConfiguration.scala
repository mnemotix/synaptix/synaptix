/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.index.elasticsearch

import com.typesafe.config.{Config, ConfigFactory}
import play.api.libs.json.{JsObject, Json}

/**
  * Created by Nicolas Delaforge on 25/10/2016.
  */
object ESConfiguration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("index.es")).getOrElse(ConfigFactory.empty())
  lazy val uri: String = Option(conf.getString("master.uri")).getOrElse("http://localhost:9300")
  lazy val user: Option[String] = if (conf.hasPath("user")) Some(conf.getString("user")) else None
  lazy val passwd: Option[String] = if (conf.hasPath("passwd")) Some(conf.getString("passwd")) else None
  lazy val prefix: Option[String] = if (conf.hasPath("prefix")) Some(conf.getString("prefix")) else None

  // index name
  lazy val responseTimeout = Option(conf.getInt("response.timeout")).getOrElse(10000)
  // number of shards
  lazy val defaultShardNumber: Int = if (conf.hasPath("default.shards")) conf.getInt("default.shards") else 1

  // number of replicas
  lazy val defaultReplicas: Int = if (conf.hasPath("default.replicas")) conf.getInt("default.replicas") else 0
  lazy val defaultRefreshRate: String = if (conf.hasPath("default.refresh_rate")) conf.getString("default.refresh_rate") else "1s"
  lazy val mappingTotalFieldsLimit: Int = if (conf.hasPath("mapping.total_fields.limit")) conf.getInt("mapping.total_fields.limit") else 1000
  lazy val mappingDepthLimit: Int = if (conf.hasPath("mapping.depth.limit")) conf.getInt("mapping.depth.limit") else 20
  lazy val mappingNestedFieldsLimit: Int = if (conf.hasPath("mapping.nested_fields.limit")) conf.getInt("mapping.nested_fields.limit") else 50
  lazy val maxResultWindow: Int = if (conf.hasPath("max_result_window")) conf.getInt("max_result_window") else 10000

  def toJson():JsObject = {
    Json.obj(
      "uri" -> uri,
      "user" -> user,
      "passwd" -> passwd
    )
  }
}
