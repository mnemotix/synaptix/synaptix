/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.index.elasticsearch

import com.mnemotix.synaptix.index._
import com.mnemotix.synaptix.index.elasticsearch.models.{AltLabel, ESIndexable, HiddenLabel, MultiPercolate, MultiPercolatorQuery, MultiQueryP, PercoLabel, Percolate, Percolator, PercolatorQuery, PrefLabel, QueryP, QueryString, RawQuery, RegisterQuery, RegisterQueryMatch, RegisterQueryString}
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.cat.CatIndicesResponse
import com.sksamuel.elastic4s.requests.cluster.{ClusterHealthResponse, ClusterStateResponse}
import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.requests.count.CountResponse
import com.sksamuel.elastic4s.requests.delete.DeleteResponse
import com.sksamuel.elastic4s.requests.indexes._
import com.sksamuel.elastic4s.requests.indexes.admin._
import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import com.sksamuel.elastic4s.requests.searches._
import com.sksamuel.elastic4s.requests.update.UpdateResponse
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import play.api.libs.json.{JsObject, JsPath, JsResult, Json}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-03-20
 */

class ESClient extends AbstractIndexClient {

  lazy val client = if (ESConfiguration.user.isDefined && ESConfiguration.passwd.isDefined) {
    val esuri = ElasticProperties(ESConfiguration.uri)
    lazy val provider = {
      val provider = new BasicCredentialsProvider
      val credentials = new UsernamePasswordCredentials(ESConfiguration.user.get, ESConfiguration.passwd.get)
      provider.setCredentials(AuthScope.ANY, credentials)
      provider
    }
    ElasticClient(
      JavaClient(
        esuri,
        (requestConfigBuilder: RequestConfig.Builder) => requestConfigBuilder,
        (httpClientBuilder: HttpAsyncClientBuilder) => httpClientBuilder.setDefaultCredentialsProvider(provider)
      )
    )
  } else ElasticClient(JavaClient(ElasticProperties(ESConfiguration.uri)))

  var connected: Boolean = false

  override def config(): JsObject = ESConfiguration.toJson()

  override def init()(implicit ec: ExecutionContext) = {
    if (isVerbose && connect()) logger.info(s"Connection to cluster at URI [${ESConfiguration.uri}] successfull.")
  }

  override def connect()(implicit ec: ExecutionContext): Boolean = {
    try {
      clusterHealth().isSuccess
    } catch {
      case t: Throwable => {
        logger.info(s"Unable to connect the cluster at URI [${ESConfiguration.uri}], retrying in 5 seconds...")
        Thread.sleep(5000)
        connect()
      }
    }
  }

  override def shutdown()(implicit ec: ExecutionContext) = client.close()

  /*
   * Index admin functions
   */

  override def clusterState()(implicit ec: ExecutionContext): Response[ClusterStateResponse] = {
    client.execute {
      ElasticDsl.clusterState
    }.await
  }

  override def clusterHealth()(implicit ec: ExecutionContext): Response[ClusterHealthResponse] = {
    client.execute {
      ElasticDsl.clusterHealth
    }.await
  }

  @throws(classOf[IndexingException])
  override def createIndex(indexName: String, settings: Map[String, Any], mappingDef: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]] = {
    val request: CreateIndexRequest = ElasticDsl.createIndex(indexName).mapping(mappingDef).settings(settings)
    println(request)
    val future = client.execute {
      request
    }
    future onComplete {
      case Success(resp) => if (resp.result.acknowledged && isVerbose) logger.info(s"Index [${indexName}] was created successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to create index [${indexName}]", err)
        throw IndexCreateException(s"Unable to create index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def createIndex(indexName: String, mappingDef: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]] = createIndex(indexName, Map[String, Any](
    "number_of_shards" -> ESConfiguration.defaultShardNumber,
    "number_of_replicas" -> ESConfiguration.defaultReplicas,
    "refresh_interval" -> ESConfiguration.defaultRefreshRate,
    "max_result_window" -> ESConfiguration.maxResultWindow,
    "mapping.total_fields.limit" -> ESConfiguration.mappingTotalFieldsLimit,
    "mapping.depth.limit" -> ESConfiguration.mappingDepthLimit,
    "mapping.nested_fields.limit" -> ESConfiguration.mappingNestedFieldsLimit
  ),
    mappingDef
  )

  @throws(classOf[IndexingException])
  override def listIndices()(implicit ec: ExecutionContext): Future[Response[Seq[CatIndicesResponse]]] = {
    val future = client.execute {
      ElasticDsl.catIndices()
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to list indices.", err)
        throw CatIndicesException(s"Unable to list indices.", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def deleteIndices(indices: Seq[String])(implicit ec: ExecutionContext): Future[Response[DeleteIndexResponse]] = {
    val future = client.execute {
      ElasticDsl.deleteIndex(indices)
    }
    future onComplete {
      case Success(resp) => if (resp.result.acknowledged && isVerbose) logger.info(s"Index [${indices.mkString(",")}] was dropped successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to drop index [${indices.mkString(",")}]", err)
        throw IndexDeleteException(s"Unable to drop index [${indices.mkString(",")}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def indexExists(indexName: String)(implicit ec: ExecutionContext): Future[Response[IndexExistsResponse]] = {
    val future = client.execute {
      ElasticDsl.indexExists(indexName)
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to check existence of index [${indexName}]", err)
        throw IndexExistsException(s"Unable to check existence of index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def addAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]] = {
    val future = client.execute {
      ElasticDsl.addAlias(aliasName, indexName)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info(s"Alias [$aliasName] added successfully on index [${indexName}]")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to create an alias on index [${indexName}]", err)
        throw IndexAliasException(s"Unable to create an alias on index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def removeAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]] = {
    val future = client.execute {
      ElasticDsl.removeAlias(aliasName, indexName)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info(s"Alias [$aliasName] removed successfully on index [${indexName}]")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to remove an alias on index [${indexName}]", err)
        throw IndexAliasException(s"Unable to remove an alias on index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def moveAlias(fromIndexName: String, toIndexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]] = {
    val future = client.execute {
      aliases(ElasticDsl.removeAlias(aliasName, fromIndexName), ElasticDsl.addAlias(aliasName, toIndexName))
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info(s"Alias [$aliasName] moved successfully from index [$fromIndexName] to index [${toIndexName}]")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to move alias [$aliasName] from index [$fromIndexName] to index [${toIndexName}]", err)
        throw IndexAliasException(s"Unable to move alias [$aliasName] from index [$fromIndexName] to index [${toIndexName}]", Some(err))
      }
    }
    future
  }

  /*
   * Document indexing functions
   */
  @throws(classOf[IndexingException])
  override def bulkInsert(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext): Future[Response[BulkResponse]] = {
    val future = client.execute {
      ElasticDsl.bulk(indexables.map(doc => doc.toIndexRequest(indexName))).refresh(RefreshPolicy.WaitFor)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.debug(s"Bulk insert into index[${indexName}] was completed successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to bulk insert documents into index [${indexName}]", err)
        throw IndexInsertException(s"Unable to bulk insert documents into index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def bulkUpdate(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext): Future[Response[BulkResponse]] = {
    val future = client.execute {
      ElasticDsl.bulk(indexables.map(doc => doc.toUpdateRequest(indexName))).refresh(RefreshPolicy.WaitFor)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.debug(s"Bulk update into index[${indexName}] was completed successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to bulk update documents into index [${indexName}]", err)
        throw IndexUpdateException(s"Unable to bulk update documents into index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def insert(indexName: String, docId: String, doc: JsObject)(implicit ec: ExecutionContext): Future[Response[IndexResponse]] = {
    val future = client.execute {
      ElasticDsl.indexInto(indexName).doc(Json.stringify(doc)).withId(docId)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.debug(s"Document [$docId] indexed successfully into index[${indexName}].")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to insert a document into index [${indexName}]", err)
        throw IndexInsertException(s"Unable to insert a document into index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def updateById(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = {
    val future = client.execute {
      indexable.toUpdateRequest(indexName)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.debug(s"Document [${indexable.id}] updated successfully on index[${indexName}].")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to update a document on index [${indexName}]", err)
        throw IndexUpdateException(s"Unable to update a document on index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def deleteById(indexName: String, docId: String)(implicit ec: ExecutionContext): Future[Response[DeleteResponse]] = {
    val future = client.execute {
      ElasticDsl.deleteById(indexName, docId)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.debug(s"Document [$docId] removed successfully from index[${indexName}].")
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to delete a document into index [${indexName}]", err)
        throw IndexDeleteException(s"Unable to delete a document into index [${indexName}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def queryString(indices: Seq[String], qryStr: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val future = client.execute {
      ElasticDsl.search(indices).query(qryStr)
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to execute the query on index [${indices.mkString(",")}] : $qryStr", err)
        throw IndexSearchException(s"Unable to execute the query on index [${indices.mkString(",")}] : $qryStr", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def rawQuery(qry: RawQuery)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val future = client.execute {
      ElasticDsl.search(qry.indices).source(Json.stringify(qry.source))
    }
    future onComplete {
      case Success(resp) => {
        if(resp.isError) {
          if (isVerbose) logger.warn(s"""Server returned an error on query : ${resp.error.`type`} ${resp.error.reason} / ${Json.stringify(qry.source)}""", resp.error.asException)
          throw IndexSearchException(s"""Server returned an error on query : ${resp.error.`type`} ${resp.error.reason} / ${Json.stringify(qry.source)}""", Some(resp.error.asException))
        }
      }
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to execute the query on index [${qry.indices.mkString(",")}] : ${Json.stringify(qry.source)}", err)
        throw IndexSearchException(s"Unable to execute the query on index [${qry.indices.mkString(",")}] : ${Json.stringify(qry.source)}", Some(err))
      }
    }
    future
  }

  override def multiRawQuery(qrys: Seq[RawQuery])(implicit ec: ExecutionContext): Future[Response[MultiSearchResponse]] = {
    val searchRequest: Seq[SearchRequest] = qrys.map(qry => ElasticDsl.search(qry.indices).source(Json.stringify(qry.source)))
    val future = client.execute {
      multi(searchRequest)
    }

    future onComplete {
      case Success(_) =>
      case Failure(err) => if (isVerbose) qrys.foreach(qry => logger.error(s"Unable to execute the query on index [${qry.indices.mkString(",")}]--\n${Json.stringify(qry.source)}\n--", err))
    }
    future
  }

  @throws(classOf[IndexingException])
  override def count(indices: Seq[String])(implicit ec: ExecutionContext): Future[Response[CountResponse]] = {
    val future = client.execute {
      ElasticDsl.count(indices)
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to execute the query on indices [${indices.mkString(",")}]", err)
        throw IndexCountException(s"Unable to execute the query on indices [${indices.mkString(",")}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def matchQuery(indices: Seq[String], field: String, value: Any)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val future = client.execute {
      ElasticDsl.search(indices).matchQuery(field, value)
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to execute the query on index [${indices.mkString(",")}]", err)
        throw IndexSearchException(s"Unable to execute the query on index [${indices.mkString(",")}]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def scrollQuery(indices: Seq[String], keepAlive: Option[String], limit: Int, fieldSortVal: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val future = client.execute {
      ElasticDsl.search(indices)
        .scroll(keepAlive.getOrElse("1m"))
        .limit(limit)
        .sortBy(fieldSort(fieldSortVal))
        .matchAllQuery()
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to execute the scroll query on index [${indices.mkString(",")}] with sort on '$fieldSortVal''", err)
        throw IndexSearchException(s"Unable to execute the scroll query on index [${indices.mkString(",")}] with sort on '$fieldSortVal''", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def searchScroll(scrollID: String, keepAlive: Option[String])(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val future = client.execute {
      ElasticDsl.searchScroll(scrollID).keepAlive(keepAlive.getOrElse("1m"))
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to scroll with id [$scrollID]", err)
        throw IndexScrollException(s"Unable to scroll with id [$scrollID]", Some(err))
      }
    }
    future
  }

  @throws(classOf[IndexingException])
  override def clearScroll(scrollID: Seq[String])(implicit ec: ExecutionContext):Future[Response[ClearScrollResponse]] = {
    val future = client.execute {
      ElasticDsl.clearScroll(scrollID)
    }
    future onComplete {
      case Success(_) =>
      case Failure(err) => {
        if (isVerbose) logger.error(s"Unable to clear the scroll with id [$scrollID]", err)
        throw IndexScrollException(s"Unable to clear the scroll with id [$scrollID]", Some(err))
      }
    }
    future
  }

  def pruneObj(percolator: Percolator, query: RegisterQuery): Option[JsResult[JsObject]] = {
    val p1 = JsPath \ "query" \ "_type"
    val p2 = JsPath \ "query" \  "match" \ "_type"

    query match {
      case _: RegisterQueryString => Some(p1.prune(Json.toJson(percolator).as[JsObject]))
      case _: RegisterQueryMatch => {
        val json = p2.prune(Json.toJson(percolator))
        Some(p1.prune(json.get))
      }
      case _ => None
    }
  }

  @throws(classOf[IndexingException])
  private def register(indexName: String, id: String, query: RegisterQuery)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = {
    val queryString = rawQuery(RawQuery(Seq(indexName), Json.parse(
      s"""{
         |  "query": {
         |    "ids" : {
         |      "values" : ["${id}"]
         |    }
         |  }
         |}
         |""".stripMargin
    ).as[JsObject]))


    val registered = queryString.map { response =>
      if (response.isSuccess && response.result.hits.total.value > 0) {
        val percolator = Percolator(query)
        val jsObj: Option[JsResult[JsObject]] = pruneObj(percolator, query)
        if (jsObj.isDefined && jsObj.get.isSuccess) {
          IndexClient.updateById(indexName, ESIndexable(
            id, Some(jsObj.get.get)
          ))
        }
        else {
          logger.error(s"We had problem with the query ${jsObj}")
          throw PercoRegisterException(s"We had problem with the query ${jsObj}")
        }
      }
      else {
        logger.error(s"The concept is not in the index ${indexName}")
        throw PercoRegisterException(s"The concept is not in the index ${indexName}")
      }
    }
    registered.flatten
  }

  @throws(classOf[IndexingException])
  def registerRawQuery(id: String, qry: RawQuery)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = {
    val queryString = rawQuery(RawQuery(qry.indices, Json.parse(
      s"""{
         |  "query": {
         |    "ids" : {
         |      "values" : ["${id}"]
         |    }
         |  }
         |}
         |""".stripMargin
    ).as[JsObject]))

    val registered: Future[Future[Response[UpdateResponse]]] = queryString.map { response =>
      if (response.isSuccess && response.result.hits.total.value > 0) {
        IndexClient.updateById(qry.indices.last, ESIndexable(
          id, Some(qry.source)
        ))
      }
      else {
        logger.error(s"The concept is not in the index ${qry.indices.mkString(", ")}")
        throw new Exception(s"The concept is not in the index ${qry.indices.mkString(", ")}")
      }
    }
    registered.flatten
  }

  @throws(classOf[IndexingException])
  def registerQueryString(indexName: String, id: String, queryString: String, default: Option[String])(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = {
    register(indexName, id, RegisterQueryString(QueryString(queryString, default)))
  }

  @throws(classOf[IndexingException])
  def registerQueryMatch(indexName: String, id: String, query: String, field: String)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = {
    def queryHelper(query: String, field: String): RegisterQueryMatch = {
      field match {
        case "hiddenLabel" => new RegisterQueryMatch(HiddenLabel(query))
        case "prefLabel" => new RegisterQueryMatch(PrefLabel(query))
        case "altLabel" => new RegisterQueryMatch(AltLabel(query))
        case "percoLabel" => new RegisterQueryMatch(PercoLabel(query))
        case _ => throw PercoRegisterException(""""field" should be "hiddenLabel", "prefLabel" or "altLabel" or "percoLabel". """)
      }
    }
    register(indexName, id, queryHelper(query, field))
  }

  def percolate(indexName: Seq[String], document: Map[String, String], from: Option[Int], size: Option[Int])(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val percolatorQuery = PercolatorQuery(from, size, QueryP(Percolate("query", document)))
    rawQuery(RawQuery(indexName, Json.toJson(percolatorQuery).as[JsObject]))
  }

  def multiPercolate(indexName: Seq[String], documents: Seq[Map[String, String]], from: Option[Int], size: Option[Int])(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = {
    val percolatorQry = MultiPercolatorQuery(from, size, MultiQueryP(MultiPercolate("query", documents)))
    rawQuery(RawQuery(indexName, Json.toJson(percolatorQry).as[JsObject]))
  }
}