/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.index

import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, RawQuery}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.cat.CatIndicesResponse
import com.sksamuel.elastic4s.requests.cluster.{ClusterHealthResponse, ClusterStateResponse}
import com.sksamuel.elastic4s.requests.count.CountResponse
import com.sksamuel.elastic4s.requests.delete.DeleteResponse
import com.sksamuel.elastic4s.requests.indexes.admin.{AliasActionResponse, DeleteIndexResponse, IndexExistsResponse}
import com.sksamuel.elastic4s.requests.indexes.{CreateIndexResponse, IndexResponse}
import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import com.sksamuel.elastic4s.requests.searches.{ClearScrollResponse, MultiSearchResponse, SearchResponse}
import com.sksamuel.elastic4s.requests.update.UpdateResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-13
  */

trait AbstractIndexClient extends LazyLogging {

  var isVerbose: Boolean = true

  def config():JsObject

  def verbose() = isVerbose = true

  def mute() = isVerbose = false

  def init()(implicit ec: ExecutionContext)

  def connect()(implicit ec: ExecutionContext): Boolean

  def shutdown()(implicit ec: ExecutionContext)

  def clusterState()(implicit ec: ExecutionContext): Response[ClusterStateResponse]

  def clusterHealth()(implicit ec: ExecutionContext): Response[ClusterHealthResponse]

  def createIndex(indexName: String, settings: Map[String, Any], mappingDef: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]]

  def createIndex(indexName: String, mappingDef: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]]

  def listIndices()(implicit ec: ExecutionContext): Future[Response[Seq[CatIndicesResponse]]]

  def deleteIndices(indices: Seq[String])(implicit ec: ExecutionContext): Future[Response[DeleteIndexResponse]]

  def deleteIndex(indexName: String)(implicit ec: ExecutionContext): Future[Response[DeleteIndexResponse]] = deleteIndices(Seq(indexName))

  def indexExists(indexName: String)(implicit ec: ExecutionContext): Future[Response[IndexExistsResponse]]

  def addAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]]

  def removeAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]]

  def moveAlias(fromIndexName: String, toIndexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]]

  def bulkInsert(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext): Future[Response[BulkResponse]]

  def bulkUpdate(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext): Future[Response[BulkResponse]]

  def insert(indexName: String, docId: String, doc: JsObject)(implicit ec: ExecutionContext): Future[Response[IndexResponse]]

  def insert(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext): Future[Response[IndexResponse]] = insert(indexName, indexable.id, indexable.source.getOrElse(Json.obj()))(ec)

  def updateById(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]]

  def deleteById(indexName: String, docId: String)(implicit ec: ExecutionContext): Future[Response[DeleteResponse]]

  def deleteDoc(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext): Future[Response[DeleteResponse]] = deleteById(indexName, indexable.id)(ec)

  def queryString(indices: Seq[String], qryStr: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]]

  def queryString(indexName: String, qryStr: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = queryString(Seq(indexName), qryStr)

  def rawQuery(qry: RawQuery)(implicit ec: ExecutionContext): Future[Response[SearchResponse]]

  def multiRawQuery(qry: Seq[RawQuery])(implicit ec: ExecutionContext): Future[Response[MultiSearchResponse]]

  def count(indices: Seq[String])(implicit ec: ExecutionContext): Future[Response[CountResponse]]

  def count(indexName: String)(implicit ec: ExecutionContext): Future[Response[CountResponse]] = count(Seq(indexName))

  def matchQuery(indices: Seq[String], field: String, value: Any)(implicit ec: ExecutionContext): Future[Response[SearchResponse]]

  def matchQuery(indexName: String, field: String, value: Any)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = matchQuery(Seq(indexName), field, value)

  def scrollQuery(indices: Seq[String], timeValue: Option[String], limit: Int, fieldSortVal: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]]

  def searchScroll(scrollID: String, timeValue: Option[String])(implicit ec: ExecutionContext): Future[Response[SearchResponse]]

  def clearScroll(scrollID: Seq[String])(implicit ec: ExecutionContext):Future[Response[ClearScrollResponse]]

  def registerRawQuery(id: String, qry: RawQuery)(implicit ec: ExecutionContext):Future[Response[UpdateResponse]]

  def registerQueryString(indexName: String, id: String, queryString: String, default: Option[String])(implicit ec: ExecutionContext): Future[Response[UpdateResponse]]

  def registerQueryMatch(indexName: String, id: String, query: String, field: String)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]]

  def percolate(indexName: Seq[String], document: Map[String, String], from: Option[Int], size: Option[Int])(implicit ec: ExecutionContext): Future[Response[SearchResponse]]

  def multiPercolate(indexName: Seq[String], documents: Seq[Map[String, String]], from: Option[Int], size: Option[Int])(implicit ec: ExecutionContext): Future[Response[SearchResponse]]
}
