/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.index

import com.mnemotix.synaptix.index.elasticsearch.{ESClient, ESConfiguration}
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, RawQuery}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.cat.CatIndicesResponse
import com.sksamuel.elastic4s.requests.cluster.{ClusterHealthResponse, ClusterStateResponse}
import com.sksamuel.elastic4s.requests.count.CountResponse
import com.sksamuel.elastic4s.requests.delete.DeleteResponse
import com.sksamuel.elastic4s.requests.indexes.admin.{AliasActionResponse, DeleteIndexResponse, IndexExistsResponse}
import com.sksamuel.elastic4s.requests.indexes.{CreateIndexResponse, IndexResponse}
import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import com.sksamuel.elastic4s.requests.searches.{ClearScrollResponse, MultiSearchResponse, SearchResponse}
import com.sksamuel.elastic4s.requests.update.UpdateResponse
import play.api.libs.json.JsObject

import scala.concurrent.{ExecutionContext, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-05-13
 */

object IndexClient extends AbstractIndexClient {

  private lazy val client: ESClient = {
    IndexConfiguration.clientDriver match {
      case "es" => new ESClient
      case _ => new ESClient
    }
  }

  override def config(): JsObject = client.config()

  override def init()(implicit ec: ExecutionContext): Unit = client.init()

  override def connect()(implicit ec: ExecutionContext): Boolean = client.connect()

  override def shutdown()(implicit ec: ExecutionContext): Unit = client.shutdown()

  override def clusterState()(implicit ec: ExecutionContext): Response[ClusterStateResponse] = client.clusterState()

  override def clusterHealth()(implicit ec: ExecutionContext): Response[ClusterHealthResponse] = client.clusterHealth()

  override def createIndex(indexName: String, settings: Map[String, Any], mappingDef: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]] = client.createIndex(indexName, settings, mappingDef)

  override def createIndex(indexName: String, mappingDef: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]] = client.createIndex(indexName, mappingDef)

  override def listIndices()(implicit ec: ExecutionContext): Future[Response[Seq[CatIndicesResponse]]] = client.listIndices()

  override def deleteIndices(indices: Seq[String])(implicit ec: ExecutionContext): Future[Response[DeleteIndexResponse]] = client.deleteIndices(indices)

  override def indexExists(indexName: String)(implicit ec: ExecutionContext): Future[Response[IndexExistsResponse]] = client.indexExists(indexName)

  override def addAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]] = client.addAlias(indexName, aliasName)

  override def removeAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]] = client.removeAlias(indexName, aliasName)

  override def moveAlias(fromIndexName: String, toIndexName: String, aliasName: String)(implicit ec: ExecutionContext): Future[Response[AliasActionResponse]] = client.moveAlias(fromIndexName, toIndexName, aliasName)

  override def bulkInsert(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext): Future[Response[BulkResponse]] = client.bulkInsert(indexName, indexables: _*)

  override def insert(indexName: String, docId: String, doc: JsObject)(implicit ec: ExecutionContext): Future[Response[IndexResponse]] = client.insert(indexName, docId, doc)

  override def updateById(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = client.updateById(indexName, indexable)

  override def deleteById(indexName: String, docId: String)(implicit ec: ExecutionContext): Future[Response[DeleteResponse]] = client.deleteById(indexName, docId)

  override def queryString(indices: Seq[String], qryStr: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.queryString(indices, qryStr)

  override def rawQuery(qry: RawQuery)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.rawQuery(qry)

  override def multiRawQuery(qry: Seq[RawQuery])(implicit ec: ExecutionContext): Future[Response[MultiSearchResponse]] = client.multiRawQuery(qry)

  override def count(indices: Seq[String])(implicit ec: ExecutionContext): Future[Response[CountResponse]] = client.count(indices)

  override def matchQuery(indices: Seq[String], field: String, value: Any)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.matchQuery(indices, field, value)

  override def scrollQuery(indices: Seq[String], timeValue: Option[String], limit: Int, fieldSortVal: String)(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.scrollQuery(indices, timeValue, limit, fieldSortVal)

  override def searchScroll(scrollID: String, timeValue: Option[String])(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.searchScroll(scrollID, timeValue)

  override def clearScroll(scrollID: Seq[String])(implicit ec: ExecutionContext): Future[Response[ClearScrollResponse]] = client.clearScroll(scrollID)

  override def bulkUpdate(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext): Future[Response[BulkResponse]] = client.bulkUpdate(indexName, indexables: _*)

  override def registerRawQuery(id: String, qry: RawQuery)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = client.registerRawQuery(id, qry)

  override def registerQueryString(indexName: String, id: String, queryString: String, default: Option[String])(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = client.registerQueryString(indexName, id, queryString, default)

  override def registerQueryMatch(indexName: String, id: String, query: String, field: String)(implicit ec: ExecutionContext): Future[Response[UpdateResponse]] = client.registerQueryMatch(indexName, id, query, field)

  override def percolate(indexName: Seq[String], document: Map[String, String], from: Option[Int], size: Option[Int])(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.percolate(indexName, document, from, size)

  override def multiPercolate(indexName: Seq[String], documents: Seq[Map[String, String]], from: Option[Int], size: Option[Int])(implicit ec: ExecutionContext): Future[Response[SearchResponse]] = client.multiPercolate(indexName, documents, from, size)
}
