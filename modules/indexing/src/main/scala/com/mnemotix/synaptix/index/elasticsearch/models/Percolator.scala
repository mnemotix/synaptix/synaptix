/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.index.elasticsearch.models

import play.api.libs.json.{Json, OFormat}

sealed trait Term

case class HiddenLabel(hiddenLabel: String) extends Term

object HiddenLabel {
  implicit lazy val format: OFormat[HiddenLabel] = Json.format[HiddenLabel]
}

case class PrefLabel(prefLabel: String) extends Term

object PrefLabel {
  implicit lazy val format: OFormat[PrefLabel] = Json.format[PrefLabel]
}

case class AltLabel(altLabel: String) extends Term

object AltLabel {
  implicit lazy val format: OFormat[AltLabel] = Json.format[AltLabel]
}

case class PercoLabel(percoLabel: String) extends Term

object PercoLabel {
  implicit lazy val format = Json.format[PercoLabel]
}

object Term {
  implicit val format: OFormat[Term] = Json.format[Term]
}

sealed trait RegisterQuery

case class QueryString(query: String, default_field: Option[String])

object QueryString {
  implicit  val format: OFormat[QueryString] = Json.format[QueryString]
}

case class RegisterQueryString(query_string: QueryString) extends RegisterQuery

object RegisterQueryString {
  implicit lazy val format: OFormat[RegisterQueryString] = Json.format[RegisterQueryString]
}

case class RegisterQueryMatch(`match`: Term) extends RegisterQuery

object RegisterQueryMatch {
  implicit lazy val format: OFormat[RegisterQueryMatch] = Json.format[RegisterQueryMatch]
}

object RegisterQuery {
  implicit lazy val format: OFormat[RegisterQuery] = Json.format[RegisterQuery]
}

case class Percolator(query: RegisterQuery)

object Percolator {
  implicit lazy val format: OFormat[Percolator] = Json.format[Percolator]
}

case class Percolate(
                      field: String,
                      document: Map[String, String]
                    )

object Percolate {
  implicit lazy val format: OFormat[Percolate] = Json.format[Percolate]
}

case class QueryP(
                   percolate: Percolate
                 )

object QueryP {
  implicit lazy val format: OFormat[QueryP] = Json.format[QueryP]
}


case class PercolatorQuery(from: Option[Int], size: Option[Int], query: QueryP)

object PercolatorQuery {
  implicit lazy val format: OFormat[PercolatorQuery] = Json.format[PercolatorQuery]
}

case class PercolateQuery(indexName: Seq[String], field: String, document: String, from: Option[Int], size: Option[Int])

object PercolateQuery {
  implicit lazy val format: OFormat[PercolateQuery] = Json.format[PercolateQuery]
}

/**
  *
  * Mutli Percolate Query
  *
  */

case class MultiPercolate(field:String, documents: Seq[Map[String, String]])

object MultiPercolate {
  implicit lazy val format: OFormat[MultiPercolate] = Json.format[MultiPercolate]
}

case class MultiQueryP(percolate: MultiPercolate)

object MultiQueryP {
  implicit lazy val format: OFormat[MultiQueryP] = Json.format[MultiQueryP]
}

case class MultiPercolatorQuery(from: Option[Int], size: Option[Int], query: MultiQueryP)

object MultiPercolatorQuery {
  implicit lazy val format: OFormat[MultiPercolatorQuery] = Json.format[MultiPercolatorQuery]
}