/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.graphdb

import java.io.{BufferedWriter, File, FileWriter}

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.graphdb.models._

import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-29
  */

class GraphDBUtilsSpec extends SynaptixTestSpec {

  val parserSettings = new ParserSettings

  val importURL = new GraphDBDataImportUrl(None, None, Some("https://gitlab.com/mnemotix/ddf/ddf-data/-/raw/master/inventaire/inventaire.trig"), None, None, None, Some("inventaire"), parserSettings, None, "PENDING", System.currentTimeMillis(), None, None)

  val repositoryName = "test-"+System.currentTimeMillis()

  "GraphDBUtils" should {

    "create a repository" ignore {
      val fileContent =
        s"""
           |@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
           |@prefix rep:  <http://www.openrdf.org/config/repository#> .
           |@prefix sr:   <http://www.openrdf.org/config/repository/sail#> .
           |@prefix sail: <http://www.openrdf.org/config/sail#> .
           |@prefix ms:   <http://www.openrdf.org/config/sail/memory#> .
           |
           |[] a rep:Repository ;
           |   rep:repositoryID   "$repositoryName" ;
           |   rdfs:label "test memory store" ;
           |   rep:repositoryImpl [
           |      rep:repositoryType "openrdf:SailRepository" ;
           |      sr:sailImpl [
           |        sail:sailType "openrdf:MemoryStore" ;
           |        ms:persist    true ;
           |        ms:syncDelay  120
           |      ]
           |   ] .
           |
           |""".stripMargin

      val tempFile = File.createTempFile("repo-config", ".ttl")
      val bw = new BufferedWriter(new FileWriter(tempFile))
      bw.write(fileContent)
      bw.close()
      tempFile.deleteOnExit()
      val (success:Boolean, _) = GraphDbRestApiClient.createRepository(tempFile)
      success shouldBe true
      GraphDbRestApiClient.listRepositories().map(_.id).contains(repositoryName) shouldBe true
    }

    "edit a repository" ignore {
      val enableLiteralIndex = new EnableLiteralIndex(value = false)
      val inMemoryLiteralProperties = new InMemoryLiteralProperties(value = false)
      val enablePredicateList = new EnablePredicateList(value = false)
      val params = new Params(enablePredicateList,inMemoryLiteralProperties, enableLiteralIndex)
      val editRepository = new GraphDBEditRepository(repositoryName, "", params, "Test repository", "free")
      val result = GraphDbRestApiClient.editRepositoryConfig(
        repositoryName,
        editRepository
      )
      result._1 shouldBe true
    }

    "insert a file" ignore {
      val resultat = GraphDbRestApiClient.importDataByUrl(repositoryName, importURL)
      resultat._1 shouldBe true
    }

    "drop a repository" ignore {
      GraphDbRestApiClient.deleteRepository(repositoryName)
      GraphDbRestApiClient.listRepositories().map(_.id).contains(repositoryName) shouldBe false
    }
  }
}
