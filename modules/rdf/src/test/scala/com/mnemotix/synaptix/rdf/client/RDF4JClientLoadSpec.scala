/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.rdf.client

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.rdf.client.models.Done
import com.mnemotix.synaptix.rdf.client.rdf4j.Rdf4jRDFClient

import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 25/09/2020
  */

class RDF4JClientLoadSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val client = new Rdf4jRDFClient
  client.init()

  "RDF4JClient" should {

    "load data to a remote repository" ignore {
      val filepath = this.getClass.getResource("/data.trig").toURI.getPath
      println(filepath)
      val wconn = client.getWriteConnection("dev-crash-mnb")
      val res = client.load(filepath, None, RDFFormats.TRIG)(ec, wconn).futureValue
      res.isInstanceOf[Done] shouldBe true
    }

  }
}
