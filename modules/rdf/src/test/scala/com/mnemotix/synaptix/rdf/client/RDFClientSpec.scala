/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.rdf.client

import java.io.{BufferedWriter, File, FileWriter}
import java.net.URL

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.graphdb.GraphDbRestApiClient
import com.mnemotix.synaptix.rdf.client.models.{Done, RDFClientReadConnection, RDFClientWriteConnection, RDFResultSet}
import play.api.libs.json.Json

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Random, Try}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-18
  */

class RDFClientSpec extends SynaptixTestSpec {


  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val repositoryName = "test-"+System.currentTimeMillis()
  val readingPool = mutable.Buffer[RDFClientReadConnection]()
  val writingPool = mutable.Buffer[RDFClientWriteConnection]()
  val connectionPoolSize = 16
  val writePoolSize = 4
  val readPoolSize = 16
  val r = new Random

  "RDF4JClient" should {

    "create a repository from a config file" in {
      val fileContent =
        s"""
           |@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
           |@prefix rep:  <http://www.openrdf.org/config/repository#> .
           |@prefix sr:   <http://www.openrdf.org/config/repository/sail#> .
           |@prefix sail: <http://www.openrdf.org/config/sail#> .
           |@prefix ms:   <http://www.openrdf.org/config/sail/memory#> .
           |
           |[] a rep:Repository ;
           |   rep:repositoryID   "$repositoryName" ;
           |   rdfs:label "test memory store" ;
           |   rep:repositoryImpl [
           |      rep:repositoryType "openrdf:SailRepository" ;
           |      sr:sailImpl [
           |        sail:sailType "openrdf:MemoryStore" ;
           |        ms:persist    true ;
           |        ms:syncDelay  120
           |      ]
           |   ] .
           |
           |""".stripMargin

      val tempFile = File. createTempFile("repo-config", ".ttl")
      val bw = new BufferedWriter(new FileWriter(tempFile))
      bw.write(fileContent)
      bw.close()
      tempFile.deleteOnExit()
      val (success:Boolean, _) = GraphDbRestApiClient.createRepository(tempFile)
      success shouldBe true
    }

    "list repositories" in {
      val repos = GraphDbRestApiClient.listRepositories()
      println(Json.prettyPrint(Json.toJson(repos)))
    }

    "open connections with the server" in {
      (1 to writePoolSize).map{i =>
        writingPool.append(RDFClient.getWriteConnection(repositoryName))
      }
      (1 to readPoolSize).map{i =>
        readingPool.append(RDFClient.getReadConnection(repositoryName))
      }
    }

    "load data from a local file" in {
      val filepath = this.getClass.getResource("/data.ttl").toURI.getPath
      println(filepath)
      val res = RDFClient.load(filepath, None, RDFFormats.TURTLE)(ec, writingPool(0)).futureValue
      res.isInstanceOf[Done] shouldBe true
    }

    "load data from a remote URL" in {
      val res = RDFClient.load(new URL("http://www.w3.org/TR/skos-reference/skos.rdf"), None, RDFFormats.RDFXML)(ec, writingPool(0)).futureValue
      res.isInstanceOf[Done] shouldBe true
    }

    "query data with SPARQL select queries" in {

      val results: mutable.Seq[RDFResultSet] = Future.sequence(readingPool.map{ conn =>
        RDFClient.select("SELECT * WHERE {?p ?r ?q} LIMIT 10")(ec, conn)
      }).futureValue
      results(0).resultSet.hasNext shouldBe true
      val qr = results(0).get
      while(qr.hasNext){
        val b = qr.next()
        println(b.getBinding("p").getValue)
      }
      println(results(0).toXml())
      results(1).resultSet.hasNext shouldBe true
      println(results(1).toCsv())
      results(2).resultSet.hasNext shouldBe true
      println(Json.prettyPrint(results(2).toJson()))
    }

    "query data with SPARQL construct queries" in {

      val m = RDFClient.construct("CONSTRUCT { ?p ?r ?q } WHERE {?p ?r ?q} LIMIT 100")(ec, readingPool(r.nextInt(readPoolSize))).futureValue
      m.model.size shouldBe > (0)
      println(m.toRdfXml())
      println(Json.prettyPrint(m.toJsonLd()))
    }

    "insert data with SPARQL Update query" in {
      val qry = """PREFIX dc: <http://purl.org/dc/elements/1.1/>
                  |INSERT DATA
                  |{ <http://example/book1> dc:title "A new book" ; dc:creator "A.N.Other" .}
                  |""".stripMargin

      val res = RDFClient.update(qry)(ec, writingPool(r.nextInt(writePoolSize))).futureValue
      res.isInstanceOf[Done] shouldBe true
    }

    "delete data with SPARQL Update query" in {
      val qry = """PREFIX dc: <http://purl.org/dc/elements/1.1/>
                  |
                  |DELETE DATA
                  |{
                  |  <http://example/book2> dc:title "David Copperfield" ;
                  |                         dc:creator "Edmund Wells" .
                  |}
                  |""".stripMargin

      val res = RDFClient.update(qry)(ec, writingPool(r.nextInt(writePoolSize))).futureValue
      res.isInstanceOf[Done] shouldBe true
    }

    "drop a repository" in {
      val result: Option[(Boolean, String)] = Try {
        GraphDbRestApiClient.deleteRepository(repositoryName)
      }.toOption
      result.isDefined should be(true)
    }
  }
}
