/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.graphdb.models

import play.api.libs.json.Json

/**
 * Created by Pierre-René LHERISSON (pr.lherisson@mnemotix.com).
 * Date: 2020-03-03
 */

case class ParserSettings(
                           failOnUnknownDataTypes: Boolean = true,
                           failOnUnknownLanguageTags: Boolean = true,
                           normalizeDataTypeValues: Boolean = true,
                           normalizeLanguageTags: Boolean = true,
                           preserveBNodeIds: Boolean = true,
                           stopOnError: Boolean = true,
                           verifyDataTypeValues: Boolean = true,
                           verifyLanguageTags: Boolean = true,
                           verifyRelativeURIs: Boolean = true,
                           verifyURISyntax: Boolean = true
                         ){
  def toJson = Json.toJson(this)(ParserSettings.parserSettingsformat)
}

case class GraphDBDataImportUrl(
                                 baseURI: Option[String],
                                 context: Option[String],
                                 data: Option[String],
                                 forceSerial: Option[String],
                                 format: Option[String],
                                 message: Option[String],
                                 name: Option[String],
                                 parserSettings: ParserSettings,
                                 replaceGraphs: Option[Seq[String]],
                                 status: String = "PENDING",
                                 timestamp: Long = 0L,
                                 `type`: Option[String],
                                 xRequestIdHeaders: Option[String]
                               ){
  def toJson = Json.toJson(this)(GraphDBDataImportUrl.graphDBDataImportUrlformat)
}

case class GraphDBServerFileImport(
                                  fileNames: Seq[String],
                                  graphDBDataImportUrl: GraphDBDataImportUrl
                                  )

object ParserSettings {
  implicit lazy val parserSettingsformat = Json.format[ParserSettings]
}

object GraphDBDataImportUrl {
  implicit lazy val graphDBDataImportUrlformat = Json.format[GraphDBDataImportUrl]
}