/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.graphdb

import com.mnemotix.synaptix.core.utils.StringUtils
import com.mnemotix.synaptix.rdf.client.RDFClientConfiguration

import java.io.File

object LoadRDF {
  lazy val accessPointUri: String = s"${StringUtils.removeTrailingSlashes(RDFClientConfiguration.rootUri)}/bin/loadrdf"

  def loadingData(repoName: String, parallel: Option[Boolean], serial: Option[Boolean], c: Option[String], f: Option[Boolean], i: Option[String], p: Option[Boolean], s: Option[String],
                  v: Option[String], files: Seq[File]): Int = {
    val m = fm(parallel, serial)
    val fOpt = if (f.isDefined) "-f" else ""
    val pOpt = if (p.isDefined) "-p" else ""
    val sOpt = if (s.isDefined) "-s" else ""
    val vOpt = if (v.isDefined) "-v" else ""
    val filesLoc = files.map(_.getPath).mkString(" ")
    val cmd = s"$accessPointUri $fOpt ${i.getOrElse("")} $pOpt $sOpt $vOpt  $repoName $m $filesLoc".trim.replaceAll(" +", " ")

    import scala.sys.process._
    val result = Process(s"$cmd")
    result.run().exitValue()
  }

  def fm(parallel: Option[Boolean], serial: Option[Boolean]) = {
    if (parallel.isDefined && serial.isDefined) {
      "-m parallel"
    }
    else if (parallel.isDefined && !serial.isDefined) {
      "-m parallel"
    }
    else if (!parallel.isDefined && serial.isDefined) {
      "-m serial"
    }
    else "-m parallel"
  }
}
