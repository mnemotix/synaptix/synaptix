/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.rdf.client

import java.io.File
import java.net.URL

import com.mnemotix.synaptix.rdf.client.models._
import com.mnemotix.synaptix.rdf.client.rdf4j.Rdf4jRDFClient
import play.api.libs.json.JsValue

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-19
  */

object RDFClient {

  private lazy val client: Rdf4jRDFClient = new Rdf4jRDFClient

  def verbose() = client.verbose()

  def mute() = client.mute()

  def init()(implicit ec: ExecutionContext): Boolean = client.init()

  def listRepositories():Set[String] = client.listRepositories()

  def shutdown(): Unit = client.shutdown()

  def load(filepath: String, baseURI: Option[String], format: RDFFormats.RDFFormat, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = client.load(filepath, baseURI, format, contexts: _*)

  def load(file: File, baseURI: Option[String], format: RDFFormats.RDFFormat, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = client.load(file, baseURI, format, contexts: _*)

  def load(url: URL, baseURI: Option[String], format: RDFFormats.RDFFormat, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = client.load(url, baseURI, format, contexts: _*)

  def load(model: RDFModel, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = client.load(model, contexts: _*)

  def loadJsonLd(jsonld: JsValue, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = client.loadJsonLd(jsonld, contexts: _*)

  def select(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[RDFResultSet] = client.select(qryStr)

  def describe(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[RDFModel] = client.describe(qryStr)

  def construct(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[RDFModel] = client.construct(qryStr)

  def ask(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[Boolean] = client.ask(qryStr)

  def update(updateStr: String)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = client.update(updateStr)

  def getReadConnection(repositoryName: String): RDFClientReadConnection = client.getReadConnection(repositoryName)

  def getWriteConnection(repositoryName: String): RDFClientWriteConnection = client.getWriteConnection(repositoryName)
}