/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.graphdb

import com.mnemotix.synaptix.core.utils.StringUtils
import com.mnemotix.synaptix.rdf.client.RDFClientConfiguration

import java.io.File

object Preload {

  lazy val accessPointUri: String = s"${StringUtils.removeTrailingSlashes(RDFClientConfiguration.rootUri)}/bin/preload"

  def loadingData(repoName: String, a: Option[String], b: Option[String], c: Option[String], f: Option[String], i: Option[String], p: Option[String], q: Option[String], r: Option[String],
                  s: Option[String], t: Option[String], x: Option[String], y: Option[String], files: Seq[File]): Int = {

    val filesLoc = files.map(_.getPath).mkString(" ")
    val cmd = s"$accessPointUri ${a.getOrElse("")} ${a.getOrElse("")} ${b.getOrElse("")} ${c.getOrElse("")} ${f.getOrElse("")} ${i.getOrElse("")} ${p.getOrElse("")}" +
      s" ${q.getOrElse("")} ${r.getOrElse("")} ${s.getOrElse("")} ${t.getOrElse("")} ${x.getOrElse("")} ${y.getOrElse("")} $repoName $filesLoc".trim.replaceAll(" +", " ")


    import scala.sys.process._
    val result = Process(s"$cmd")
    result.run().exitValue()
  }
}
