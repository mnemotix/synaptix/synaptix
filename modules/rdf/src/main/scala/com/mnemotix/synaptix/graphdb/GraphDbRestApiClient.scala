/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.graphdb

import akka.stream.scaladsl.FileIO

import java.io.File
import com.mnemotix.synaptix.core.utils.StringUtils
import com.mnemotix.synaptix.graphdb.models.{GraphDBDataImportUrl, GraphDBEditRepository, GraphDBRepository}
import com.mnemotix.synaptix.rdf.client.{RDFClientConfiguration, RDFCreateRepoException, RDFListRepositoriesException, RDFLoadException, RDFRepositoryAlreadyExistsException}
import com.softwaremill.sttp._

import java.nio.file.Files
//import sttp.client3._
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-29
  */

object GraphDbRestApiClient extends LazyLogging {

  implicit val backend = HttpURLConnectionBackend()

  lazy val accessPointUri: String = s"${StringUtils.removeTrailingSlashes(RDFClientConfiguration.rootUri)}/rest"

  def listRepositories() = {
    try{
      val request = sttp.auth.basic(RDFClientConfiguration.user.get, RDFClientConfiguration.pwd.get).headers(Map("Accept" -> "application/json")).get(uri"$accessPointUri/repositories")
      val response = request.send()
      if (response.body.isLeft) {
        logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
        throw RDFListRepositoriesException(response.body.left.get, null)
      }
      else {
        Json.parse(response.body.right.get).as[Seq[GraphDBRepository]]
      }
    }
    catch {
      case t:Throwable => throw RDFListRepositoriesException(t.getMessage, t.getCause)
    }
  }

  def deleteRepository(repositoryId: String): (Boolean, String) = {
    val request = sttp.auth.basic(RDFClientConfiguration.user.get, RDFClientConfiguration.pwd.get)
      .delete(uri"$accessPointUri/repositories/$repositoryId")
    val response = request.send()
    if (response.body.isLeft) {
      logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
      (false, response.body.left.get)
    }
    else (true, "Done")
  }

  def createRepository(configTtlFile: File): (Boolean, String) = {
    val request = sttp.auth.basic(RDFClientConfiguration.user.get, RDFClientConfiguration.pwd.get)
      .headers(Map("Content-Type" -> "multipart/form-data"))
      .multipartBody(
        multipartFile("config", configTtlFile)
      )
      .post(uri"$accessPointUri/repositories")
    val response = request.send()
    if (response.body.isLeft) {
      logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
      if(response.body.left.get.contains("already exists")) throw RDFRepositoryAlreadyExistsException(response.body.left.get, null)
      else throw RDFCreateRepoException(response.body.left.get, null)
    }
    else (true, "Done")
  }

  def editRepositoryConfig(repositoryId: String, graphDBEditRepository: GraphDBEditRepository): (Boolean, String) = {
    val request = sttp.auth.basic(RDFClientConfiguration.user.get, RDFClientConfiguration.pwd.get)
      .headers(Map("Content-Type" -> "application/json", "Accept" -> "application/json"))
      .body(graphDBEditRepository.toJson.toString())
      .put(uri"$accessPointUri/repositories/$repositoryId")
    val response = request.send()
    if (response.body.isLeft) {
      logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
      (false, response.body.left.get)
    }
    else (true, "Done")
  }

  def importDataByUrl(repositoryId: String, graphDBImportUrl: GraphDBDataImportUrl): (Boolean, String) = {
    val request = sttp.auth.basic(RDFClientConfiguration.user.get, RDFClientConfiguration.pwd.get)
      .headers(Map("Content-Type" -> "application/json", "Accept" -> "application/json"))
      .body(graphDBImportUrl.toJson.toString())
      .post(uri"$accessPointUri/data/import/upload/$repositoryId/url")
    val response = request.send()
    if (response.body.isLeft) {
      logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
      (false, response.body.left.get)
    }
    else (true, "Done")
  }

}