/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.rdf.client.models

import java.io.ByteArrayOutputStream

import com.mnemotix.synaptix.rdf.client._
import org.eclipse.rdf4j.query.resultio.sparqljson.SPARQLResultsJSONWriter
import org.eclipse.rdf4j.query.resultio.sparqlxml.SPARQLResultsXMLWriter
import org.eclipse.rdf4j.query.resultio.text.csv.SPARQLResultsCSVWriter
import org.eclipse.rdf4j.repository.RepositoryConnection
import org.eclipse.rdf4j.rio.Rio
import play.api.libs.json.Json

import scala.xml.XML

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-07-11
  */


case class Done(message: String, isSuccess: Boolean = true)

case class RDFStatement(stmt: org.eclipse.rdf4j.model.Statement) {
  def get = stmt
}

case class RDFModel(model: org.eclipse.rdf4j.model.Model) {
  def get = model

  def format(format: RDFFormats.RDFFormat) = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, format.`type`)
    val bytes = os.toByteArray
    os.close()
    bytes
  }

  def toJsonLd() = Json.parse(format(RDFFormats.JSONLD))

  def toRdfXml() = XML.loadString(new String(format(RDFFormats.RDFXML), "UTF-8"))
}

case class RDFResultSet(resultSet: BindingSetIterator) {
  def get = resultSet

  def format(format: SPARQLResultsFormats.SPARQLResultsFormat) = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    val writer = format match {
      case SPARQLResultsFormats.JSON => new SPARQLResultsJSONWriter(os)
      case SPARQLResultsFormats.XML => new SPARQLResultsXMLWriter(os)
      case SPARQLResultsFormats.CSV => new SPARQLResultsCSVWriter(os)
      case _ => new SPARQLResultsJSONWriter(os)
    }
    val bindings = resultSet
    val columnHeaders = bindings.getBindingNames
    writer.startQueryResult(columnHeaders)
    while (bindings.hasNext) {
      writer.handleSolution(bindings.next())
    }
    writer.endQueryResult
    val bytes = os.toByteArray
    os.close()
    bytes
  }

  def toCsv() = new String(format(SPARQLResultsFormats.CSV), "UTF-8")

  def toJson() = Json.parse(format(SPARQLResultsFormats.JSON))

  def toXml() = XML.loadString(new String(format(SPARQLResultsFormats.XML), "UTF-8"))
}

abstract class RDFClientConnection(val repositoryName: String, conn: RepositoryConnection) {
  def get = conn

  def close() = conn.close()
}

case class RDFClientReadConnection(override val repositoryName: String, conn: RepositoryConnection) extends RDFClientConnection(repositoryName, conn)

case class RDFClientWriteConnection(override val repositoryName: String, conn: RepositoryConnection) extends RDFClientConnection(repositoryName, conn)
