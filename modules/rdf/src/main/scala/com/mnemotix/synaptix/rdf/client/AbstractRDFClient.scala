/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.rdf.client

import java.io.File
import java.net.URL

import com.mnemotix.synaptix.rdf.client.models._
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.JsValue

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-19
  */

trait AbstractRDFClient extends LazyLogging {

  var isVerbose:Boolean=true

  /**
    * Enables the logging of events on all methods of the client
    */
  def verbose() = isVerbose=true

  /**
    * Disables the logging of events on all methods of the client
    */
  def mute() = isVerbose=false

  /**
    * Establishes the connection with the remote server
    */
  def init()(implicit ec: ExecutionContext):Boolean

  /**
    * Disconnect with the remote server
    */
  def shutdown()

  /**
    * Lists current repositories available on the server
    */
  def listRepositories():Set[String]

  /**
    * Loads data to the specified repository
    */
  def load(filepath: String, baseURI: Option[String], format:RDFFormats.RDFFormat, contexts:String*)(implicit ec: ExecutionContext, conn:RDFClientWriteConnection):Future[Done]

  /**
    * Loads data to the specified repository
    */
  def load(file: File, baseURI: Option[String], format:RDFFormats.RDFFormat, contexts:String*)(implicit ec: ExecutionContext, conn:RDFClientWriteConnection):Future[Done]

  /**
    * Loads data to the specified repository
    */
  def load(url: URL, baseURI: Option[String], format:RDFFormats.RDFFormat, contexts:String*)(implicit ec: ExecutionContext, conn:RDFClientWriteConnection):Future[Done]

  /**
    * Loads data to the specified repository
    */
  def load(model: RDFModel, contexts:String*)(implicit ec: ExecutionContext, conn:RDFClientWriteConnection):Future[Done]

  /**
    * Loads data to the specified repository
    */
  def loadJsonLd(jsonld: JsValue, contexts: String*)(implicit ec: ExecutionContext, conn:RDFClientWriteConnection):Future[Done]

  def select(qryStr: String)(implicit ec: ExecutionContext, conn:RDFClientReadConnection):Future[RDFResultSet]

  def describe(qryStr: String)(implicit ec: ExecutionContext, conn:RDFClientReadConnection):Future[RDFModel]

  def construct(qryStr: String)(implicit ec: ExecutionContext, conn:RDFClientReadConnection):Future[RDFModel]

  def ask(qryStr: String)(implicit ec: ExecutionContext, conn:RDFClientReadConnection): Future[Boolean]

  def update(updateStr: String)(implicit ec: ExecutionContext, conn:RDFClientWriteConnection): Future[Done]

  def getReadConnection(repositoryName:String):RDFClientReadConnection

  def getWriteConnection(repositoryName:String):RDFClientWriteConnection

}
