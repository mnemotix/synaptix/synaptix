/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.graphdb.models

import play.api.libs.json.{JsValue, Json}

/**
 * Created by Pierre-René LHERISSON (pr.lherisson@mnemotix.com).
 * Date: 2020-03-03
 */

case class EnableLiteralIndex(label: String = "enable-literal-index", name: String = "enable-literal-index", value: Boolean) {
  def toJson: JsValue = Json.toJson(this)(EnableLiteralIndex.format)
}

case class InMemoryLiteralProperties(label: String = "in-memory-literal-properties", name: String = "in-memory-literal-properties", value: Boolean) {
  def toJson: JsValue = Json.toJson(this)(InMemoryLiteralProperties.format)
}

case class EnablePredicateList(label: String = "enablePredicateList", name: String = "enablePredicateList", value: Boolean) {
  def toJson: JsValue = Json.toJson(this)(EnablePredicateList.format)
}

case class Params(enablePredicateList: EnablePredicateList, inMemoryLiteralProperties: InMemoryLiteralProperties, EnableLiteralIndex: EnableLiteralIndex) {
  def toJson: JsValue = Json.toJson(this)(Params.format)
}

case class GraphDBEditRepository(id: String, location: String, params: Params ,title: String, `type`: String) {
  def toJson: JsValue = Json.toJson(this)(GraphDBEditRepository.format)
}

object EnableLiteralIndex {
  implicit lazy val format = Json.format[EnableLiteralIndex]
}

object InMemoryLiteralProperties {
  implicit lazy val format = Json.format[InMemoryLiteralProperties]
}

object EnablePredicateList {
  implicit lazy val format = Json.format[EnablePredicateList]
}

object Params {
  implicit lazy val format = Json.format[Params]
}

object GraphDBEditRepository {
  implicit lazy val format = Json.format[GraphDBEditRepository]
}