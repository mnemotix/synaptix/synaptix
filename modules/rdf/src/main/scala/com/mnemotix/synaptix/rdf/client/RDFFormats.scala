/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.rdf.client

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-11-13
  */

object RDFFormats {

  sealed abstract class RDFFormat(
    val name : String,
    val extension : String,
    val `type` : org.eclipse.rdf4j.rio.RDFFormat
  )

  case object TRIG extends RDFFormat("TriG", ".trig", org.eclipse.rdf4j.rio.RDFFormat.TRIG)
  case object TRIX extends RDFFormat("TriX", ".trix", org.eclipse.rdf4j.rio.RDFFormat.TRIX)
  case object TURTLE extends RDFFormat("Turtle", ".ttl", org.eclipse.rdf4j.rio.RDFFormat.TURTLE)
  case object JSONLD extends RDFFormat("JSON-LD", ".jsonld", org.eclipse.rdf4j.rio.RDFFormat.JSONLD)
  case object RDFJSON extends RDFFormat("RDF/JSON", ".rj", org.eclipse.rdf4j.rio.RDFFormat.RDFJSON)
  case object RDFXML extends RDFFormat("RDF/XML", ".rdf", org.eclipse.rdf4j.rio.RDFFormat.RDFXML)
  case object NTRIPLES extends RDFFormat("N-Triples", ".nt", org.eclipse.rdf4j.rio.RDFFormat.NTRIPLES)
  case object NQUADS extends RDFFormat("N-Quads", ".nq", org.eclipse.rdf4j.rio.RDFFormat.NQUADS)

  val values = Seq(TRIG, TURTLE, JSONLD, RDFJSON, RDFXML, NTRIPLES, NQUADS)

}

object SPARQLResultsFormats {

  sealed abstract class SPARQLResultsFormat(
    val name : String,
    val extension : String
  )

  case object JSON extends SPARQLResultsFormat("JSON", ".json")
  case object XML extends SPARQLResultsFormat("XML", ".xml")
  case object CSV extends SPARQLResultsFormat("CSV", ".csv")

  val values = Seq(JSON, XML, CSV)

}
