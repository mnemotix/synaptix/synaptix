/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.rdf.client.rdf4j

import com.mnemotix.synaptix.rdf.client._
import com.mnemotix.synaptix.rdf.client.models._
import org.eclipse.rdf4j.http.protocol.UnauthorizedException
import org.eclipse.rdf4j.model.impl.{SimpleValueFactory, TreeModel}
import org.eclipse.rdf4j.model.util.Models
import org.eclipse.rdf4j.model.vocabulary.RDF
import org.eclipse.rdf4j.model.{Model, Resource}
import org.eclipse.rdf4j.query.{QueryResults, TupleQueryResult}
import org.eclipse.rdf4j.repository.config.{RepositoryConfig, RepositoryConfigSchema}
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager
import org.eclipse.rdf4j.repository.{Repository, RepositoryConnection, RepositoryException}
import org.eclipse.rdf4j.rio.helpers.StatementCollector
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}
import play.api.libs.json.{JsValue, Json}

import java.io.{File, FileInputStream, StringReader}
import java.net.URL
import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.compat.java8.OptionConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-03-19
 */
class Rdf4jRDFClient extends AbstractRDFClient {

  lazy val repositoryManager: RemoteRepositoryManager = {
    val rm = new RemoteRepositoryManager(RDFClientConfiguration.rootUri)
    rm.setUsernameAndPassword(RDFClientConfiguration.user.get, RDFClientConfiguration.pwd.get)
    rm
  }

  lazy val repositories = mutable.HashMap[String, Repository]()

  override def init()(implicit ec: ExecutionContext): Boolean = {
    try {
      logger.info(s"Trying to connect the repository manager [${RDFClientConfiguration.rootUri}]...")
      repositoryManager.init()
      listRepositories()
      logger.info(s"Connection successfull.")
      true
    }
    catch {
      case _: org.eclipse.rdf4j.repository.RepositoryException => {
        logger.info(s"Unable to connect the repository manager [${RDFClientConfiguration.rootUri}]. Retrying in 5 sec...")
        Thread.sleep(5000)
        init()(ec)
      }
      case wce: RDFClientWrongCredentialsException => {
        logger.error(wce.getMessage, wce)
        logger.error("Aborting.")
        sys exit (-1)
      }
      case t: Throwable => {
        logger.error(s"Unknown error while trying to connect the repository manager. Aborting.", t)
        sys exit (-1)
      }
    }
  }

  override def listRepositories(): Set[String] = {
    try {
      val repositories = repositoryManager.getRepositoryIDs.asScala.toSet
      logger.debug(s"Available repositories: ${repositories.mkString(", ")}")
      repositories
    }
    catch {
      case re: RepositoryException => {
        re.getCause match {
          case ue: UnauthorizedException => {
            throw RDFClientWrongCredentialsException("Wrong credentials.", ue)
          }
          case _ => throw re
        }
      }
      case t: Throwable => throw t
    }
  }

  def createRepository(configTtlFile: File) = {
    // Instantiate a repository graph model
    val graph: Model = new TreeModel()
    // Read repository configuration
    val fis = new FileInputStream(configTtlFile)
    val rdfParser = Rio.createParser(RDFFormat.TURTLE)
    rdfParser.setRDFHandler(new StatementCollector(graph))
    rdfParser.parse(fis, RepositoryConfigSchema.NAMESPACE)
    fis.close
    // Retrieve the repository node as a resource
    val subject: Resource = Models.subject(graph.filter(null, RDF.TYPE, RepositoryConfigSchema.REPOSITORY)).asScala.getOrElse(throw new RuntimeException("Oops, no <http://www.openrdf.org/config/repository#> subject found!"))
    // Create a repository configuration object and add it to the repositoryManager
    val repositoryConfig = RepositoryConfig.create(graph, subject)
    repositoryManager.addRepositoryConfig(repositoryConfig)
  }

  override def shutdown() = {
    logger.debug(s"Client shutdown...")
    repositories.values.foreach(_.shutDown())
    repositoryManager.shutDown()
  }

  override def load(file: String, baseURI: Option[String], format: RDFFormats.RDFFormat, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = {
    val future = Future {
      try {
        val input = new FileInputStream(file)
        conn.get.add(input, baseURI.getOrElse(""), format.`type`, contexts.map(ctx => SimpleValueFactory.getInstance().createIRI(ctx)): _*)
        input.close()
        Done("Success")
      } catch {
        case e: RepositoryException => throw new RDFClientMalformedDataException("The RDF parser returned an error to parse the message body. Well formed JSON-LD string is expected.", e)
      }
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The data was loaded successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error("The store failed to load the data.", err)
      }
    }
    future
  }

  override def load(file: File, baseURI: Option[String], format: RDFFormats.RDFFormat, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = load(file.getAbsolutePath, baseURI, format, contexts: _*)

  override def load(url: URL, baseURI: Option[String], format: RDFFormats.RDFFormat, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = {
    val future = Future {
      try {
        conn.get.add(url, baseURI.getOrElse(""), format.`type`, contexts.map(ctx => SimpleValueFactory.getInstance().createIRI(ctx)): _*)
        Done("Success")
      } catch {
        case e: RepositoryException => throw new RDFClientMalformedDataException("The RDF parser returned an error to parse the message body. Well formed JSON-LD string is expected.", e)
      }
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The data was loaded successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error("The store failed to load the data.", err)
      }
    }
    future
  }

  override def load(model: RDFModel, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = {
    val future = Future {
      try {
        conn.get.add(model.get, contexts.map(ctx => SimpleValueFactory.getInstance().createIRI(ctx)): _*)
        Done("Success")
      } catch {
        case e: RepositoryException => throw new RDFClientMalformedDataException("The RDF parser returned an error to parse the message body. Well formed JSON-LD string is expected.", e)
      }
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The data was loaded successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error("The store failed to load the data.", err)
      }
    }
    future
  }

  override def loadJsonLd(jsonld: JsValue, contexts: String*)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = {
    val future = Future {
      val reader = new StringReader(Json.stringify(jsonld))
      val m: Model = Rio.parse(reader, "", RDFFormat.JSONLD)
      try {
        conn.get.add(m)
        Done("Success")
      } catch {
        case e: RepositoryException => throw new RDFClientMalformedDataException("The RDF parser returned an error to parse the message body. Well formed JSON-LD string is expected.", e)
      }
      finally {
        reader.close()
      }
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The data was inserted successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error("The store failed to load the data.", err)
      }
    }
    future
  }

  override def select(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[RDFResultSet] = {
    val future = Future {
      val tupleQuery = conn.get.prepareTupleQuery(qryStr)
      val tuples: TupleQueryResult = tupleQuery.evaluate()
      RDFResultSet(BindingSetIterator(tuples))
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The query was executed successfully.")
      case Failure(err) => if (isVerbose) logger.error("The RDF client failed to execute the query.", err)
    }
    future
  }

  override def describe(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[RDFModel] = {
    val future = Future {
      val graphQuery = conn.get.prepareGraphQuery(qryStr)
      val model: Model = QueryResults.asModel(graphQuery.evaluate())
      RDFModel(model)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The query was executed successfully.")
      case Failure(err) => if (isVerbose) logger.error("The RDF client failed to execute the query.", err)
    }
    future
  }

  override def construct(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[RDFModel] = {
    val future = Future {
      val graphQuery = conn.get.prepareGraphQuery(qryStr)
      val model: Model = QueryResults.asModel(graphQuery.evaluate())
      RDFModel(model)
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The query was executed successfully.")
      case Failure(err) => if (isVerbose) logger.error("The RDF client failed to execute the query.", err)
    }
    future
  }

  override def ask(qryStr: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[Boolean] = {
    val future = Future {
      conn.get.prepareBooleanQuery(qryStr).evaluate()
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The query was executed successfully.")
      case Failure(err) => if (isVerbose) logger.error("The RDF client failed to execute the query.", err)
    }
    future
  }

  override def update(updateStr: String)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection): Future[Done] = {
    val future = Future {
      val update = conn.get.prepareUpdate(updateStr)
      update.execute()
      Done("Success")
    }
    future onComplete {
      case Success(_) => if (isVerbose) logger.info("The query was executed successfully.")
      case Failure(err) => {
        if (isVerbose) logger.error("The RDF client failed to execute the query.", err)
      }
    }
    future
  }

  @throws[RDFClientConnectException]
  private def getConnection(repositoryName: String): RepositoryConnection = {
    try {
      val repo = if (repositories.contains(repositoryName)) repositories(repositoryName) else repositoryManager.getRepository(repositoryName)
      val conn = repo.getConnection
      conn
    } catch {
      case t: Throwable => throw new RDFClientConnectException("Unable to connect to the SPARQL Read endpoint", t)
    }
  }

  override def getReadConnection(repositoryName: String): RDFClientReadConnection = RDFClientReadConnection(repositoryName, getConnection(repositoryName))

  override def getWriteConnection(repositoryName: String): RDFClientWriteConnection = RDFClientWriteConnection(repositoryName, getConnection(repositoryName))
}
