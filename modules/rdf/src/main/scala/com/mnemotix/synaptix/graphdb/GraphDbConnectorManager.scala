/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.graphdb

object GraphDbConnectorManager {

  // todo add this request
  // PREFIX elastic: <http://www.ontotext.com/connectors/elasticsearch#>
  // PREFIX elastic-index: <http://www.ontotext.com/connectors/elasticsearch/instance#>
  //
  //SELECT ?createString {
  //  elastic-index:dev-datapoc-agent elastic:listOptionValues ?createString .
  //}
  // by concatenate the name of the cnt to elastic-index

  def listConnectors = {
    val qry = """PREFIX elastic: <http://www.ontotext.com/connectors/elasticsearch#>
                |
                |SELECT ?cntUri ?cntStr ?cntStatus {
                |  ?cntUri elastic:listConnectors ?cntStr .
                |  ?cntUri elastic:connectorStatus ?cntStatus .
                |}
                |""".stripMargin
  }

  /**
    * Dropping a connector instance removes all references to its external store from GraphDB as well as the Elasticsearch index associated with it.
    * @param connectorName
    */

  def dropConnector(indexName: String) = {
    val qry = s"""PREFIX elastic: <http://www.ontotext.com/connectors/elasticsearch#>
                |PREFIX elastic-index: <http://www.ontotext.com/connectors/elasticsearch/instance#>
                |
                |INSERT DATA {
                |  elastic-index:$indexName elastic:dropConnector [] .
                |}""".stripMargin
  }

}
