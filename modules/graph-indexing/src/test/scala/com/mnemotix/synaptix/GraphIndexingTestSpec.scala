/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.IndexingHelper
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-03-12
 */

class GraphIndexingTestSpec extends AnyWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with LazyLogging {

  override implicit val patienceConfig = PatienceConfig(Duration.Inf)

  implicit val system = ActorSystem("SynaptixTestSpec-"+System.currentTimeMillis())
  implicit val ec = system.dispatcher

  override protected def beforeAll(): Unit = {
    RDFClient.init()
    IndexingHelper.init()
  }

  override protected def afterAll(): Unit = {
    RDFClient.shutdown()
    IndexingHelper.shutdown()
    system.terminate()
  }
}