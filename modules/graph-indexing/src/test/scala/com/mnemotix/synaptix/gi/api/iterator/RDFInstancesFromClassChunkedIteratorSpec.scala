/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.iterator

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.synaptix.GraphIndexingTestSpec
import com.mnemotix.synaptix.rdf.client.RDFClient

import scala.concurrent.duration.Duration

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 31/01/2020
 */

class RDFInstancesFromClassChunkedIteratorSpec extends GraphIndexingTestSpec {

  override implicit val patienceConfig:PatienceConfig = PatienceConfig(Duration.Inf)

  val repositoryName = "graph-indexing"
  val clazz = "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot"

  val chunkSize = 100

  "SparqlClassChunkedIterator" should {
    "iterate over classes" in {
      implicit val conn = RDFClient.getReadConnection(repositoryName)

      val counter = new RDFInstancesFromClassCounter(clazz)
      val cnt = counter.count()(ec, conn).futureValue
      println(cnt)

      val it = new RDFInstancesFromClassChunkedIterator(clazz, chunkSize, cnt)(ec, conn)
      val start = System.currentTimeMillis()
      val src: Source[String, NotUsed] = it.toSource()

      var processed = 0
      val f1 = Flow[Seq[String]].map { s =>
        processed += s.size
      }

      val f = src.grouped(100).via(f1).runWith(Sink.ignore)
      f.futureValue

      processed shouldEqual cnt
      val end = System.currentTimeMillis()
      println(s"Process took ${end - start} ms")
    }
  }
}
