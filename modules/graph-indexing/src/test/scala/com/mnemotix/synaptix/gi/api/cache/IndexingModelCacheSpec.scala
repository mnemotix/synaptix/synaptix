/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.cache

import com.mnemotix.synaptix.GraphIndexingTestSpec
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.{JsObject, JsString, Json}

import scala.concurrent.duration.Duration

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 25/11/2020
 */

class IndexingModelCacheSpec extends GraphIndexingTestSpec {

  override implicit val patienceConfig:PatienceConfig = PatienceConfig(Duration.Inf)

  val repositoryName = "graph-indexing"

  "IndexingModelCache" should {
    "retrieve indexing models from the graph" in {
      implicit val conn = RDFClient.getReadConnection(repositoryName)
      IndexingModelCache.build()
      println(IndexingModelCache.getQuery("http://data.clairsienne.com/data/2019/12/clr-patrimoine/lot#000001", "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot").get)
      println(Json.prettyPrint(IndexingModelCache.getMapping("http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot").get))
      println(Json.prettyPrint(JsObject(IndexingModelCache.getDatatypes("http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot").get.map{case(k,v) => k -> JsString(v)})))
      IndexingModelCache.models.size shouldBe > (0)
    }
  }
}
