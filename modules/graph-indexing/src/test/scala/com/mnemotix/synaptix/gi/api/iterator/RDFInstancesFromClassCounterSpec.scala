/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.iterator

import com.mnemotix.synaptix.GraphIndexingTestSpec
import com.mnemotix.synaptix.rdf.client.RDFClient

import scala.concurrent.duration.Duration

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/02/2020
 */

class RDFInstancesFromClassCounterSpec extends GraphIndexingTestSpec {

  override implicit val patienceConfig:PatienceConfig = PatienceConfig(Duration.Inf)

  val repositoryName = "graph-indexing"

  val clazz = "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot"

  "RDFInstancesFromClassCounter" should {
    val counter = new RDFInstancesFromClassCounter(clazz)

    "count instances from a given class inside a given repository" in {
      implicit val conn = RDFClient.getReadConnection(repositoryName)
      val cnt = counter.count().futureValue
      logger.info(cnt.toString)
      cnt.isInstanceOf[Int] shouldBe true
      cnt shouldBe >= (0)
    }
  }
}
