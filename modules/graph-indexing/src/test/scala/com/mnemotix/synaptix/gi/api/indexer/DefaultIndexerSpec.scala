/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.indexer

import com.mnemotix.synaptix.GraphIndexingTestSpec
import com.mnemotix.synaptix.gi.api.IndexingHelper
import com.mnemotix.synaptix.gi.api.cache.IndexingModelCache
import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.{JsObject, Json}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/02/2020
 */

class DefaultIndexerSpec extends GraphIndexingTestSpec {

  val repositoryName = "prod-clr"
  val indexName = repositoryName
  val clazz = "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot"
  val uri = "http://data.clairsienne.com/data/2019/12/clr-patrimoine/lot#000001"

  val chunkSize = 12

  "DefaultIndexer" should {

    "index all instances from a given class" in {

      // Build Cache
      implicit val conn = RDFClient.getReadConnection(repositoryName)
      IndexingModelCache.build()

      // Get mappings
      val mapping:JsObject = IndexingModelCache.getMapping(clazz).getOrElse(Json.obj("properties" -> Json.obj()))

      // drop existing index
      IndexingHelper.dropIndex(indexName).map(_.futureValue).getOrElse()

      // create index if mapping exists
      IndexingHelper.createIndex(indexName, ESMappingDefinitions(mapping).toMappingDefinition()).futureValue
      Thread.sleep(2000)

      // Index all instances of clazz
      val indexer = new DefaultIndexer(clazz)

      val start = System.currentTimeMillis()
      indexer.indexAll(indexName, chunkSize).futureValue
      val end = System.currentTimeMillis()

      println(s"Process took ${end - start} ms")
    }
  }
}
