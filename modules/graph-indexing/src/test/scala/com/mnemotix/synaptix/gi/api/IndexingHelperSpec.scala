/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api

import com.mnemotix.synaptix.GraphIndexingTestSpec
import com.mnemotix.synaptix.gi.api.cache.IndexingModelCache
import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.{JsObject, Json}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-06-24
 */

class IndexingHelperSpec extends GraphIndexingTestSpec {

  val repositoryName = "graph-indexing"
  val indexName = repositoryName
  val clazz = "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot"
  val uri = "http://data.clairsienne.com/data/2019/12/clr-patrimoine/lot#000001"

  "IndexingHelper" should {
    IndexingHelper.mute()

    "display indexing information" ignore {
      println(s"Index Name : $indexName")
      println(Json.prettyPrint(IndexingHelper.config()))
    }
    "drop an index" in {
      IndexingHelper.dropIndex(indexName).map(_.futureValue).getOrElse()
      IndexingHelper.indexExists(indexName).futureValue.result.exists shouldBe false
    }
    "create an index from an indexing model" in {
      implicit val conn = RDFClient.getReadConnection(repositoryName)
      IndexingModelCache.build()
      val mapping:Option[JsObject] = IndexingModelCache.getMapping("http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot")
      println(Json.prettyPrint(mapping.getOrElse(Json.obj())))

      mapping.map{json =>
        IndexingHelper.createIndex(indexName, ESMappingDefinitions(json).toMappingDefinition()).futureValue.isSuccess shouldBe true
        Thread.sleep(2000)
        IndexingHelper.indexExists(indexName).futureValue.result.exists shouldBe true
      }
    }
  }
}
