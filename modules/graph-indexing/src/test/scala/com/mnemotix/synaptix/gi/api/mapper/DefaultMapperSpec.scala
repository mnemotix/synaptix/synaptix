/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.mapper

import com.mnemotix.synaptix.GraphIndexingTestSpec
import com.mnemotix.synaptix.gi.api.cache.IndexingModelCache
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 10/12/2020
 */

class DefaultMapperSpec extends GraphIndexingTestSpec {

  val repositoryName = "graph-indexing"
  val clazz = "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#Lot"
  val uri = "http://data.clairsienne.com/data/2019/12/clr-patrimoine/lot#000001"

  "DefaultIndexer" should {
    "index all instances from a given class" in {
      // Prepare data
      implicit val conn = RDFClient.getReadConnection(repositoryName)
      IndexingModelCache.build()
      val datatypes = IndexingModelCache.getDatatypes(clazz).getOrElse(Map.empty)
      val qry = IndexingModelCache.getQuery(uri, clazz).getOrElse("Not Found")

      // Map query result to Json doc
      val mapper = new DefaultMapper
      val start = System.currentTimeMillis()
      val doc = mapper.map(qry, datatypes)
      val end = System.currentTimeMillis()
      println(s"Process took ${end - start} ms")
      println(Json.prettyPrint(doc))
    }
  }

}
