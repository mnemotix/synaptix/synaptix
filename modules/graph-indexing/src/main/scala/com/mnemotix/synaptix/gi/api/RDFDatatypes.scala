/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.gi.api

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-25
  */

object RDFDatatypes {
//  def matchDatatype(literal: Literal) = {
//    val dt = literal.getDatatype
//    dt match {
//      case XSDDatatype.XSDfloat => literal.getFloat
//      case XSDDatatype.XSDdouble => literal.getDouble
//      case XSDDatatype.XSDint => literal.getInt
//      case XSDDatatype.XSDlong => literal.getLong
//      case XSDDatatype.XSDshort => literal.getShort
//      case XSDDatatype.XSDbyte => literal.getByte
//      case XSDDatatype.XSDunsignedByte => literal.getByte
//      case XSDDatatype.XSDunsignedShort => literal.getShort
//      case XSDDatatype.XSDunsignedInt => literal.getInt
//      case XSDDatatype.XSDunsignedLong => literal.getLong
//      case XSDDatatype.XSDdecimal => literal.getDouble
//      case XSDDatatype.XSDinteger => literal.getInt
//      case XSDDatatype.XSDnonPositiveInteger => literal.getInt
//      case XSDDatatype.XSDnonNegativeInteger => literal.getInt
//      case XSDDatatype.XSDpositiveInteger => literal.getInt
//      case XSDDatatype.XSDnegativeInteger => literal.getInt
//      case XSDDatatype.XSDboolean => literal.getBoolean
//      case XSDDatatype.XSDstring => literal.getString
//      case XSDDatatype.XSDnormalizedString => literal.getString
//      case XSDDatatype.XSDanyURI => literal.getString
//      case XSDDatatype.XSDtoken => literal.getString
//      case XSDDatatype.XSDName => literal.getString
//      case XSDDatatype.XSDQName => literal.getString
//      case XSDDatatype.XSDlanguage => literal.getLanguage
//      case XSDDatatype.XSDNMTOKEN =>
//      case XSDDatatype.XSDENTITY =>
//      case XSDDatatype.XSDID =>
//      case XSDDatatype.XSDNCName =>
//      case XSDDatatype.XSDIDREF =>
//      case XSDDatatype.XSDNOTATION =>
//      case XSDDatatype.XSDhexBinary =>
//      case XSDDatatype.XSDbase64Binary =>
//      case XSDDatatype.XSDdate => DateTime.parse(literal.getString)
//      case XSDDatatype.XSDtime => literal.getLong
//      case XSDDatatype.XSDdateTime => DateTime.parse(literal.getString)
//      case XSDDatatype.XSDdateTimeStamp => literal.getLong
//      case XSDDatatype.XSDduration => literal.getInt
//      case XSDDatatype.XSDdayTimeDuration => literal.getInt
//      case XSDDatatype.XSDyearMonthDuration => literal.getInt
//      case XSDDatatype.XSDgDay => literal.getInt
//      case XSDDatatype.XSDgMonth => literal.getInt
//      case XSDDatatype.XSDgYear => literal.getInt
//      case XSDDatatype.XSDgYearMonth => literal.getInt
//      case XSDDatatype.XSDgMonthDay => literal.getInt
//    }
//  }
}
