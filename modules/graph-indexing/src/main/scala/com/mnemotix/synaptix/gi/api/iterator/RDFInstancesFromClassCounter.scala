/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.iterator

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.RDFClientReadConnection
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-06-24
 */

class RDFInstancesFromClassCounter(clazz: String) extends LazyLogging {

  def count()(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[Int] = {
    val qry =
      s"""
         |SELECT (count(distinct ?uri) as ?cnt)
         |WHERE {
         |  ?uri a <$clazz> .
         |  FILTER (!isBlank(?uri))
         |}""".stripMargin
    RDFClient.select(qry).map { rs =>
      rs.get.next().getBinding("cnt").getValue.stringValue().toInt
    }
  }

}