/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.gi.api.iterator

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.RDFClientReadConnection

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-24
  */

class RDFInstancesFromClassSource(clazz:String, chunkSize:Int, nbItems:Int)(implicit ec:ExecutionContext, conn: RDFClientReadConnection) extends Iterator[Future[Iterator[String]]] {

  val idle:Long = 0

  lazy val nbChunks: Int = Math.ceil(nbItems.toDouble / chunkSize.toDouble).toInt
  var currentChunk = 0

//  val connectionPool = mutable.HashSet[RDFClientReadConnection]()
//
//  def shutdown() = {
//    connectionPool.foreach(_.close())
//  }
//
//  sys.addShutdownHook(shutdown())

  override def hasNext: Boolean = (currentChunk < nbChunks)

  override def next(): Future[Iterator[String]] = {
    val offset = currentChunk * chunkSize
    currentChunk += 1
    if(idle > 0) Thread.sleep(idle)
    getChunk(offset, chunkSize)
  }

  def getChunk(offset: Int, size: Int): Future[Iterator[String]] = {
    val q = getInstances(clazz, offset, size)
//    connectionPool.add(conn)
    RDFClient.select(q).map(rs => rs.get).map(_.map(_.getBinding("uri").getValue.stringValue()))
  }

  def getInstances(clazz: String, offset: Int, limit: Int): String =
    s"""
       |SELECT DISTINCT ?uri WHERE {
       |  ?uri a <$clazz> .
       |   FILTER (!isBlank(?uri))
       |}
       |ORDER BY ?uri
       |LIMIT $limit
       |OFFSET $offset
      """.stripMargin

}
