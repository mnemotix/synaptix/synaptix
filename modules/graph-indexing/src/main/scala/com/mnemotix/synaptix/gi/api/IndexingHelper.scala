/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api

import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions}
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.RDFClientReadConnection
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.CreateIndexResponse
import com.sksamuel.elastic4s.requests.indexes.admin.{DeleteIndexResponse, IndexExistsResponse}
import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.JsObject

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-06-24
 */

object IndexingHelper extends LazyLogging {

  def config() = IndexClient.config()

  def init()(implicit ec: ExecutionContext) = IndexClient.init()

  def shutdown()(implicit ec: ExecutionContext) = IndexClient.shutdown()

  def mute() = IndexClient.mute()

  def verbose() = IndexClient.verbose()

  def dropIndex(indexName: String)(implicit ec: ExecutionContext):Option[Future[Response[DeleteIndexResponse]]] = {
    val exists = Await.result(IndexClient.indexExists(indexName), Duration.Inf).result.isExists
    if(exists) {
      Some(IndexClient.deleteIndex(indexName))
    }
    else None
  }

  def indexExists(indexName: String)(implicit ec: ExecutionContext) = IndexClient.indexExists(indexName)

  def createIndex(indexName: String, json: JsObject)(implicit ec: ExecutionContext) = {
    val mappings = ESMappingDefinitions(json)
    val future = IndexClient.createIndex(indexName, mappings.toMappingDefinition())
    future.onComplete {
      case Success(resp) => logger.info(resp.body.getOrElse(""))
      case Failure(err) => logger.error(err.getMessage, err)
    }
    future
  }

  def createIndex(indexName: String, mapping: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]] = IndexClient.createIndex(indexName, mapping)

  def addAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext) = IndexClient.addAlias(indexName, aliasName)

  def removeAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext) = IndexClient.removeAlias(indexName, aliasName)

  def bulkInsert(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext) = IndexClient.bulkInsert(indexName, indexables: _*)

  def insertDoc(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext) = IndexClient.insert(indexName, indexable)

  def progress(processed: Int, grandTotal: Int) = {
    if (grandTotal > 0) {
      val progress = processed
      val percent = ((progress.toDouble / grandTotal.toDouble) * 100).toInt
      val bar = new StringBuilder("[")
      for (i <- 0 to 50) {
        if (i < (percent / 2)) bar.append("=")
        else if (i == (percent / 2)) bar.append(">")
        else bar.append(" ")
      }
      bar.append(s"] ${percent.toString}%")
      println(bar.toString())
      //      print("\r" + bar.toString())
      if (progress == grandTotal) {
        println(Console.GREEN + "\nDone." + Console.RESET)
      }
    }
  }

}
