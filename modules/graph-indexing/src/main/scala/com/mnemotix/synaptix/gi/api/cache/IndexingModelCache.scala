/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.cache

import com.mnemotix.synaptix.gi.api.models.{IndexingField, IndexingModel}
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFResultSet}
import org.eclipse.rdf4j.query.BindingSet
import play.api.libs.json.JsObject

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 25/11/2020
 */

object IndexingModelCache {

  val qry =
    """
      |PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      |PREFIX im: <http://ns.mnemotix.com/ontologies/2019/1/indexing-model#>
      |PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
      |SELECT ?im ?class
      |(GROUP_CONCAT(CONCAT(?prLabel, " : <", ?prVal, ">") ; SEPARATOR = ",") AS ?prefixes)
      |?fLabel
      |?dt
      |?path
      |?multi
      |?analyzed
      |?optional
      |?analyzer
      |?ignore
      |?filterDeleted
      |?subfield
      |?subfieldLabel
      |?subfieldDatatype
      |?subfieldDataPath
      |?subfieldMulti
      |?subfieldAnalyzed
      |?subfieldAnalyzer
      |?subfieldOptional
      |?subfieldIgnore
      | WHERE {
      |    ?im a im:IndexingModel ; im:indexingModelOf ?class ; im:field ?field .
      |	   ?field rdfs:label ?fLabel ; im:fieldDatatype ?dt ; im:dataPath ?path .
      |    OPTIONAL {
      |        ?im im:prefix ?pref .
      |	       ?pref rdfs:label ?prLabel ; im:value ?prVal .
      |    }
      |    OPTIONAL {?field im:multivalued  ?multi }
      |    OPTIONAL {?field im:analyzed     ?analyzed }
      |    OPTIONAL {?field im:optional     ?optional }
      |    OPTIONAL {?field im:analyzer     ?analyzer }
      |    OPTIONAL {?field im:ignore_above ?ignore }
      |    OPTIONAL {?field im:filterDeleted ?filterDeleted }
      |    OPTIONAL {
      |        ?field im:subfield 	 ?subfield .
      |        ?subfield rdfs:label ?subfieldLabel ; im:fieldDatatype ?subfieldDatatype ; im:dataPath ?subfieldDataPath .
      |        OPTIONAL { ?subfield im:multivalued ?subfieldMulti }
      |        OPTIONAL { ?subfield im:analyzed ?subfieldAnalyzed }
      |        OPTIONAL { ?subfield im:analyzer ?subfieldAnalyzer }
      |        OPTIONAL { ?subfield im:optional ?subfieldOptional }
      |        OPTIONAL { ?subfield im:ignore_above ?subfieldIgnore }
      |    }
      |}
      |GROUP BY ?im ?class ?fLabel ?dt ?path ?multi ?analyzed ?optional ?analyzer ?ignore ?filterDeleted ?subfield ?subfieldLabel ?subfieldDatatype ?subfieldDataPath ?subfieldMulti ?subfieldAnalyzed ?subfieldAnalyzer ?subfieldOptional ?subfieldIgnore
      |""".stripMargin

  val models: mutable.Map[String, IndexingModel] = mutable.Map()

  def getQuery(uri: String, clazz: String): Option[String] = models.get(clazz).map(im => im.getQuery(uri))

  def getMapping(clazz: String): Option[JsObject] = {
    models.get(clazz).map(im => im.getMapping())
  }

  def getDatatypes(clazz: String): Option[Map[String, String]] = {
    models.get(clazz).map(im => im.getDatatypes())
  }

  def build()(implicit ec: ExecutionContext, conn: RDFClientReadConnection) = {
    val resultSet: RDFResultSet = Await.result(RDFClient.select(IndexingModelCache.qry), Duration.Inf)

    val iter = resultSet.get
    while (iter.hasNext) {
      val bs: BindingSet = iter.next()
      // MANDATORY FIELDS
      val clazz = bs.getBinding("class").getValue.stringValue()
      val fLabel = bs.getBinding("fLabel").getValue.stringValue()
      val dt = bs.getBinding("dt").getValue.stringValue()
      val path = bs.getBinding("path").getValue.stringValue()

      // OPTIONAL FIELDS
      val prefixes: Option[String] = Option(bs.getBinding("prefixes")).map(b => b.getValue.stringValue())
      val multi: Boolean = Option(bs.getBinding("multi")).exists(b => b.getValue.stringValue().equals("true"))
      val analyzed: Boolean = Option(bs.getBinding("analyzed")).exists(b => b.getValue.stringValue().equals("true"))
      val optional: Boolean = Option(bs.getBinding("optional")).exists(b => b.getValue.stringValue().equals("true"))
      val analyzer: Option[String] = Option(bs.getBinding("analyzer")).map(b => b.getValue.stringValue())
      val ignore: Option[Int] = Option(bs.getBinding("ignore")).map(b => b.getValue.stringValue().toInt)
      val filterDeleted: Boolean = Option(bs.getBinding("filterDeleted")).exists(b => b.getValue.stringValue().equals("true"))

      val subfield: Option[String] = Option(bs.getBinding("subfield")).map(b => b.getValue.stringValue())
      val subfieldLabel: Option[String] = Option(bs.getBinding("subfieldLabel")).map(b => b.getValue.stringValue())
      val subfieldDatatype: Option[String] = Option(bs.getBinding("subfieldDatatype")).map(b => b.getValue.stringValue())
      val subfieldDataPath: Option[String] = Option(bs.getBinding("subfieldDataPath")).map(b => b.getValue.stringValue())
      val subfieldMulti: Option[Boolean] = Option(bs.getBinding("subfieldMulti")).map(b => b.getValue.stringValue().equals("true"))
      val subfieldAnalyzed: Option[Boolean] = Option(bs.getBinding("subfieldAnalyzed")).map(b => b.getValue.stringValue().equals("true"))
      val subfieldAnalyzer: Option[String] = Option(bs.getBinding("subfieldAnalyzer")).map(b => b.getValue.stringValue())
      val subfieldOptional: Option[Boolean] = Option(bs.getBinding("subfieldOptional")).map(b => b.getValue.stringValue().equals("true"))
      val subfieldIgnore: Option[Int] = Option(bs.getBinding("subfieldIgnore")).map(b => b.getValue.stringValue().toInt)

      if (!IndexingModelCache.models.contains(clazz)) {
        val ns: Map[String, String] = prefixes.get.split(",").map { p =>
          val pattern = """(.*)\s*:\s*<(.*)>""".r
          p match {
            case pattern(prefix, uri) => Some(prefix.trim -> uri.trim)
            case _ => None
          }
        }.filter(o => o.isDefined).map(o => o.get).toMap
        val im = IndexingModel(clazz, ns)
        IndexingModelCache.models.put(clazz, im)
      }

      val im = IndexingModelCache.models(clazz)
      if (!im.fields.contains(fLabel)) {
        im.fields += fLabel -> IndexingField(fLabel, dt, path, multi, analyzed, optional, analyzer, ignore, filterDeleted)
      }
      val field: IndexingField = im.fields(fLabel)

      if (subfield.isDefined && subfieldLabel.isDefined) {
        if (!field.subfields.contains(subfieldLabel.get)) {
          field.subfields.put(subfieldLabel.get, IndexingField(
            subfieldLabel.get,
            subfieldDatatype.get,
            subfieldDataPath.get,
            subfieldMulti.getOrElse(false),
            subfieldAnalyzed.getOrElse(false),
            subfieldOptional.getOrElse(false),
            subfieldAnalyzer,
            subfieldIgnore
          ))
        }
      }
      im.fields += fLabel -> field
    }
  }
}
