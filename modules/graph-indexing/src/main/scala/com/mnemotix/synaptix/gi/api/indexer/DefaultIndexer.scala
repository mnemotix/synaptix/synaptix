/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.indexer

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.synaptix.gi.api.IndexingHelper
import com.mnemotix.synaptix.gi.api.cache.IndexingModelCache
import com.mnemotix.synaptix.gi.api.iterator.{RDFInstancesFromClassChunkedIterator, RDFInstancesFromClassCounter}
import com.mnemotix.synaptix.gi.api.mapper.DefaultMapper
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.mnemotix.synaptix.rdf.client.models.RDFClientReadConnection
import play.api.libs.json.JsObject

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/02/2020
 */

class DefaultIndexer(override val classToIndex: String)(implicit val ec: ExecutionContext, conn: RDFClientReadConnection) extends AbstractIndexer {

  val mapper = new DefaultMapper()

  def count(): Int = {
    val counter = new RDFInstancesFromClassCounter(classToIndex)
    val f = counter.count()
    Await.result(f, Duration.Inf)
  }

  def indexAll(indexName:String, chunkSize: Int)(implicit mat:Materializer) = {
    val c = count()
    val datatypes = IndexingModelCache.getDatatypes(classToIndex).getOrElse(Map.empty)
    val it = new RDFInstancesFromClassChunkedIterator(classToIndex, chunkSize, c)
//    val it = new RDFInstancesFromClassChunkedIterator(classToIndex, chunkSize, 500)
    val src: Source[String, NotUsed] = it.toSource()
    val f1 = Flow[String].map { uri => IndexingModelCache.getQuery(uri, classToIndex).getOrElse("Not Found") }
    val f2 = Flow[String].map { qry => mapper.map(qry, datatypes) }
    val f3 = Flow[Seq[JsObject]].map { docs =>
      val indexables = docs.map{doc:JsObject =>
        val uri = (doc \ "uri").as[String]
        ESIndexable(uri, Some(doc))
      }
      IndexingHelper.bulkInsert(indexName, indexables:_*)
    }
    val f = src.via(f1).via(f2).grouped(500).via(f3).runWith(Sink.ignore)
    f
  }
}
