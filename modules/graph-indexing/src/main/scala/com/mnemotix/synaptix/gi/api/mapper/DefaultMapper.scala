/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.mapper

import com.mnemotix.synaptix.rdf.client.models.RDFClientReadConnection
import com.typesafe.scalalogging.LazyLogging
import org.eclipse.rdf4j.query.{Binding, TupleQuery, TupleQueryResult}
import play.api.libs.json._

import scala.collection.JavaConverters.asScalaIteratorConverter
import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 10/12/2020
 */

class DefaultMapper extends AbstractMapper with LazyLogging {

  override def map(qry: String, datatypes: Map[String, String])(implicit ec: ExecutionContext, conn: RDFClientReadConnection): JsObject = {
    val query: TupleQuery = conn.get.prepareTupleQuery(qry)
    val tuples: TupleQueryResult = query.evaluate()
    var fields: Map[String, JsValue] = Map.empty
    while (tuples.hasNext) {
      val bindings: Iterator[Binding] = tuples.next().iterator().asScala
      fields ++= bindings.map { b =>
        val className = datatypes.get(b.getName).getOrElse(JsString.getClass.getName)
        className match {
          case "play.api.libs.json.JsString$" => b.getName -> JsString(b.getValue.stringValue())
          case "play.api.libs.json.JsNumber$" => b.getName -> JsNumber(b.getValue.stringValue().toDouble)
          case "play.api.libs.json.JsBoolean$" => b.getName -> JsBoolean(b.getValue.stringValue().toBoolean)
          case "JsString$" => b.getName -> JsString(b.getValue.stringValue())
          case "JsNumber$" => {
            if(b.getValue != null && b.getValue.stringValue().length >0) b.getName -> JsNumber(b.getValue.stringValue().toDouble)
            else b.getName -> JsNull
          }
          case "JsBoolean$" => b.getName -> JsBoolean(b.getValue.stringValue().toBoolean)
          case _ => {
            logger.error(s"Not found : $className")
            b.getName -> JsNull
          }
        }
      }.toMap
    }
    JsObject(fields)
  }
}
