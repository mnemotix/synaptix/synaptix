/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.models

import org.eclipse.rdf4j.sparqlbuilder.core.{Prefix, SparqlBuilder}
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.{GraphPattern, GraphPatterns}
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf.{bNode, iri}
import play.api.libs.json._

import scala.collection.{Map, mutable}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 25/11/2020
 */

case class IndexingField(
  label: String,
  datatype: String,
  path: String,
  multivalued: Boolean,
  analyzed: Boolean,
  optional: Boolean,
  analyzer: Option[String] = None,
  ignore: Option[Int] = None,
  filterDeleted: Boolean = false,
  subfields: mutable.Map[String, IndexingField] = mutable.Map()
) {

  def getMappingDataType(): JsObject = {
    datatype match {
      case "http://www.w3.org/2001/XMLSchema#boolean" => Json.obj("type" -> JsString("boolean"))
      case "http://www.w3.org/2001/XMLSchema#decimal" => Json.obj("type" -> JsString("float"))
      case "http://www.w3.org/2001/XMLSchema#long" => Json.obj("type" -> JsString("long"))
      case "http://www.w3.org/2001/XMLSchema#float" => Json.obj("type" -> JsString("float"))
      case "http://www.w3.org/2001/XMLSchema#date" => Json.obj("type" -> JsString("date"))
      case "http://www.w3.org/2001/XMLSchema#dateTime" => Json.obj("type" -> JsString("date"))
      case "http://www.w3.org/2001/XMLSchema#time" => Json.obj("type" -> JsString("date_nanos"))
      case "http://www.w3.org/2001/XMLSchema#integer" => Json.obj("type" -> JsString("integer"))
      case "http://www.w3.org/2001/XMLSchema#int" => Json.obj("type" -> JsString("integer"))
      case "http://www.w3.org/2001/XMLSchema#anyURI" => Json.obj("type" -> JsString("keyword")) ++ ignoreClause()
      case "http://www.w3.org/2001/XMLSchema#string" => {
        if (analyzed && multivalued) Json.obj("type" -> JsString("text"), "fields" -> Json.obj("keyword" -> (Json.obj("type" -> JsString("keyword")) ++ ignoreClause()))) ++ analyzerClause()
        else if (analyzed) Json.obj("type" -> JsString("text")) ++ analyzerClause()
        else Json.obj("type" -> JsString("keyword")) ++ ignoreClause()
      }
      case "http://ns.mnemotix.com/ontologies/2019/8/generic-model/geo_point" => Json.obj("type" -> JsString("geo_point"))
      case "http://ns.mnemotix.com/ontologies/2019/8/generic-model/geo_shape" => Json.obj("type" -> JsString("geo_shape"))
      case "http://ns.mnemotix.com/ontologies/2019/8/generic-model/percolator" => Json.obj("type" -> JsString("percolator"))
      case "http://www.opengis.net/ont/geosparql#wktLiteral" => Json.obj("type" -> JsString("geo_shape"))
    }
  }

  def getStatement(uri: String): String = {
    val builder = mutable.StringBuilder.newBuilder
    if (optional) builder.append("\tOPTIONAL { ") else builder.append("\t{ ")
    if (uri.startsWith("?")) builder.append(s"\t$uri $path ?$label . ")
    else builder.append(s"\t<$uri> $path ?$label . ")
    if (filterDeleted) builder.append(s"${filterClause()} . ")
    subfields.values.foreach { sf => builder.append(sf.getStatement(s"?$label")) }
    builder.append("}")
    builder.toString()
  }

  def getPattern(uri: String, prefs: Map[String, Prefix]): GraphPattern = {
    val mnx = SparqlBuilder.prefix("mnx", iri("http://ns.mnemotix.com/ontologies/2019/8/generic-model/"))
    // retrieve prefix from path and generate iri
    val pathComponents: Array[String] = path.split(":")
    val pathPrefix: Option[Prefix] = prefs.get(pathComponents(0).trim)
    val pathIri = pathPrefix.map(p => p.iri(pathComponents(1).trim)).getOrElse(iri(path))
    // create graph pattern
    val pattern: GraphPattern = if (optional) {
      GraphPatterns.optional(iri(uri).has(pathIri, SparqlBuilder.`var`(label)))
    } else {
      iri(uri).has(pathIri, SparqlBuilder.`var`(label))
    }
    // append delete filter if any
    if (filterDeleted) {
      pattern.filterNotExists(
        SparqlBuilder.`var`(label).has(mnx.iri("hasDeletion"), bNode("action")),
        bNode("action").isA(mnx.iri("Deletion"))
      )
    }
    // append subfields
    //    subfields.values.foreach { sf =>
    //      builder.append(sf.getStatement(s"?$label"))
    //    }
    pattern
  }

  def getMapping(): JsObject = {
    var mappings = Json.obj(
      label -> getMappingDataType()
    )
    if (subfields.size > 0) {
      val submappings: JsObject = subfields.values.map { sf => sf.getMapping() }.reduce(_ ++ _)
      mappings ++= submappings
    }
    mappings
  }

  def getDatatypes(): scala.collection.immutable.Map[String, String] = {
    val dt:String = datatype match {
      case "http://www.w3.org/2001/XMLSchema#boolean" => JsBoolean.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#decimal" => JsNumber.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#long" => JsNumber.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#float" => JsBoolean.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#date" => JsBoolean.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#dateTime" => JsString.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#time" => JsNumber.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#integer" => JsNumber.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#int" => JsNumber.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#anyURI" => JsString.getClass.getSimpleName
      case "http://www.w3.org/2001/XMLSchema#string" => JsString.getClass.getSimpleName
      case "http://ns.mnemotix.com/ontologies/2019/8/generic-model/geo_point" => JsString.getClass.getSimpleName
      case "http://ns.mnemotix.com/ontologies/2019/8/generic-model/geo_shape" => JsString.getClass.getSimpleName
      case "http://ns.mnemotix.com/ontologies/2019/8/generic-model/percolator" => JsString.getClass.getSimpleName
      case "http://www.opengis.net/ont/geosparql#wktLiteral" => JsString.getClass.getSimpleName
    }
    var datatypes = scala.collection.immutable.Map(label -> dt)

    if (subfields.size > 0) {
      val submappings: Map[String, String] = subfields.values.map { sf => sf.getDatatypes() }.reduce(_ ++ _)
      datatypes ++= submappings
    }
    datatypes
  }

  def getIri() = s"NOT EXISTS { ?$label mnx:hasDeletion/rdf:type mnx:Deletion }";

  def filterClause() = s"FILTER NOT EXISTS { ?$label mnx:hasDeletion/rdf:type mnx:Deletion }";

  def ignoreClause() = ignore.map { i => Json.obj("ignore_above" -> JsNumber(i)) }.getOrElse(Json.obj())

  def analyzerClause() = if (analyzed) Json.obj("analyzer" -> JsString(analyzer.getOrElse("standard"))) else Json.obj()

  def subfieldsStatements() = subfields.map(sf => sf)

}