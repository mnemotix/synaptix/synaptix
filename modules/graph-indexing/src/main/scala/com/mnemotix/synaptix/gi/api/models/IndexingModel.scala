/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api.models

import org.eclipse.rdf4j.sparqlbuilder.core.{Prefix, SparqlBuilder}
import org.eclipse.rdf4j.sparqlbuilder.core.query.{Queries, SelectQuery}
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern
import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf._
import play.api.libs.json.{JsObject, Json}

import scala.collection.mutable

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 25/11/2020
 */

case class IndexingModel(
  clazz: String,
  prefixes: Map[String, String] = Map(),
  fields: mutable.Map[String, IndexingField] = mutable.Map()
) {
  def getPattern(uri: String): SelectQuery = {
    val selectQuery = Queries.SELECT()
    val prefs: Map[String, Prefix] = prefixes.map { case (k, v) => k -> SparqlBuilder.prefix(k, iri(v)) }
    val node: Iri = iri(uri)
    val patterns: Seq[GraphPattern] = Seq(node.isA(iri(clazz))) ++ fields.values.map(f => f.getPattern(uri, prefs)).toSeq
    selectQuery.prefix(prefs.values.toList: _*).distinct().select().where(patterns: _*)
  }

  def getQuery(uri: String): String = {
    val builder = mutable.StringBuilder.newBuilder
    prefixes.foreach { case (prefix, namespace) =>
      builder.append(s"PREFIX $prefix:<$namespace>\n")
    }
    builder.append("SELECT DISTINCT * WHERE {\n")
    builder.append(s"\t?uri a <$clazz> .\n")
    builder.append(s"\tFILTER(?uri = <$uri>) .\n")
    builder.append(fields.values.map(f => f.getStatement(uri)).mkString("\n"))
    builder.append("\n}")
    builder.toString()
  }

  def getMapping(): JsObject = {
    val fieldMappings: JsObject = fields.values.map(f => f.getMapping()).reduce(_ ++ _)
    val m = Json.obj("properties" -> fieldMappings)
    m
  }

  def getDatatypes(): scala.collection.immutable.Map[String, String] = {
    fields.values.map(f => f.getDatatypes()).reduce(_ ++ _)
  }
}