/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gi.api

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.RDFClientReadConnection

import scala.collection.mutable
import scala.util.Random

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 06/02/2020
 */

class RDFClientConnectionPoolManager(repositoryName:String, poolSize:Int=4) {
  private val readingPool = mutable.HashSet[RDFClientReadConnection]()

  lazy val random = new Random

  sys addShutdownHook(shutdown())

  def refresh() = {
    readingPool.foreach { conn =>
      if (!conn.get.isOpen) readingPool.remove(conn)
    }
  }

  def shutdown() = {
    readingPool.foreach(_.close())
  }

  def getReadConnection(): RDFClientReadConnection = {
    refresh()
    readingPool.find(!_.get.isActive).getOrElse{
      if(readingPool.size < poolSize) {
        val conn = RDFClient.getReadConnection(repositoryName)
        readingPool.add(conn)
        conn
      }
      else {
        readingPool.toSeq(random.nextInt(readingPool.size))
      }
    }
  }
}
