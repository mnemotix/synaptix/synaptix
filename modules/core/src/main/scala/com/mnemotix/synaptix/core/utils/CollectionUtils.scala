/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.core.utils

import com.mnemotix.synaptix.core.utils.StringUtils._
import play.api.libs.json._

/**
 * Created by Nicolas Delaforge on 13/09/2017.
 */

object CollectionUtils {



  /*
  def mapValues(map: Map[String, Any]): JsObject = JsObject(
      map.mapValues {
        case s: String => JsString(s)
        case i: Int => JsNumber(i)
        case l: Long => JsNumber(l)
        case bd: BigDecimal => JsNumber(bd)
        case f: Float => JsNumber(f.toDouble)
        case d: Double => JsNumber(d)
        case b: Boolean => JsBoolean(b)
        case da: Date => JsNumber(da.getTime)
        case dt: DateTime => JsNumber(dt.getMillis)
        case any: _ => JsString(any.toString)
      }
  )

   */

  def mapToJson(map: Map[String, Any]): JsObject = Json.toJson(MapAny(map)).as[JsObject]

  def jsObjectToProps(parentKey:Option[String], tuple:(String, JsValue)):Seq[(String, AnyRef)] = {
    val fullKey = if(parentKey.isDefined) parentKey.get + tuple._1.capitalize else tuple._1
    tuple._2 match {
      case s: JsString => Seq(q(fullKey) -> s.toString.replace("$", "\\$"))
      case n: JsNumber => Seq(q(fullKey) -> n.value).asInstanceOf[Seq[(String, AnyRef)]]
      case b: JsBoolean => Seq(q(fullKey) -> b.value).asInstanceOf[Seq[(String, AnyRef)]]
      case b: JsObject => b.fields.toSeq.flatMap(t => jsObjectToProps(Some(fullKey), t))
      case a: JsArray => {
        a.value.toSeq.flatMap{jsval:JsValue =>
          jsObjectToProps(Some(fullKey), (String.valueOf(a.value.indexOf(jsval)), jsval))
        }
      }
      case default => Seq("'"+fullKey+"'" -> default.toString())
    }
  }
}