/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.core.utils

import java.lang.reflect.Field
import java.util.Date
import org.joda.time.DateTime

import scala.reflect.ClassTag

/**
 * Created by Nicolas Delaforge on 13/09/2017.
 */

object SortUtils {

  def sort[A: ClassTag](sortBy: Option[String], order: Option[String], items: List[A]) = {

    var sortedList=items

    // on récupère la liste des champs de la classe A
    val fields: Seq[String] = scala.reflect.classTag[A].runtimeClass.getDeclaredFields.map(_.getName).toSeq
    // on verifie que le champ sortBy est défini et existe dans la classe A
    if (sortBy.isDefined && fields.contains(sortBy.get)) {
      // on récupère le champ
      val field: Field = scala.reflect.classTag[A].runtimeClass.getDeclaredField(sortBy.get)
      field.setAccessible(true)
      // on récupère son type
      val fieldType=field.getType.getSimpleName.toLowerCase
      val supportedTypes = Seq("int", "long", "double", "float", "bigdecimal", "string", "boolean", "datetime", "date")
      // si le type est supporté, on trie
      if(supportedTypes.contains(fieldType)){
        sortedList = sortedList.sortWith { (left, right) =>
          fieldType match {
            case "int" => field.getInt(left) < field.getInt(right)
            case "long" => field.getLong(left) < field.getLong(right)
            case "double" => field.getDouble(left) < field.getDouble(right)
            case "float" => field.getFloat(left) < field.getFloat(right)
            case "bigdecimal" => field.get(left).asInstanceOf[BigDecimal] < field.get(right).asInstanceOf[BigDecimal]
            case "string" => field.get(left).asInstanceOf[String] < field.get(right).asInstanceOf[String]
            case "boolean" => field.getBoolean(left) < field.getBoolean(right)
            case "datetime" => field.get(left).asInstanceOf[DateTime].isBefore(field.get(right).asInstanceOf[DateTime])
            case "date" => field.get(left).asInstanceOf[Date].before(field.get(right).asInstanceOf[Date])
          }
        }
      }
    }
    if (order.isDefined && order.get == "desc") sortedList.reverse else sortedList
  }
}
