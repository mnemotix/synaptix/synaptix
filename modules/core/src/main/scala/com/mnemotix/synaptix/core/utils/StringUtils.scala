/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.core.utils

import java.text.Normalizer
import java.util.StringTokenizer
import akka.stream.scaladsl.Source
import com.mnemotix.synaptix.core.nlp
import com.mnemotix.synaptix.core.nlp.StopWordsFilter

import java.net.URI
import scala.collection.JavaConverters._

/**
 * Created by Nicolas Delaforge on 13/09/2017.
 */

object StringUtils {

  lazy val PUNCTUATION: Seq[String] = Seq(",",".",";","!","?","-",":","_")

  def q(field:String):String = "'"+field+"'"
  def qq(field:String):String = "\""+field+"\""

  def tokenize(lang:String , str:String):Iterable[String] = StopWordsFilter.filterWords(lang, new StringTokenizer(str).asScala.asInstanceOf[Iterator[String]].toIterable)

  def normalize(str:String):String = nlp.Normalizer.normalize(str)

  def slugify(input: String): String = {
    import java.text.Normalizer
    Normalizer.normalize(input, Normalizer.Form.NFD)
      .replaceAll("[^\\w\\s-]", "") // Remove all non-word, non-space or non-dash characters
      .replace('-', ' ')            // Replace dashes with spaces
      .trim                         // Trim leading/trailing whitespace (including what used to be leading/trailing dashes)
      .replaceAll("\\s+", "-")      // Replace whitespace (including newlines and repetitions) with single dashes
      .toLowerCase                  // Lowercase the final results
  }

  def stripAccents(str:String):String = {
    val s = Normalizer.normalize(str, Normalizer.Form.NFD)
    s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
  }

  def stripPunctuation(str: String): String = {
    str.replaceAll("[\\p{Punct}]", " ")
  }

  def removeNonPrintableCharachter(str: String): String = {
    str.replaceAll("[\\p{C}&&\\S]", "")
  }

  def removeTrailingSlashes(str: String): String = str.stripSuffix("/")

  def lastPathOfUri(uri: String): String = {
    lastPathOfUri(new URI(uri))
  }

  def lastPathOfUri(uri: URI): String = {
    val path = uri.getPath()
    path.substring(path.lastIndexOf('/') + 1)
  }
}