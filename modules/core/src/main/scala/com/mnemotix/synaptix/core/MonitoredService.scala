/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.core

import scala.collection.mutable

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-01-29
  */

trait MonitoredService extends GenericService {

  val serviceName: String

  val listeners = mutable.HashMap[String, mutable.ArrayBuffer[ServiceListener]]()

  def registerListener(key: String, listener: ServiceListener) = {
    if (isVerbose) logger.info(s"""Service "$serviceName" registered a listener (${listener.getClass.getSimpleName}) on key [$key].""")
    val buffer = if (listeners.contains(key)) listeners(key) else mutable.ArrayBuffer[ServiceListener]()
    buffer.append(listener)
    listeners.put(key, buffer)
  }

  def unregisterListener(key: String) = {
    if (isVerbose) logger.info(s"""Service "$serviceName" unregistered all listeners on key [$key].""")
    listeners -= key
  }

  def notify(key: NotificationValues.NotificationValue, message: Option[String] = None) = listeners.values.foreach { arr =>
    key match {
      case NotificationValues.STARTUP => arr.foreach(l => l.onStartup())
      case NotificationValues.SHUTDOWN => arr.foreach(l => l.onShutdown())
      case NotificationValues.DONE => arr.foreach(l => l.onDone())
      case NotificationValues.TERMINATE => arr.foreach(l => l.onTerminate())
      case NotificationValues.ABORT => arr.foreach(l => l.onAbort())
      case NotificationValues.UPDATE => arr.foreach(l => l.onUpdate(message.getOrElse("")))
    }
  }

  def notifyProgress(processedItems: Int, totalItems: Option[Int], message: Option[String]) = listeners.values.foreach { arr => arr.foreach(l => l.onProgress(processedItems, totalItems, message)) }

  def notifyError(errorMessage: String, cause: Option[Throwable] = None) = listeners.values.foreach { arr => arr.foreach(l => l.onError(errorMessage, cause)) }
}
