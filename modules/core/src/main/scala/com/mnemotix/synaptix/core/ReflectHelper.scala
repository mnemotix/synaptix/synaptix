/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.core

import scala.reflect.{ClassTag, api}

// Runtime universe
import scala.reflect.runtime.universe._
import scala.reflect.runtime.{universe => ru}


/**
  * Provides functionality for obtaining reflective information about
  * classes and objects.
  */
object ReflectHelper {

  /*
   * Reflection API basic principles :
   * 1 - Get a runtime universe : val ru = scala.reflect.runtime.universe
   * 2 - Get a runtime mirror : val rm = ru.runtimeMirror(getClass.getClassLoader)
   *
   * There are 2 main types of mirrors inside the reflection API :
   *   - "Classloader" mirrors : These mirrors translate names to symbols (via methods
   *     staticClass/staticModule/staticPackage).
   *   - "Invoker" mirrors : These mirrors implement reflective invocations (via methods MethodMirror.apply,
   *     FieldMirror.get, etc.). These “invoker” mirrors are the types of mirrors that are most commonly used.
   *
   * We can use the runtime mirror directly to get an instance mirror of an object :
   *    class C { def x = 2 }
   *    val im = rm.reflect(new C)
   *
   */

  def rm: Mirror = ru.runtimeMirror(getClass.getClassLoader)

  def reflectInstance(obj: Any): InstanceMirror = rm.reflect(obj)

  def reflectClass(cs: ClassSymbol): ClassMirror = rm.reflectClass(cs)

  def reflectModule(module: ModuleSymbol): ModuleMirror = rm.reflectModule(module)

  def reflectField(im:InstanceMirror, ts:TermSymbol): FieldMirror = im.reflectField(ts)

  def reflectMethod(im:InstanceMirror, ms:MethodSymbol): MethodMirror = im.reflectMethod(ms)

  def reflectConstructor(cm:ClassMirror, ms:MethodSymbol):MethodMirror = cm.reflectConstructor(ms)
  /*
   * REFLECTION FROM INSTANCE
   */

  def classSymbolOf[T: ru.TypeTag](obj: T): ClassSymbol = classSymbolOf[T]

  def classMirrorOf[T: ru.TypeTag](obj: T): ClassMirror = reflectClass(classSymbolOf(obj))

  def constructorOf[T: ru.TypeTag](obj: T): MethodSymbol = constructorOf[T]

  def constructorMirrorOf[T: ru.TypeTag](obj: T): MethodMirror = reflectConstructor(classMirrorOf(obj), constructorOf(obj))

  def membersOf[T: ru.TypeTag](obj: T): MemberScope = membersOf[T]

  def moduleOf[T: ru.TypeTag](obj: T): ModuleSymbol = moduleOf[T]

//  def reflectModule[T: ru.TypeTag](obj: T): ModuleMirror = reflectModule(moduleOf[T])

  def typeTagOf[T: ru.TypeTag](obj: T): TypeTag[T] = ru.typeTag[T]

  def tpeOf[T: ru.TypeTag](obj: T): Type = ru.typeOf[T]

  /*
   * REFLECTION FROM NAME
   */

  def tpeOf(className: String): Type = typeTagOf(className).tpe

  def classSymbolOf(className: String): ClassSymbol = rm.staticClass(className)

  def classMirrorOf[T: ru.TypeTag](className: String): ClassMirror = reflectClass(classSymbolOf(className))

  def constructorOf(className: String): MethodSymbol = tpeOf(className).decl(termNames.CONSTRUCTOR).asMethod

  def constructorMirrorOf(className: String): MethodMirror = reflectConstructor(classMirrorOf(className), constructorOf(className))

  def instanceMirrorOf(className: String): InstanceMirror = rm.reflect(classSymbolOf(className: String))

  def instanceFromName(className: String, args: Any*) = constructorMirrorOf(className).apply(args: _*)

  def membersOf(className: String): MemberScope = tpeOf(className).members

  def moduleOf(className: String): ModuleSymbol = rm.staticModule(className)

//  def reflectModule(className: String): ModuleMirror = reflectModule(moduleOf(className))

  def typeTagOf[T: ru.TypeTag](className: String): TypeTag[T] = {
    val mirror = rm
    val sym = mirror.staticClass(className) // obtain class symbol for `c`
    val tpe = sym.selfType // obtain type object for `c`
    // create a type tag which contains above type object
    TypeTag(mirror, new api.TypeCreator {
      def apply[U <: api.Universe with Singleton](m: api.Mirror[U]) =
        if (m eq mirror) tpe.asInstanceOf[U#Type]
        else throw new IllegalArgumentException(s"Type tag defined in $mirror cannot be migrated to other mirrors.")
    })
  }

  /*
   * REFLECTION FROM TYPE
   */

  def classSymbolOf[T: ru.TypeTag]: ClassSymbol = typeOf[T].typeSymbol.asClass

  def constructorOf[T: ru.TypeTag]: MethodSymbol = typeOf[T].decl(termNames.CONSTRUCTOR).asMethod

  def classMirrorOf[T: ru.TypeTag]: ClassMirror = reflectClass(classSymbolOf[T])

  def constructorMirrorOf[T: ru.TypeTag]: MethodMirror = reflectConstructor(classMirrorOf[T], constructorOf[T])

  def instanceOf[T: ru.TypeTag](args: Any*): T = constructorMirrorOf[T].apply(args: _*).asInstanceOf[T]

  def membersOf[T: ru.TypeTag]: MemberScope = typeOf[T].members

  def moduleOf[T: ru.TypeTag]: ModuleSymbol = rm.staticModule(typeOf[T].typeSymbol.fullName)

//  def reflectModule[T: ru.TypeTag]: ModuleMirror = reflectModule(moduleOf[T])

  def typeTagOf[T: ru.TypeTag]: TypeTag[T] = ru.typeTag[T]

  def declsOf[T: ru.TypeTag]: MemberScope = typeOf[T].decls

  def declsOf[T: ru.TypeTag](obj: T): MemberScope = declsOf[T]

  def declsOf(className: String): MemberScope = tpeOf(className).decls


  def memberOf[T: ru.TypeTag](methodName: String): MethodSymbol = typeOf[T].member(TermName(methodName)).asMethod

  def memberOf[T: ru.TypeTag](obj: T, methodName: String): MethodSymbol = memberOf[T](methodName)

  def memberOf(className: String, methodName: String): MethodSymbol = tpeOf(className).member(TermName(methodName)).asMethod


  def declOf[T: ru.TypeTag](methodName: String): MethodSymbol = typeOf[T].decl(TermName(methodName)).asMethod

  def declOf[T: ru.TypeTag](obj: T, methodName: String): MethodSymbol = memberOf[T](methodName)

  def declOf(className: String, methodName: String): MethodSymbol = tpeOf(className).decl(TermName(methodName)).asMethod


  def termOf[T: ru.TypeTag](termName: String): TermSymbol = typeOf[T].member(TermName(termName)).asTerm

  def termOf[T: ru.TypeTag](obj: T, termName: String): TermSymbol = termOf[T](termName)

  def termOf(className: String, termName: String): TermSymbol = tpeOf(className).member(TermName(termName)).asTerm


  def methodMirrorOf[T: ClassTag](obj: T, methodName: String): MethodMirror = {
    val cname = obj.getClass.getName
    reflectInstance(obj).reflectMethod(memberOf(cname, methodName))
  }

  def fieldMirrorOf[T: ClassTag](obj: T, fieldName: String): FieldMirror = {
    val cname = obj.getClass.getName
    reflectInstance(obj).reflectField(memberOf(cname, fieldName))
  }

//  def methodMirrorOf[T: ru.TypeTag](obj: T, methodName: String): MethodMirror = {
//    val cname = obj.getClass.getName
//    instanceMirrorOf(obj).reflectMethod(memberOf(cname, methodName))
//  }
//
//  def fieldMirrorOf[T: ru.TypeTag](obj: T, fieldName: String): FieldMirror = {
//    val cname = obj.getClass.getName
//    instanceMirrorOf(obj).reflectField(memberOf(cname, fieldName))
//  }

  def methodParamsOf[T: ru.TypeTag](methodName: String): List[Symbol] = declOf[T](methodName).paramLists.head.map(_.typeSignature.typeSymbol)

  def methodReturnType[T: ru.TypeTag](methodName: String): Type = declOf[T](methodName).returnType

  def companionSymbolOf(className: String): Symbol = reflectClass(classSymbolOf(className)).symbol.companion

  def companionModuleMirrorOf(className: String): ModuleMirror = reflectModule(companionSymbolOf(className).asModule)

  def companionInstanceMirrorOf(className: String): InstanceMirror = reflectInstance(companionModuleMirrorOf(className).instance)

  def companionTypeOf(className: String): Type = companionInstanceMirrorOf(className).symbol.toType

  def companionTermOf(className: String, termName: String): TermSymbol = companionTypeOf(className).member(TermName(termName)).asTerm

  def companionMemberOf(className: String, methodName: String): MethodSymbol = companionTypeOf(className).member(TermName(methodName)).asMethod

  def classAnnotationsOf[T: ru.TypeTag]: List[Annotation] = classSymbolOf[T].annotations

  def classAnnotationsOf[T: ru.TypeTag](obj: T): List[Annotation] = classAnnotationsOf[T]

  def classAnnotationsOf(className: String): List[Annotation] = classSymbolOf(className).annotations

  def getAnnotationsFrom(methodSymbol: MethodSymbol): Map[Symbol, List[Annotation]] = {
    methodSymbol.paramLists.head.map { s =>
      s -> s.annotations
    }.toMap
  }

  def constructorAnnotationsOf[T: ru.TypeTag]: Map[Symbol, List[Annotation]] = getAnnotationsFrom(constructorOf[T])

  def constructorAnnotationsOf[T: ru.TypeTag](obj: T): Map[Symbol, List[Annotation]] = constructorAnnotationsOf[T]

  def constructorAnnotationsOf(className: String): Map[Symbol, List[Annotation]] = getAnnotationsFrom(constructorOf(className))

  def membersAnnotationsOf[T: ru.TypeTag]: Map[Symbol, List[Annotation]] = {
    membersOf[T].collect {
      case s: MethodSymbol if s.annotations.length > 0 => s -> s.annotations
      case s: TermSymbol if s.annotations.length > 0 => s -> s.annotations
    }.toMap
  }

  def membersAnnotationsOf[T: ru.TypeTag](obj: T): Map[Symbol, List[Annotation]] = membersAnnotationsOf[T]

  def membersAnnotationsOf(className: String): Map[Symbol, List[Annotation]] = {
    membersOf(className).collect {
      case s: MethodSymbol if s.annotations.length > 0 => s -> s.annotations
      case s: TermSymbol if s.annotations.length > 0 => s -> s.annotations
    }.toMap
  }

  //  /** **************
  //    * ANNOTATIONS
  //    * *****************/
  //
  //
  //  /**
  //    * Returns a `Map` from annotation names to annotation data for
  //    * the specified type.
  //    *
  //    * @tparam T The type to get class annotations for.
  //    * @return The class annotations for `T`.
  //    */
  //  def classAnnotations[T: TypeTag]: Map[String, Map[String, Any]] = parseTypeDeclaration(classSymbolOf[T])
  //
  //  def classAnnotations(className: String): Map[String, Map[String, Any]] = parseTypeDeclaration(classSymbolOf(className))
  //
  //  def constructorAnnotations[T: TypeTag]: Map[String, Map[String, Any]] = parseConstructor(constructorOf[T])
  //
  //  def constructorAnnotations(className: String): Map[String, Map[String, Any]] = parseConstructor(constructorOf(className))
  //
  //  /**
  //    * Returns a `Map` from method names to a `Map` from annotation names to
  //    * annotation data for the specified type.
  //    *
  //    * @tparam T The type to get method annotations for.
  //    * @return The method annotations for `T`.
  //    */
  //  def methodAnnotations[T: TypeTag]: Map[String, Map[String, Map[String, Any]]] = parseMembers(declsOf[T])
  //
  //  def methodAnnotations(className: String): Map[String, Map[String, Map[String, Any]]] = parseMembers(declsOf(className))
  //
  //
  //  def parseTypeDeclaration(classSymbol: ClassSymbol): Map[String, Map[String, Any]] = {
  //    classSymbol.annotations.map { annot: Annotation =>
  //      annot.tree.tpe.typeSymbol.name.toString -> annot.tree.children.withFilter {
  //        _.productPrefix eq "AssignOrNamedArg"
  //      }.map { tree =>
  //        tree.productElement(0).toString -> tree.productElement(1)
  //      }.toMap
  //    }.toMap
  //  }
  //
  //  def parseConstructor(constructor: MethodSymbol): Map[String, Map[String, Any]] = {
  //    constructor.paramLists.head.map { p =>
  //      p.name.toString -> p.asTerm.annotations.map { annot: Annotation =>
  //        annot.tree.tpe.typeSymbol.name.toString -> annot.tree.children.withFilter {
  //          _.productPrefix eq "AssignOrNamedArg"
  //        }.map { tree =>
  //          tree.productElement(0).toString -> tree.productElement(1)
  //        }.toMap
  //      }.toMap
  //    }.toMap
  //  }
  //
  //  def parseMembers(members: MemberScope): Map[String, Map[String, Map[String, Any]]] = {
  //    members.collect { case m: MethodSymbol => m }.withFilter {
  //      _.annotations.length > 0
  //    }.map { m =>
  //      //      getAnnots(m)
  //      //      m.name.toString -> getAnnots(m)
  //      m.name.toString -> m.annotations.map { annot: Annotation =>
  //        annot.tree.tpe.typeSymbol.name.toString -> annot.tree.children.withFilter {
  //          _.productPrefix eq "AssignOrNamedArg"
  //        }.map { tree =>
  //          tree.productElement(0).toString -> tree.productElement(1)
  //        }.toMap
  //      }.toMap
  //    }.toMap
  //  }

  def mapAnnotations(m: MethodSymbol): Map[String, Any] = m.annotations.map(parseAnnotation(_)).toMap

  def parseAnnotation(annot: Annotation): (String, Any) = {
    annot.tree.tpe.typeSymbol.name.toString -> annot.tree.children.withFilter {
      _.productPrefix eq "AssignOrNamedArg"
    }.map { tree =>
      tree.productElement(0).toString -> tree.productElement(1)
    }
  }
}
