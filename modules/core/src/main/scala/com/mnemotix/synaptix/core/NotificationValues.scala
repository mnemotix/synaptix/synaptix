/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.core

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-01-29
  */

object NotificationValues {
  sealed abstract class NotificationValue(val key: String) {
    override def toString = key
  }

  case object STARTUP extends NotificationValue("startup")

  case object SHUTDOWN extends NotificationValue("shutdown")

  case object DONE extends NotificationValue("done")

  case object TERMINATE extends NotificationValue("terminate")

  case object ABORT extends NotificationValue("abort")

  case object UPDATE extends NotificationValue("update")

  case object PROGRESS extends NotificationValue("progress")
}
