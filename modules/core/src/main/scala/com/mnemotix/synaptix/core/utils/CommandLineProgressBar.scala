/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.core.utils

/**
 * Created by Nicolas Delaforge on 13/09/2017.
 */

class CommandLineProgressBar {
  var isSpinning = false

  def percentage(processedItems:Int, totalItems:Int) = {
    if (totalItems <= 0) 100
    else if (processedItems == 0) 0
    else if (processedItems == totalItems) 100
    else {
      ((processedItems.toDouble / totalItems.toDouble) * 100).toInt
    }
  }

  def printProgBar(percent: Int): Unit = printProgBar(percent, "")

  def printProgBar(percent: Int, message: String): Unit = {
    val bar = new StringBuilder("[")
    var i = 0
    while (i < 50) {
      if (i < (percent / 2))        bar.append("=")
      else if (i == (percent / 2))  bar.append(">")
      else                          bar.append(" ")
      {
        i += 1; i - 1
      }
    }
    bar.append("]   " + percent + "%     " + message)
    System.out.print("\r" + bar.toString)
  }
}