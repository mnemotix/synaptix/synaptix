/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.core.utils

import java.io.{FileInputStream, InputStream}
import java.nio.file.Paths

import akka.stream.scaladsl.{Compression, FileIO, Source, StreamConverters}
import akka.stream.{IOResult, Materializer}
import akka.util.ByteString
import org.apache.commons.compress.compressors.CompressorInputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream


import scala.concurrent.{ExecutionContext, Future}

object ZipUtils {
  def unzipBZip2File(fileInputName: String, fileOutputName: String)(implicit mat: Materializer): Future[IOResult] = {
    val uncompressedFileSource: Source[ByteString, Future[IOResult]] =
      StreamConverters.fromInputStream(() => new ZipUtils.BZip2MultiStreamCompressorInputStream(new FileInputStream(fileInputName)))
    uncompressedFileSource.runWith(FileIO.toPath(Paths.get(fileOutputName)))
  }

  def unzipFileCMD(filePath: String)(implicit ex: ExecutionContext): Future[Int] = {
    import scala.sys.process._
     val result = Process(s"bzip2 -dkf $filePath")
      Future(result.run().exitValue())
    }


  def gzipFile(fileInputName: String, fileOutputName: String)(implicit mat: Materializer): Future[IOResult] = {
    val graph = FileIO.fromPath(Paths.get(fileInputName)).via(Compression.gzip).to(FileIO.toPath(Paths.get(fileOutputName)))
    graph.run()
  }

  class BZip2MultiStreamCompressorInputStream(inputStream: InputStream) extends CompressorInputStream {

    private var bZip2CompressorInputStream: BZip2CompressorInputStream = new BZip2CompressorInputStream(inputStream)

    override def read(): Int = {
      var chunkSize: Int = bZip2CompressorInputStream.read()

      if(chunkSize == -1 && inputStream.available()>0){
        bZip2CompressorInputStream = new BZip2CompressorInputStream(inputStream)
        chunkSize = bZip2CompressorInputStream.read()
      }

      chunkSize
    }

    override def close(): Unit = {
      inputStream.close()
      bZip2CompressorInputStream.close()
    }
  }
}


