/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.core.utils

import play.api.libs.json.{JsBoolean, JsError, JsNumber, JsString, JsSuccess, Json, Reads, Writes}

case class MapAny(data:Map[String,Any])
object MapAny {
  private implicit val readsAny: Reads[Any] = Reads {
    case JsString(s) => JsSuccess(s)
    case JsNumber(n) => JsSuccess(n)
    case JsBoolean(b) => JsSuccess(b)
    case jsValue => JsError(s"Unknown type: $jsValue")
  }
  private implicit val writesAny: Writes[Any] = Writes ( a => a match {
    case v:String => Json.toJson(v)
    case v:Int => Json.toJson(v)
    case v:Any => Json.toJson(v.toString)
    // or, if you don't care about the value
    case _ => throw new RuntimeException("unserializeable type")
  })
  implicit val reads: Reads[MapAny] = Json.reads
  implicit val writes: Writes[MapAny] = Json.writes
}
