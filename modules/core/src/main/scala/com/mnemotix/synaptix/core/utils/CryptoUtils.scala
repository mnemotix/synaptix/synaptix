/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.core.utils

import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.security.{MessageDigest, NoSuchAlgorithmException}
import java.util.zip.CRC32

/**
 * Created by Nicolas Delaforge on 13/09/2017.
 */

object CryptoUtils {

  @throws(classOf[NoSuchAlgorithmException])
  @throws(classOf[UnsupportedEncodingException])
  def crc32(components:String*):String= {
    val crc = new CRC32
    crc.update(components.mkString("_").getBytes)
    crc.getValue.toHexString
  }

  @throws(classOf[NoSuchAlgorithmException])
  @throws(classOf[UnsupportedEncodingException])
  def md5sum(components:String*):String= {
    val sb: StringBuilder = new StringBuilder
    for (c <- components) {
      if (c != null && c.length > 0) sb.append(c.trim.toLowerCase)
    }
    val number: BigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(sb.toString.getBytes("UTF-8")))
    return number.toString(16)
  }
}
