/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.nlp

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.core.nlp.PorterStemmer

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 12/10/2020
 */

class PorterStemmerSpec extends SynaptixTestSpec {
  "PorterStemmer" should {
    "widlcard string" in {
      val tokens = "Senator Lindsey Graham, Republican of South Carolina and the chairman of the Judiciary Committee, opened Wednesday’s hearing by proclaiming Judge Amy Coney Barrett’s coming confirmation a historic victory for conservative women who he said have faced steeper obstacles in public life than liberal women. This is the first time in American history that we’ve nominated a woman who is unashamedly pro-life and embraces her faith without apology, and she is going to the court, Mr. Graham said. Judge Barrett, President Trump’s Supreme Court nominee, has declined repeatedly during the hearings to answer how she would rule on a challenge to the Roe v. Wade decision that established abortion rights, but has made clear that she opposes abortion rights.".split(" ")
//      val tokens = "Rien n'était si beau, si leste, si brillant, si bien ordonné que les deux armées. Les trompettes, les fifres, les hautbois, les tambours, les canons, formaient une harmonie telle qu'il n'y en eut jamais en enfer. Les canons renversèrent d'abord à peu près six mille hommes de chaque côté ; ensuite la mousqueterie ôta du meilleur des mondes environ neuf à dix mille coquins qui en infectaient la surface. La baïonnette fut aussi la raison suffisante de la mort de quelques milliers d'hommes. Le tout pouvait bien se monter à une trentaine de mille âmes. Candide, qui tremblait comme un philosophe, se cacha du mieux qu'il put pendant cette boucherie héroïque.".split(" ")
      println(tokens.map(s=> PorterStemmer.wildcard(s)).mkString(" "))
    }
    "stem strings" in {
      val tokens = "Senator Lindsey Graham, Republican of South Carolina and the chairman of the Judiciary Committee, opened Wednesday’s hearing by proclaiming Judge Amy Coney Barrett’s coming confirmation a historic victory for conservative women who he said have faced steeper obstacles in public life than liberal women. This is the first time in American history that we’ve nominated a woman who is unashamedly pro-life and embraces her faith without apology, and she is going to the court, Mr. Graham said. Judge Barrett, President Trump’s Supreme Court nominee, has declined repeatedly during the hearings to answer how she would rule on a challenge to the Roe v. Wade decision that established abortion rights, but has made clear that she opposes abortion rights.".split(" ")
//      val tokens = "Rien n'était si beau, si leste, si brillant, si bien ordonné que les deux armées. Les trompettes, les fifres, les hautbois, les tambours, les canons, formaient une harmonie telle qu'il n'y en eut jamais en enfer. Les canons renversèrent d'abord à peu près six mille hommes de chaque côté ; ensuite la mousqueterie ôta du meilleur des mondes environ neuf à dix mille coquins qui en infectaient la surface. La baïonnette fut aussi la raison suffisante de la mort de quelques milliers d'hommes. Le tout pouvait bien se monter à une trentaine de mille âmes. Candide, qui tremblait comme un philosophe, se cacha du mieux qu'il put pendant cette boucherie héroïque.".split(" ")
      println(tokens.map(s=> PorterStemmer.stem(s)).mkString(" "))
    }
  }
}
