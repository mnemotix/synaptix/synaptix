/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.utils

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.core.utils.StringUtils

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 12/10/2020
 */

class StringUtilsSpec extends SynaptixTestSpec {

  val txt = "Rien n'était si beau, si leste, si brillant, si bien ordonné que les deux armées. Les trompettes, les fifres, les hautbois, les tambours, les canons, formaient une harmonie telle qu'il n'y en eut jamais en enfer. Les canons renversèrent d'abord à peu près six mille hommes de chaque côté ; ensuite la mousqueterie ôta du meilleur des mondes environ neuf à dix mille coquins qui en infectaient la surface. La baïonnette fut aussi la raison suffisante de la mort de quelques milliers d'hommes. Le tout pouvait bien se monter à une trentaine de mille âmes. Candide, qui tremblait comme un philosophe, se cacha du mieux qu'il put pendant cette boucherie héroïque."

  "StringUtils" should {

    "quote strings" in {
      StringUtils.q("Lorem Ipsum") shouldEqual "'Lorem Ipsum'"
    }

    "double quote strings" in {
      StringUtils.qq("Lorem Ipsum") shouldEqual "\"Lorem Ipsum\""
    }
    "tokenize string" in {
//      StringUtils.tokenize(txt).foreach{s => println(s)}
      println(StringUtils.tokenize("fr", txt).toList)
//      StringUtils.tokenize(txt).size shouldEqual 111
    }

    "slugify strings" in {
      StringUtils.slugify("L'albatros règne sur les cieux") shouldEqual "lalbatros-regne-sur-les-cieux"
    }

    "strip accents from strings" in {
      StringUtils.stripAccents(txt) shouldEqual "Rien n'etait si beau, si leste, si brillant, si bien ordonne que les deux armees. Les trompettes, les fifres, les hautbois, les tambours, les canons, formaient une harmonie telle qu'il n'y en eut jamais en enfer. Les canons renverserent d'abord a peu pres six mille hommes de chaque cote ; ensuite la mousqueterie ota du meilleur des mondes environ neuf a dix mille coquins qui en infectaient la surface. La baionnette fut aussi la raison suffisante de la mort de quelques milliers d'hommes. Le tout pouvait bien se monter a une trentaine de mille ames. Candide, qui tremblait comme un philosophe, se cacha du mieux qu'il put pendant cette boucherie heroique."
    }

    "strip punctuation from strings" in {
      StringUtils.stripPunctuation(txt) shouldEqual "Rien n était si beau  si leste  si brillant  si bien ordonné que les deux armées  Les trompettes  les fifres  les hautbois  les tambours  les canons  formaient une harmonie telle qu il n y en eut jamais en enfer  Les canons renversèrent d abord à peu près six mille hommes de chaque côté   ensuite la mousqueterie ôta du meilleur des mondes environ neuf à dix mille coquins qui en infectaient la surface  La baïonnette fut aussi la raison suffisante de la mort de quelques milliers d hommes  Le tout pouvait bien se monter à une trentaine de mille âmes  Candide  qui tremblait comme un philosophe  se cacha du mieux qu il put pendant cette boucherie héroïque "
    }

    "remove non printable charachters from strings" ignore {
      val specialChars = "Pop music is like " + new String(Character.toChars(0x1F4A9))
      new String(StringUtils.removeNonPrintableCharachter(specialChars)) shouldEqual "Pop music is like ?"
    }
  }
}
