/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai

import akka.Done
import com.mnemotix.SynaptixTestSpec
import com.mnemotix.oai.models.oai.{OaiGetRecord, OaiIdentify, OaiListIdentifiers, OaiListRecords, OaiListSets, OaiMetadataFormats}
import com.mnemotix.synaptix.core.ServiceListener

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 02/02/2022
  */

class OaiPmhClientSpec extends SynaptixTestSpec {

  val rootUri1 = "https://sciencepress.mnhn.fr/fr/oai"
  val client1 = new OaiPmhClient("SciencePress OAI Harvester", rootUri1)
  val rootUri2 = "http://www.calames.abes.fr/oai/oai2.aspx"
  val client2 = new OaiPmhClient("Calames OAI Harvester", rootUri2)
  val MNHN_SET_ID = "751059807"

  val rootUri3 = "https://api.archives-ouvertes.fr/oai/hal/"
  val client3 = new OaiPmhClient("HAL OAI PMH Client", rootUri3)

  /*
  Verbe	Rôle	Arguments
  GetRecord	=> identifier, metadataPrefix
  Identify
  ListMetadataFormats	=> identifier
  ListIdentifiers => from, until,  metadataPrefix, set, resumptionToken
  ListRecords => from, until,  metadataPrefix, set, resumptionToken
  ListSets => resumptionToken
  */

  "OaiPmhClient" should {
    "build an Uri" in {
      val waitUri = "https://api.archives-ouvertes.fr/oai/hal?verb=ListRecords&resumptionToken=AoEnMzUyOTgxMQ=="
      val uri = client3.buildUri(verb = "ListRecords", resumptionToken = Some("AoElOTE2MDU="))
      println(uri)
    }
  }

  "OaiPmhClient" should {
    "identitfy a remote OAI PMH endpoint" ignore {
      val result: OaiIdentify = client1.identify()
      println(result)
    }
    "list sets from a remote OAI PMH endpoint" ignore {
      val result: OaiListSets = client1.listSets()
      println(result)
    }
    "list identifiers from a remote OAI PMH endpoint" ignore {
      val result: OaiListIdentifiers = client2.listIdentifiers()
      println(result)
    }
    "list records from a remote OAI PMH endpoint" in {
      val result: OaiListRecords = client1.listRecords()
      println(result)
    }
    "list metadata formats from a remote OAI PMH endpoint" ignore {
      val identifier = "oai:sciencepress.mnhn.fr:14439"
      val result: OaiMetadataFormats = client1.listMetadataFormats(identifier)
      println(result)
    }
    "get record from a remote OAI PMH endpoint" ignore {
      val identifier = "oai:sciencepress.mnhn.fr:14439"
      val result: OaiGetRecord = client1.getRecord(identifier)
      println(result)
    }
    "get all sets from a remote OAI PMH endpoint" ignore {
      client2.getAllSets()
    }
    "get all identifiers from a remote OAI PMH endpoint" ignore {
      client1.getAllIdentifiers()
    }
    "get all records from a remote OAI PMH endpoint" ignore {
      client1.getAllRecords()
    }
    "cache all records from a remote OAI PMH endpoint" in {
      client2.registerListener("console", new ServiceListener {
        override def onStartup(): Unit = {}
        override def onShutdown(): Unit = {}
        override def onUpdate(message: String): Unit = println(message)
      })
      client2.cacheAllRecords(metadataPrefix = "oai_ead", set = Some(MNHN_SET_ID))
      //      client1.cacheAllRecords().futureValue
    }
  }
}
