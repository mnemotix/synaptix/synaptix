/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai.models.oai

import com.mnemotix.oai.api.{OaiMappingException, OaiModel}
import com.mnemotix.oai.api.XMLHelper.ExtendedNodeSeq
import org.joda.time.DateTime

import scala.xml.Node

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 02/02/2022
  */

case class OaiListRecords(request: OaiRequest, records: Seq[OaiRecord], resumptionToken:Option[String]=None) extends OaiModel

object OaiListRecords {
  def apply(xml: Option[Node]): OaiListRecords = {
    xml.map { elem =>
      val responseDate = DateTime.parse((elem \ "responseDate").text)
      val request: String = (elem \ "request").text
      val verb: String = (elem \ "request" \ "@verb").text
      val records: Seq[OaiRecord] = (elem \ "ListRecords" \\ "record").map { n: Node =>
        OaiRecord(Some(n))
      }
      val rToken:Option[String] = (elem \ "ListRecords" \ "resumptionToken").textOption
//      <resumptionToken completeListSize="18202" cursor="0" expirationDate="2022-03-23T16:27:21Z">4d0cc8f6eb11ee8e54422c53323c4fee</resumptionToken>

      OaiListRecords(
        OaiRequest(responseDate, request, verb),
        records,
        rToken
      )
    }.getOrElse(throw new OaiMappingException("The given XML argument was empty."))
  }
}