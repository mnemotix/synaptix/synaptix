/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai.api

import com.mnemotix.synaptix.cache.RocksDBStore

import scala.concurrent.ExecutionContext

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 01/03/2022
  */

class OaiRecordCache(val filepath:String) {
  var db:Option[RocksDBStore] = None

  def init() = {
    db = Some(new RocksDBStore(filepath))
    db.get.init
  }

  def shutdown() = {
    db.get.shutdown()
  }

  def put(key:String, value:Array[Byte]) = {
    db.get.put(key, value)
  }

  def bulkInsert(entries: Seq[(String, Array[Byte])])(implicit ec:ExecutionContext) = {
    db.get.bulkInsert(entries)
  }

  def get(key:String)(implicit ec:ExecutionContext) = {
    db.get.get(key)
  }

  def getKeys()(implicit ec:ExecutionContext) = {
    db.get.iterator().keys()
  }

  def getValues()(implicit ec:ExecutionContext) = {
    db.get.iterator().values()
  }
}
