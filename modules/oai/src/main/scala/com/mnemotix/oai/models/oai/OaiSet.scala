package com.mnemotix.oai.models.oai

import com.mnemotix.oai.api.{OaiMappingException, OaiModel}

import scala.xml.Node

case class OaiSet(setSpec:String, setName:String) extends OaiModel

object OaiSet {
  def apply(xml: Option[Node]): OaiSet = {
    xml.map { elem =>
      OaiSet(
        (elem \ "setSpec").text,
        (elem \ "setName").text
      )
    }.getOrElse(throw new OaiMappingException("The given XML argument was empty."))
  }
}