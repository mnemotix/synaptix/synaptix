/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai.api

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 02/02/2022
  */

abstract class OaiPmhException(message: String, cause: Option[Throwable]) extends Exception(message, cause.getOrElse(null))

class OaiPmhProcessResponseException(message: String, cause: Option[Throwable]=None) extends OaiPmhException(message, cause)

class OaiMappingException(message: String, cause: Option[Throwable]=None) extends OaiPmhException(message, cause)