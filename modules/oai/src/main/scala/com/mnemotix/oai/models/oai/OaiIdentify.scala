/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai.models.oai

import com.mnemotix.oai.api.{OaiMappingException, OaiModel}
import org.joda.time.DateTime

import scala.xml.Node

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 02/02/2022
  */

case class OaiIdentify(
                        request: OaiRequest,
                        repositoryName: String,
                        baseURL: String,
                        protocolVersion: String,
                        adminEmail: String,
                        earliestDatestamp: String,
                        deletedRecord: String,
                        granularity: String,
                        compression: String
                      ) extends OaiModel

object OaiIdentify {
  def apply(xml: Option[Node]): OaiIdentify = {
    xml.map { elem =>
      OaiIdentify(
        OaiRequest(Some(elem)),
        (elem \ "Identify" \ "repositoryName").text,
        (elem \ "Identify" \ "baseURL").text,
        (elem \ "Identify" \ "protocolVersion").text,
        (elem \ "Identify" \ "adminEmail").text,
        (elem \ "Identify" \ "earliestDatestamp").text,
        (elem \ "Identify" \ "deletedRecord").text,
        (elem \ "Identify" \ "granularity").text,
        (elem \ "Identify" \ "compression").text
      )
    }.getOrElse(throw new OaiMappingException("The given XML argument was empty."))
  }
}
