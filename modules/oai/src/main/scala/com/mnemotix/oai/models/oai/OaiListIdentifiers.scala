/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai.models.oai

import com.mnemotix.oai.api.{OaiMappingException, OaiModel}
import com.mnemotix.oai.api.XMLHelper.ExtendedNodeSeq

import scala.xml.Node

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 02/02/2022
  */

case class OaiListIdentifiers(request: OaiRequest, identifiers: Seq[OaiIdentifier], resumptionToken:Option[String]=None) extends OaiModel

object OaiListIdentifiers {
  def apply(xml: Option[Node]): OaiListIdentifiers = {
    xml.map { elem =>
      val identifiers: Seq[OaiIdentifier] = (elem \ "ListIdentifiers" \\ "header").map { n: Node => OaiIdentifier(Some(n)) }
      val rToken:Option[String] = (elem \ "ListIdentifiers" \ "resumptionToken").textOption
      OaiListIdentifiers(OaiRequest(Some(elem)), identifiers, rToken)
    }.getOrElse(throw new OaiMappingException("The given XML argument was empty."))
  }
}