package com.mnemotix.oai.models.oai

import com.mnemotix.oai.api.{OaiMappingException, OaiModel}

import scala.xml.Node

case class OaiIdentifier(identifier:String, datestamp:String, setSpec:String) extends OaiModel

object OaiIdentifier {
  def apply(xml: Option[Node]): OaiIdentifier = {
    xml.map { elem =>
      OaiIdentifier(
        (elem \ "identifier").text,
        (elem \ "datestamp").text,
        (elem \ "setSpec").text
      )
    }.getOrElse(throw new OaiMappingException("The given XML argument was empty."))
  }

}