package com.mnemotix.oai.api

import scala.xml.NodeSeq

object XMLHelper {
  implicit class ExtendedNodeSeq(nodeSeq: NodeSeq) {
    def textOption: Option[String] = {
      val text = nodeSeq.text
      if (text == null || text.length == 0) None else Some(text)
    }
    def textOrElse(elze : String) : String = textOption.getOrElse(elze)
  }
}
