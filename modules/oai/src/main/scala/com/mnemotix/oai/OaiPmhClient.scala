/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.oai

import com.mnemotix.oai.models.oai._
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.core.{MonitoredService, NotificationValues}
import com.mnemotix.synaptix.core.utils.StringUtils
import com.softwaremill.sttp._
import org.joda.time.DateTime

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import scala.xml.Elem
import scala.collection.compat._


/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 02/02/2022
  */

class OaiPmhClient(override val serviceName: String, rootUri: String, cachePath: String = "./data/cache/oai.db") extends MonitoredService {

  implicit val backend = HttpURLConnectionBackend()

  lazy val accessPointUri: String = StringUtils.removeTrailingSlashes(rootUri)
  lazy val cache: RocksDBStore = new RocksDBStore(cachePath)

  def init() = cache.init()

  def shutdown() = cache.shutdown()

  def getRecord(identifier: String, metadataPrefix: String = "oai_dc") = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "GetRecord", metadataPrefix = Some(metadataPrefix), identifier = Some(identifier)))
    OaiGetRecord(processResponse(request.send()))
  }

  def identify(): OaiIdentify = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "Identify"))
    OaiIdentify(processResponse(request.send()))
  }

  def listMetadataFormats(identifier: String) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListMetadataFormats", identifier = Some(identifier)))
    OaiMetadataFormats(processResponse(request.send()))
  }

  /*
    Resumable
  */

  def listSets(): OaiListSets = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListSets"))
    OaiListSets(processResponse(request.send()))
  }

  def listIdentifiers(metadataPrefix: String = "oai_dc", from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListIdentifiers", metadataPrefix = Some(metadataPrefix), from = from, until = until, set = set))
    OaiListIdentifiers(processResponse(request.send()))
  }

  def listRecords(metadataPrefix: String = "oai_dc", from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListRecords", metadataPrefix = Some(metadataPrefix), from = from, until = until, set = set))
    OaiListRecords(processResponse(request.send()))
  }

  def cacheRecords(overwrite: Boolean = false, metadataPrefix: String = "oai_dc", from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None)(implicit ec: ExecutionContext) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListRecords", metadataPrefix = Some(metadataPrefix), from = from, until = until, set = set))
    val rec = OaiListRecords(processResponse(request.send()))
    if (!cache.isOpen) cache.init()
    val f = if (overwrite) cache.bulkInsert(rec.records.map { r => (r.header.identifier, r.metadata.toString.getBytes("UTF-8")) })
      else cache.bulkAppend(rec.records.map { r => (r.header.identifier, r.metadata.toString.getBytes("UTF-8")) })
    (rec, f)
  }

  def buildUri(verb: String, metadataPrefix: Option[String] = None, identifier: Option[String] = None, from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None, resumptionToken: Option[String] = None): Uri = {
    val sb = new StringBuilder()
    sb.append(s"verb=$verb")
    if (metadataPrefix.isDefined) sb.append(s"&metadataPrefix=${metadataPrefix.get}")
    if (identifier.isDefined) sb.append(s"&identifier=${identifier.get}")
    if (from.isDefined) sb.append(s"&from=${from.get.toString("YYYY-MM-dd")}")
    if (until.isDefined) sb.append(s"&until=${until.get.toString("YYYY-MM-dd")}")
    if (set.isDefined) sb.append(s"&set=${set.get}")
    if (resumptionToken.isDefined) sb.append(s"&resumptionToken=${resumptionToken.get.replace("=", "%3D")}")
    val uri = s"$accessPointUri?${sb.toString()}"
    uri"$uri"
  }

  def getAllSets() = {
    var sets = listSets()
    var rToken = sets.resumptionToken
    while (rToken.isDefined) {
      sets = nextSets(rToken.get)
      rToken = sets.resumptionToken
    }
  }

  def getAllIdentifiers(metadataPrefix: String = "oai_dc", from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None) = {
    var identifiers = listIdentifiers(metadataPrefix, from, until, set)
    var rToken = identifiers.resumptionToken
    while (rToken.isDefined) {
      identifiers = nextIdentifiers(rToken.get)
      rToken = identifiers.resumptionToken
    }
  }

  def getAllRecords(metadataPrefix: String = "oai_dc", from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None) = {
    var records = listRecords(metadataPrefix, from, until, set)
    var rToken = records.resumptionToken
    while (rToken.isDefined) {
      records = nextRecords(rToken.get)
      rToken = records.resumptionToken
    }
  }

  def cacheAllRecords(overwrite: Boolean = false, metadataPrefix: String = "oai_dc", from: Option[DateTime] = None, until: Option[DateTime] = None, set: Option[String] = None)(implicit ec: ExecutionContext) = {
    var processedItems = 0
    val futures = collection.mutable.ArrayBuffer[Future[Boolean]]()
    if (!cache.isOpen) cache.init()
    var rToken = Option("init")
    notify(NotificationValues.STARTUP)
    while (rToken.isDefined) {
      val recs = if (rToken.get.equals("init")) listRecords(metadataPrefix, from, until, set) else nextRecords(rToken.get)
      processedItems = processedItems + recs.records.size
      if(overwrite)
      futures.addOne(cache.bulkInsert(recs.records.map { r =>
        notifyProgress(processedItems, None, None)
        (r.header.identifier, r.metadata.toString.getBytes("UTF-8"))
      }))
      else futures.addOne(cache.bulkAppend(recs.records.map { r =>
        notifyProgress(processedItems, None, None)
        (r.header.identifier, r.metadata.toString.getBytes("UTF-8"))
      }))

      rToken = recs.resumptionToken
      if(rToken.isDefined) notify(NotificationValues.UPDATE, Some(s"Processing ${rToken.get}"))
    }
    val f = Future.sequence(futures)
    f.onComplete {
      case Success(_) => {
        processedItems = 0
        notify(NotificationValues.DONE)
      }
      case Failure(exception) => {
        processedItems = 0
        notifyError(exception.getMessage, Some(exception))
      }
    }
    f
  }

  /*
    Pagination
  */
  def nextSets(resumptionToken: String) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListSets", resumptionToken = Some(resumptionToken)))
    OaiListSets(processResponse(request.send()))
  }

  def nextIdentifiers(resumptionToken: String) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListIdentifiers", resumptionToken = Some(resumptionToken)))
    OaiListIdentifiers(processResponse(request.send()))
  }

  def nextRecords(resumptionToken: String) = {
    val request = sttp.headers(Map("Accept" -> "text/xml")).get(buildUri(verb = "ListRecords", resumptionToken = Some(resumptionToken)))
    OaiListRecords(processResponse(request.send()))
  }

  def processResponse(response: Id[Response[String]]): Option[Elem] = {
    val result: Either[String, String] = response.body
    result match {
      case Right(content) =>
        val elem = scala.xml.XML.loadString(content)
        //        val p = new scala.xml.PrettyPrinter(80, 4)
        //        println(p.format(elem))
        Some(elem)
      case Left(error) =>
        logger.error(s"${response.statusText}(${response.code}): $error")
        None
    }
  }
}
