/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.synaptix.cache

import org.joda.time.DateTime

import java.io.File
import java.nio.charset.StandardCharsets
import scala.collection.mutable.ArrayBuffer

class RocksDBStoreSpec extends CacheSpec {

  "RocksDBStore" should {

    lazy val UTF8:String = StandardCharsets.UTF_8.name

    val tmpFile:File = File.createTempFile(s"rocksdb-${DateTime.now.getMillis}", ".db")
    tmpFile.delete
    val store  = new RocksDBStore(tmpFile.getAbsolutePath)
    store.init

    val bulkSize = 1000

    def key(id:Int):String = s"oai:sciencepress.mnhn.fr:$id"
    def content(id:Int):Array[Byte] =
      s"""
         |<oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/  http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
         | <dc:description>Une analyse morphologique comparative de &lt;i&gt;Lotus roudairei&lt;/i&gt; nord-ouest-africain, &lt;i&gt;Lotus&lt;/i&gt; sect. &lt;i&gt;Simpeteria&lt;/i&gt; nord-américain et &lt;i&gt;Lotus&lt;/i&gt; sect. &lt;i&gt;Microlotus&lt;/i&gt; (= gen. &lt;i&gt;Acmispon&lt;/i&gt; s. str.) américain a été réalisée. En partant des données obtenues, il apparaît que ces 3 taxons ne peuvent pas former un genre (ou sous-genre) particulier, &lt;i&gt;Acmispon&lt;/i&gt; sensu P. Lassen (1986). Une section nouvelle &lt;i&gt;Pseudosimpeteria&lt;/i&gt;, avec une seule espèce &lt;i&gt;L. roudairei&lt;/i&gt;, est décrite dans le genre &lt;i&gt;Lotus&lt;/i&gt; subgen. &lt;i&gt;Lotus&lt;/i&gt; , répandu dans l&#039;Ancien Monde. Les relations entre les Loteae de l&#039;Ancien Monde et ceux du Nouveau Monde sont brièvement discutées.</dc:description>
         | <dc:description>A comparative analysis of morphological features in NW African &lt;i&gt;Lotus roudairei&lt;/i&gt;, N American &lt;i&gt;Lotus&lt;/i&gt; sect. &lt;i&gt;Simpeteria&lt;/i&gt;, and American &lt;i&gt;Lotus&lt;/i&gt; sect. &lt;i&gt;Microlotus&lt;/i&gt; (= gen. &lt;i&gt;Acmispon&lt;/i&gt; s. str.) was carried out. According to the data obtained, these three taxa seemed not to form the distinct genus or subgenus &lt;i&gt;Acmispon&lt;/i&gt; sensu P. Lassen (1986). A new section &lt;i&gt;Pseudosimpeteria&lt;/i&gt; with a single species, &lt;i&gt;L. roudairei&lt;/i&gt;, is described within the Old World &lt;i&gt;Lotus&lt;/i&gt; subgen. &lt;i&gt;Lotus&lt;/i&gt;. The relationships between Old World and New World Loteae are briefly discussed.</dc:description>
         | <dc:date>1997-12-18 01:00:00</dc:date>
         | <dc:identifier>$id</dc:identifier>
         | <dc:type>Article de fascicule</dc:type>
         | <dc:creator />
         | <dc:language>EN</dc:language>
         | <dc:coverage>&lt;img src=&quot;https://sciencepress.mnhn.fr/sites/default/files/default_images/img-default_0.jpg&quot; alt=&quot;&quot; /&gt;</dc:coverage>
         |</oai_dc:dc>
         |""".stripMargin.getBytes(UTF8)

    def entries(nbEntries:Int):Seq[(String, Array[Byte])] = (1 to nbEntries).map{i =>
      (key(i), content(i))
    }

    "insert a single value in cache" in {
      assert(store.put(key(0), content(0)))
      val it = store.iterator()
      it.size shouldEqual 1
      it.close()
    }

    "get a value from key in cache" in {
      val value = store.get(key(0))
      assert(value.isDefined)
      value.get shouldEqual content(0)
    }

    "append a single value in cache" in {
      assert(store.append(key(0), content(0)))
      var it = store.iterator()
      it.size shouldEqual 1
      it.close()
      it = store.iterator()
      val v = it.next()
      println(new String(v.value, "UTF-8"))
      it.close()
    }

    "delete a value in cache" in {
      assert(store.delete(key(0)))
      val it = store.iterator()
      it.size shouldEqual 0
      it.close()
    }

    "put a lot of entries in cache" in {
      entries(bulkSize).foreach{case(k,v) => store.put(k, v)}
      val it = store.iterator()
      it.size shouldEqual bulkSize
      it.close()
    }

    "bulkInsert in cache" in {
      assert(store.bulkInsert(entries(bulkSize)).futureValue)
      val it = store.iterator()
      it.size shouldEqual bulkSize
      it.close()
    }

    "bulkAppend in cache" in {
      assert(store.bulkAppend(entries(bulkSize)).futureValue)
      val it = store.iterator()
      it.size shouldEqual bulkSize
      it.close()
    }

    "iterate over all keys in cache" in {
      val it = store.iterator()
      val keys = it.keys()
      val buffer = new ArrayBuffer[String]()
      while(keys.hasNext){
        val k = keys.next()
        buffer.addOne(k)
      }
      buffer.size shouldEqual bulkSize
      buffer(1) shouldEqual key(10)
      it.close()
    }

    "get multiple values from keys in cache" in {
      val nbKeys = 1000
      val keys:Seq[String] = (1 to nbKeys).map(key)
      val values:Seq[Array[Byte]] = store.multiGet(keys)
      values.size shouldEqual nbKeys
    }

    "iterate over all values in cache" in {
      val it = store.iterator()
      val values = it.values()
      val buffer = new ArrayBuffer[Array[Byte]]()
      while(values.hasNext){
        val v = values.next()
        buffer.addOne(v)
      }
      buffer.size shouldEqual bulkSize
      it.close()
    }

    "clear the DB" in {
      assert(store.clear())
      val it = store.iterator()
      it.size shouldEqual 0
      it.close()
    }

    "close the database" in {
      store.shutdown()
    }

    "drop the database" in {
      assert(store.drop())
    }
  }

//
//  it should "open a store" in {
//    val aURI:(String,String) = ("WritenRepSingle", "www.URISingle.com")
//    val path = "modules/cache/src/test/resources/test.db"
//    val store = new RocksDBStore(path)
//    store.init
//    val fut = store.put(aURI._1, ByteString(aURI._2.getBytes(UTF8)))
//    store.shutdown()
//  }
//
//  it should "throw an exception when we close a store and try to access it" in {
//    val path = "modules/cache/src/test/resources/test.db"
//    val store = new RocksDBStore(path)
//    store.init
//    store.shutdown()
//
//    val fut = intercept[Exception] {
//      store.get("WritenRepSingle")
//    }
//    assert(fut.getMessage === "An error occured during the get process")
//  }
//
//  it should "throw an exception when we open two time the store on different object" in {
//    val path = "modules/cache/src/test/resources/test.db"
//    val store = new RocksDBStore(path)
//    store.init
//    store.shutdown()
//
//    /*
//        val fut = intercept[Exception] {
//          new RocksDBStore(path)
//        }
//        assert(fut.getMessage === "An error occured during the init process")
//
//     */
//  }
//
//  it should "open the store on different object" in {
//    val path = "modules/cache/src/test/resources/test.db"
//    val store = new RocksDBStore(path)
//    store.init
//    store.shutdown()
//
//    val store2 = new RocksDBStore(path)
//    store2.init
//    store2.shutdown()
//  }
//
//  it should "delete database" in {
//    val path = "modules/cache/src/test/resources/test.db"
//    val store = new RocksDBStore(path)
//    store.init
//    store.shutdown()
//    store.drop()
//  }
}