package com.mnemotix.synaptix.cache

import com.typesafe.config.{Config, ConfigFactory}

object CacheConfig {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("cache")).getOrElse(ConfigFactory.empty())
  lazy val createIfMissing: Boolean = conf.getBoolean("createIfMissing")
  lazy val writeBufferSize: Int = conf.getInt("writeBufferSize")
  lazy val maxWriteBufferNumber: Int = conf.getInt("maxWriteBufferNumber")
}
