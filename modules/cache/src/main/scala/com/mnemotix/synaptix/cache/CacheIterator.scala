package com.mnemotix.synaptix.cache

import org.rocksdb.RocksIterator

import java.nio.charset.StandardCharsets

case class CacheEntry(key:String, value:Array[Byte])

class CacheIterator(it: RocksIterator) extends Iterator[CacheEntry] {

  it.seekToFirst()

  override def hasNext: Boolean = it.isValid

  override def next(): CacheEntry = {
    val entry:CacheEntry = CacheEntry(new String(it.key(), StandardCharsets.UTF_8), it.value())
    it.next()
    entry
  }

  def nextKey(): String = {
    val k:String = new String(it.key(), StandardCharsets.UTF_8)
    it.next()
    k
  }

  def nextValue(): Array[Byte] = {
    val v:Array[Byte] = it.value()
    it.next()
    v
  }

  def keys(): Iterator[String] = this.map(v => v.key)
  def values(): Iterator[Array[Byte]] = this.map(v => v.value)

  def close() = it.close()
}
