/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.cache

import com.mnemotix.synaptix.core.GenericService
import com.typesafe.scalalogging.LazyLogging
import org.rocksdb._
import org.rocksdb.util.SizeUnit

import java.io.File
import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._
import scala.reflect.io.Directory
import scala.collection.compat._


class RocksDBStore(path: String) extends LazyLogging with GenericService {

  val RECORD_SEPERATOR = "\n@@@@\n"

  private var store: Option[RocksDB] = None

  @throws(classOf[CacheException])
  def init() = {
    try {
      RocksDB.loadLibrary()
      val options: Options = new Options()
        .setCreateIfMissing(CacheConfig.createIfMissing)
        .setWriteBufferSize(CacheConfig.writeBufferSize * SizeUnit.MB)
        .setMaxWriteBufferNumber(CacheConfig.maxWriteBufferNumber)
        .setCompressionType(CompressionType.SNAPPY_COMPRESSION)
        .setCompactionStyle(CompactionStyle.UNIVERSAL)
      store = Some(RocksDB.open(options, path))
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the initialization process", t)
        throw CacheInitException("An error occured during the initialization process", Some(t))
    }
  }

  private def getCache() = store.getOrElse(throw CacheInitException("Cache was not initialized"))

  def isOpen: Boolean = store.isDefined

  @throws(classOf[CacheException])
  def get(key: String)(implicit ec: ExecutionContext): Option[Array[Byte]] = {
    try {
      Option(getCache().get(key.getBytes(StandardCharsets.UTF_8)))
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the get process", t)
        throw CacheSearchException("An error occured during the get process", Some(t))
    }
  }

  @throws(classOf[CacheException])
  def multiGet(keys: Seq[String]): Seq[Array[Byte]] = {
    try {
      getCache().multiGetAsList(keys.map(_.getBytes(StandardCharsets.UTF_8)).asJava).asScala.toSeq
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the get process", t)
        throw CacheSearchException("An error occured during the get process", Some(t))
    }
  }

  def iterator(): CacheIterator = new CacheIterator(getCache().newIterator())

  @throws(classOf[CacheException])
  def put(key: String, data: Array[Byte]): Boolean = {
    try {
      getCache().put(key.getBytes(StandardCharsets.UTF_8), data)
      true
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the `put` process", t)
        throw CacheSearchException("An error occured during the `put` process", Some(t))
    }
  }

  @throws(classOf[CacheException])
  def append(key: String, data: Array[Byte]): Boolean = {
    try {
      val k = key.getBytes(StandardCharsets.UTF_8)
      if (getCache().keyMayExist(k, null)) {
        if (getCache().get(k) ==  null) {
          getCache().put(k, data)
        }
        else {
          val oldv = new String(getCache().get(k), "UTF-8")
          val newv = oldv + RECORD_SEPERATOR + new String(data, "UTF-8")
          getCache().put(k, newv.getBytes("UTF-8"))
        }
      }
      else getCache().put(k, data)
      true
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the `put` process", t)
        throw CacheSearchException("An error occured during the `put` process", Some(t))
    }
  }

  @throws(classOf[CacheException])
  def bulkInsert(entries: Seq[(String, Array[Byte])])(implicit ec: ExecutionContext): Future[Boolean] = Future {
    try {
      val batch = new WriteBatch()
      entries.foreach(ll => {
        val (k, v) = ll
        batch.put(k.getBytes(StandardCharsets.UTF_8), v)
      })
      getCache().write(new WriteOptions(), batch)
      true
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the `put` process", t)
        throw CacheSearchException("An error occured during the `put` process", Some(t))
    }
  }

  @throws(classOf[CacheException])
  def bulkAppend(entries: Seq[(String, Array[Byte])])(implicit ec: ExecutionContext): Future[Boolean] = Future {
    try {
      val batch = new WriteBatch()
      entries.foreach { ll =>
        val (key, data) = ll
        val k = key.getBytes(StandardCharsets.UTF_8)
        if (getCache().keyMayExist(k, null)) {
            if (getCache().get(k) ==  null) {
              batch.put(k, data)
            }
            else  {
              val oldv = new String(getCache().get(k), "UTF-8")
              val newv = oldv + RECORD_SEPERATOR + new String(data, "UTF-8")
              batch.put(k, newv.getBytes("UTF-8"))
            }
        } else batch.put(k, data)
      }
      getCache().write(new WriteOptions(), batch)
      true
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the `put` process", t)
        throw CacheSearchException("An error occured during the `put` process", Some(t))
    }
  }

  @throws(classOf[CacheException])
  def delete(key: String): Boolean = {
    try {
      getCache().delete(key.getBytes(StandardCharsets.UTF_8))
      true
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the `delete` process", t)
        throw CacheUpdateException("An error occured during the `delete` process", Some(t))
    }
  }

  def clear() = {
    iterator().keys().foreach(delete(_))
    true
  }

  /**
    * Flush all memory table data.
    *
    * @return
    */
  def flush() = {
    getCache().flush(new FlushOptions().setWaitForFlush(true))
    true
  }

  def compact(): Boolean = {
    getCache().compactRange()
    true
  }

  def drop(): Boolean = {
    val files = new Directory(new File(path))
    files.deleteRecursively()
  }

  def shutdown() = {
    getCache().cancelAllBackgroundWork(false)
    getCache().close()
    store = None
  }
}