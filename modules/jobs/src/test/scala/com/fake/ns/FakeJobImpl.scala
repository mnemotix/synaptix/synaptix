/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.fake.ns

import akka.Done
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.synaptix.jobs.api.SynaptixJob
import com.mnemotix.synaptix.jobs.listeners.DefaultListener

import scala.concurrent.Future

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 18/08/2021
  */

class FakeJobImpl extends SynaptixJob {

  override val prefix: String = "fake"
  override val label: String = "Fake Job Implementation for testing purpose"

  registerListener("default", new DefaultListener(this))

  override def process(): Future[Done] = {
    args.foreach(c => println(s"${c._1}  -> ${c._2.label}"))


    var processed = 0
    val total = 10000
    thread = Some(Source(1 to total)
      .via(killSwitch.flow)
      .via(Flow[Int].map{ i:Int =>
        println(i, args.mkString(";"))
      })
//      .grouped(1000)
//      .via(Flow[Seq[Int]].map{ s:Seq[Int] =>
//        monitor.onStatus()
//      })
      .runWith(Sink.ignore))

    thread.get
  }

}
