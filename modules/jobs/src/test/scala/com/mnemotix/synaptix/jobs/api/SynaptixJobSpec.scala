/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs.api

import com.fake.ns.FakeJobImpl
import com.mnemotix.synaptix.jobs.JobsTestSpec

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 12/07/2021
  */

class SynaptixJobSpec extends JobsTestSpec {
  "SynaptixJob" should {
    "run on demand with provided arguments" in {
      val job = new FakeJobImpl
      job.execute().futureValue
    }
    "abort on demand" in {
      val job = new FakeJobImpl
      job.execute()
      logger.error("Job aborted", job.abort().futureValue)
    }
    "shutdown on demand" in {
      val job = new FakeJobImpl
      job.execute()
      job.close().futureValue
    }
    "terminate on demand" in {
      val job = new FakeJobImpl
      job.execute()
      job.terminate().futureValue
    }
  }
}
