/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import com.fake.ns.FakeJobImpl

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 12/07/2021
  */

class JobsRegistrySpec extends JobsTestSpec {
  "class JobsRegister" should {
    "create a job id from a prefix" in {

      val inst1 = new FakeJobImpl
      val inst2 = new FakeJobImpl
      val inst3 = new FakeJobImpl
      JobsRegistry.registerJob(inst1)
      JobsRegistry.registerJob(inst2)
      JobsRegistry.registerJob(inst3)
      val jid4 = JobsRegistry.getJobIdFromPrefix("fake")
      jid4 shouldEqual "fake-4"
    }
  }
}
