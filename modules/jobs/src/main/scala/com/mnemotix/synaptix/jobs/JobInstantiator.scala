/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import com.mnemotix.synaptix.core.ReflectHelper
import com.mnemotix.synaptix.jobs.api.SynaptixJob
import com.mnemotix.synaptix.jobs.configuration.{JobArgument, JobConfig, JobsConfiguration}
import com.mnemotix.synaptix.jobs.exceptions.{JobInstanciationException, JobNotFound}

import scala.util.Try

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 18/08/2021
  */

object JobInstantiator {

  def createInstance(qname: String, args: Map[String, JobArgument]=Map.empty): SynaptixJob = {
    val instanciationTry = Try(ReflectHelper.instanceFromName(qname, args).asInstanceOf[SynaptixJob])
    if (instanciationTry.isSuccess) {
      val inst = instanciationTry.get
      inst.args = args
      JobsRegistry.registerJob(inst.id, inst)
      inst
    }
    else throw JobInstanciationException("Unable to instanciate the job", instanciationTry.failed.toOption)
  }

  def createInstanceById(jobId: String): SynaptixJob = {
    val jconfig:JobConfig = JobsConfiguration.monitored.getOrElse(jobId, throw JobNotFound("Job's ID was not found"))
    createInstance(jconfig.qname, jconfig.args)
  }

  def createInstanceFromConf(jconfig: JobConfig): SynaptixJob = {
    createInstance(jconfig.qname, jconfig.args)
  }
}
