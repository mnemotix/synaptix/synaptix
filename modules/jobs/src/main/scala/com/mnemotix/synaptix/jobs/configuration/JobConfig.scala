/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs.configuration

import com.typesafe.config.{Config, ConfigRenderOptions}
import play.api.libs.json.{JsValue, Json}

import scala.collection.JavaConverters._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 18/08/2021
  */

case class JobArgument(label: String, value: JsValue)

case class JobConfig(id: String, qname: String, args: Map[String, JobArgument])

object JobArgument {

  implicit lazy val format = Json.format[JobArgument]

  def apply(conf: Config): JobArgument = {
    val label = conf.getString("label").trim
    val value = Json.parse(conf.getValue("value").render(ConfigRenderOptions.concise()))
    JobArgument(label, value)
  }
}

object JobConfig {

  implicit lazy val format = Json.format[JobConfig]

  def apply(conf: Config): JobConfig = {
    val id = conf.getString("id").trim
    val qname = conf.getString("qname").trim
    val args: Map[String, JobArgument] = conf.getConfigList("args").asScala.map(c => c.getString("label").trim -> JobArgument(c)).toMap
    JobConfig(id, qname, args)
  }
}