package com.mnemotix.synaptix.jobs.listeners

import com.mnemotix.synaptix.core.ServiceListener
import com.mnemotix.synaptix.core.utils.CommandLineProgressBar
import com.mnemotix.synaptix.jobs.api.SynaptixJob

class  DefaultListener(val job: SynaptixJob) extends ServiceListener {

  lazy val pbar = new CommandLineProgressBar

  override def onStartup(): Unit = logger.info(s"${job.serviceName} has started...")

  override def onTerminate(): Unit = logger.warn(s"Termination signal was sent to ${job.serviceName}")

  override def onShutdown(): Unit = logger.warn(s"Shutdown signal was sent to ${job.serviceName}")

  override def onAbort(): Unit = logger.warn(s"Abortion signal was sent to ${job.jobSystem}")

  override def onUpdate(message: String): Unit = {
    print("\r" + message)
  }

  override def onProgress(processedItems: Int, totalItems: Option[Int], message: Option[String]): Unit = {
    if(message.isDefined) pbar.printProgBar(pbar.percentage(processedItems, totalItems.get), message.get)
    else pbar.printProgBar(pbar.percentage(processedItems, totalItems.get))
  }

  override def onDone(): Unit = logger.info(s"${job.serviceName} has been completed.")

  override def onError(errorMessage: String, cause: Option[Throwable]): Unit = logger.error(s"${job.serviceName} has encountered an error.", cause.getOrElse(null))
}
