/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import com.mnemotix.synaptix.jobs.api.SynaptixJob
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 18/08/2021
  */

object JobsRegistry extends LazyLogging {

  private val jobs = collection.mutable.Map[String, Option[SynaptixJob]]()

  /**
    * Registers a Job using its prefix to create a unique Job Id
    * @param job
    * @return the job
    */
  def registerJob(job: SynaptixJob) = {
    val jobId = getJobIdFromPrefix(job.prefix)
    logger.info(s"New job registered with id '$jobId'")
    jobs.put(jobId, Some(job)).getOrElse(None)
  }

  /**
    * Registers a Job using a given Job Id
    * @param jobId
    * @param job
    * @return
    */
  def registerJob(jobId: String, job: SynaptixJob) = {
    logger.info(s"New job registered with id '$jobId'")
    jobs.put(jobId, Some(job)).getOrElse(None)
  }

  /**
    * Unregisters a job by its job id
    * @param jobId
    * @return a job option if job was found
    */
  def unregisterJob(jobId: String) = {
    logger.info(s"Job registered with id '$jobId' was unregistered")
    jobs.remove(jobId).getOrElse(None)
  }

  /**
    * Returns a job by its job id
    * @param jobId
    * @return a job option if job was found
    */
  def get(jobId: String) = {
    jobs.get(jobId).getOrElse(None)
  }

  /**
    * Returns a list of jobs matching a given prefix
    * @param prefix
    * @return a filtered Map of (id -> job)
    */
  def getByPrefix(prefix: String) = {
    jobs.filter{case(id, _) => id.startsWith(prefix)}.toMap
  }

  /**
    * Returns an unused job id from a prefix.
    * It counts the number of jobs having the same prefix to append a number, making the job id unique.
    * @param jobPrefix
    * @return a unique jobid (string)
    */
  def getJobIdFromPrefix(jobPrefix: String) : String = {
    val count:Int = jobs.keys.count(id => id.startsWith(jobPrefix)) + 1
    val jobId = s"$jobPrefix-${count.toString}"
    logger.debug(s"Job ID created : $jobId")
    jobs.put(jobId, None) // creates an empty entry waiting for the job to be registered.
    jobId
  }
}
