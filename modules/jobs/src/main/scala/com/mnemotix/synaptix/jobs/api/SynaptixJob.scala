/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs.api

import akka.Done
import akka.actor.ActorSystem
import akka.stream.KillSwitches
import com.mnemotix.synaptix.core.{MonitoredService, NotificationValues}
import com.mnemotix.synaptix.jobs.JobsRegistry
import com.mnemotix.synaptix.jobs.configuration.JobArgument
import com.mnemotix.synaptix.jobs.exceptions.JobAbortedException

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 06/07/2021
  */

trait SynaptixJob extends MonitoredService {

  lazy val id: String = JobsRegistry.getJobIdFromPrefix(prefix) // the job unique id, used by the registry
  val prefix: String // the prefix used to create unique ids inside the registry
  val label: String // the label of the job (used by the monitor)
  override val serviceName:String = s"Job[${id}]/'${label}'"

  var args: Map[String, JobArgument] = Map.empty

  implicit lazy val jobSystem: ActorSystem = ActorSystem(id)
  implicit lazy val jobExecutionContext: ExecutionContext = jobSystem.dispatcher
  lazy val killSwitch = KillSwitches.shared(s"$id")

  var thread: Option[Future[Done]] = None

  /**
    * Callback method to run things before process execution.
    */
  def beforeProcess() = {}

  def execute(): Future[Done] = {
    notify(NotificationValues.STARTUP)
    beforeProcess()
    thread = Some(process)
    thread.get.onComplete {
      case Success(_) => afterProcess()
      case Failure(exception) => logger.error(s"Error on $serviceName", exception)
    }
    thread.get
  }

  /**
    * Callback method to run things after process execution.
    */
  def afterProcess() = {}

  /**
    * Method where the  to run things after process execution.
    */
  def process(): Future[Done]

  def terminate() = {
    notify(NotificationValues.TERMINATE)
    jobSystem.terminate()
    jobSystem.whenTerminated
  }

  def init() = {}

  def shutdown() = close()

  def close() = {
    notify(NotificationValues.SHUTDOWN)
    killSwitch.shutdown()
    thread.get
  }

  def abort() = {
    notify(NotificationValues.ABORT)
    killSwitch.abort(JobAbortedException("Job was aborted by hosting process."))
    thread.get.failed
  }

  /**
    * This method is used to provide a progress bar like display.
    * Usefull when the job has a known number of items to process and is able to give a percentage of its own progression
    * @param processed
    * @param total
    * @param message
    */
  def progress(processedItems:Int, totalItems:Option[Int], message:Option[String]=None) = {
    notifyProgress(processedItems, totalItems, message)
  }

  def update(message:String) = {
    notify(NotificationValues.UPDATE, Some(message))
  }

  jobSystem.registerOnTermination{
    notify(NotificationValues.TERMINATE)
  }
}
