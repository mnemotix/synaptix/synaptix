/**
  * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.jc.tasks.listeners

import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.amqp.api.rabbitmq.{RabbitMQClient, RabbitMQProducer}
import com.mnemotix.synaptix.core.ServiceListener
import com.mnemotix.synaptix.jobs.api.SynaptixJob
import com.rabbitmq.client.{AMQP, Channel, Connection}
import play.api.libs.json.{JsNumber, JsString, JsValue, Json}

class AmqpListener(job: SynaptixJob, correlationId: Option[String], replyTo: Option[String], exchangeName: String = "default") extends ServiceListener {

  val producer = new RabbitMQProducer {
    override implicit val client: RabbitMQClient = new RabbitMQClient()

    override implicit val conn: Connection = client.getConnection()
    override implicit val channel: Channel = client.createChannel()

    override def init(): Unit = {}
  }

  val replyProps = new AMQP.BasicProperties()
    .builder()
    .correlationId(correlationId.getOrElse(""))
    .replyTo(replyTo.getOrElse(""))
    .build()
//    messages.foreach(msg => channel.basicPublish(exchangeName, topic, props, msg.toString.getBytes("UTF-8")))
  override def onStartup(): Unit = {
    val message = getResponseMessage(JsString(s"${job.serviceName} has started"),"JSON", "started")
    producer.publish(replyTo.getOrElse(""), "", Vector(message), replyProps)
  }

  override def onTerminate(): Unit = {
    val message = getResponseMessage(JsString(s"Termination signal was sent to ${job.serviceName}"),"JSON", "terminated")
    producer.publish(replyTo.getOrElse(""), "", Vector(message), replyProps)
  }

  override def onShutdown(): Unit = {
    logger.warn(s"Shutdown signal was sent to ${job.serviceName}")
    val message = getResponseMessage(JsString(s"Shutdown signal was sent to ${job.serviceName}"),"JSON", "shutdown")
    producer.publish(replyTo.getOrElse(""), "", Vector(message), replyProps)
  }

  override def onAbort(): Unit = {
    logger.warn(s"Abortion signal was sent to ${job.jobSystem}")
    val message = getResponseMessage(JsString(s"Abortion signal was sent to ${job.jobSystem}"),"JSON", "aborted")
    producer.publish(replyTo.getOrElse(""), "", Vector(message), replyProps)
  }

  override def onUpdate(message: String): Unit = {
    print("\r" + message)
    val amqpmessage = getResponseMessage(JsString(s"${"\r" + message}"),"JSON", "updated")
    producer.publish(replyTo.getOrElse(""), "", Vector(amqpmessage), replyProps)
  }

  override def onProgress(processedItems: Int, totalItems: Option[Int], message: Option[String]): Unit = {
    val json =  Json.toJson(UpdateMsg(processedItems, totalItems, message))
    val amqpmessage = getResponseMessage(json,"JSON", "updated")
    producer.publish(replyTo.getOrElse(""), "", Vector(amqpmessage), replyProps)
  }

  override def onDone(): Unit = {
    val amqpmessage = getResponseMessage(JsString(s"${job.serviceName} has been completed."),"JSON", "done")
    producer.publish(replyTo.getOrElse(""), "", Vector(amqpmessage), replyProps)
  }

  override def onError(errorMessage: String, cause: Option[Throwable]): Unit = {
    val amqpmessage = getErrorMessage(cause.getOrElse(null))
    producer.publish(replyTo.getOrElse(""), "", Vector(amqpmessage), replyProps)
  }

  def getResponseMessage(body: JsValue, format: String, status: String): AmqpMessage = {
    val head = Map(
      "sender" -> JsString(job.id),
      "service" -> JsString(job.serviceName),
      "timestamp" -> JsNumber(System.currentTimeMillis()),
      "format" -> JsString(format),
      "status" -> JsString(status)
    )
    AmqpMessage(head, body, correlationId: Option[String], replyTo: Option[String])
  }

  def getErrorMessage(err: Throwable): AmqpMessage = {
    val body = Json.obj(
      "errorClass" -> JsString(err.getClass.getSimpleName),
      "errorMessage" -> JsString(err.getMessage)
    )
    getResponseMessage(body, "JSON", "ERROR")
  }
}
