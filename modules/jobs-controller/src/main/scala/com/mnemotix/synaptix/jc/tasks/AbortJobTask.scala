/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jc.tasks

import akka.actor.ActorSystem
import akka.stream.KillSwitches
import com.mnemotix.amqp.api.models.{AmqpMessage, MessageProcessResult, OkMessage}
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.synaptix.jobs.JobsRegistry
import com.mnemotix.synaptix.jobs.exceptions.{JobAbortedException, JobNotFound}
import play.api.libs.json.JsBoolean

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 12/07/2021
  */

class AbortJobTask(override val topic: String, override val exchangeName: String)(implicit override val client: RabbitMQClient, override val system: ActorSystem, override val ec: ExecutionContext) extends JobsControllerTask {
  override def processMessage(msg: AmqpMessage, context: Option[Seq[String]]): Future[MessageProcessResult] = {
    val jobId = msg.body.as[String]
    JobsRegistry.get(jobId).map(job => job.abort()).getOrElse(throw JobNotFound(s"Unknown Job ID : $jobId")).map(_ => OkMessage(JsBoolean(true)))
  }
}
