/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.fake.ns

import akka.Done
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.synaptix.jobs.api.SynaptixJob

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.Future

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 18/08/2021
  */

class FakeJobImpl extends SynaptixJob {

  override val prefix: String = "fake"
  override val label: String = "Fake Job Implementation for testing purpose"


  override def process(): Future[Done] = {
    val j: AtomicInteger = new AtomicInteger(0)
    thread = Some(Source(1 to 10000)
      .via(killSwitch.flow)
      .via(Flow[Int].map{ i:Int =>
        val processed = j.incrementAndGet()
        notifyProgress(processed, Some(10000), Some(s"We processed $j / 10000"))
        //logger.debug(i + " : " + args.mkString(";"))
      })
      .runWith(Sink.ignore))
    thread.get
  }
//  override val monitors: mutable.Map[String, JobMonitor] = ???
}
