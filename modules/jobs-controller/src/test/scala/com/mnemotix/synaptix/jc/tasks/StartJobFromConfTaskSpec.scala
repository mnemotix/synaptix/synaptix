/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jc.tasks

import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.synaptix.jc.JobsControllerTestSpec
import play.api.libs.json.Json

import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 18/08/2021
  */

class StartJobFromConfTaskSpec extends JobsControllerTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  "StartJobTask" should {
    "start a job on an incoming message containing the jobid" in {
      val task = new StartJobFromConfTask("job.confstart", AmqpClientConfiguration.exchangeName)
      val jsConf =
        """
          |{
          |  "id" : "fake",
          |  "qname" : "com.fake.ns.FakeJobImpl",
          |  "args" : {
          |    "arg1" : {
          |      "label" : "arg1",
          |      "value" : 12
          |    },
          |    "arg2" : {
          |      "label" : "arg2",
          |      "value" : "my string"
          |    },
          |    "arg3" : {
          |      "label" : "arg3",
          |      "value" : [ "my", "list", "of", "arguments" ]
          |    }
          |  }
          |}
          |""".stripMargin

      val message = AmqpMessage(Map.empty, Json.parse(jsConf))
      val resultMessage = task.processMessage(message, None).futureValue
      println(resultMessage)
    }
  }
}

