/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic.tasks

import com.mnemotix.amqp.api._
import com.mnemotix.amqp.api.models.{AmqpMessage, MessageProcessResult}
import com.mnemotix.synaptix.IndexControllerTestSpec
import play.api.libs.json._

import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-15
  */

class IndexQueryStringTaskSpec extends IndexControllerTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val task = new IndexQueryStringTask("index.query", AmqpClientConfiguration.exchangeName)
  val indices: Seq[String] = Seq("person-1612541542152")
  val topic = "index.search"

  def headers(topic: String) = Map[String, JsValue](
    "routing.key" -> JsString(topic),
    "indices" -> JsArray(indices.map(v => JsString(v)))
  )

  def msg(topic: String, qryStr: String): AmqpMessage = AmqpMessage(
    headers = headers(topic),
    body = JsString(qryStr)
  )

//  IndexHelper.bootstrapIndex()

  "IndexQueryStringTask" should {
    "execute a search query" in {
      val qryStr = "email:\"person1@fake.com\""
      val amqpQuery = msg(topic, qryStr)
      val resultMessage: MessageProcessResult = task.processMessage(amqpQuery).futureValue
      println(Json.prettyPrint(resultMessage.content))
      //      val msg = resultMessage.content.as[AmqpMessage]
      //      (msg.body \ "hits" \ "total").as[Int] shouldEqual 1
      //      val jsperson = (msg.body \ "hits" \ "hits" \\ "_source")(0).as[JsValue]
      //      println(Json.prettyPrint(jsperson))
      //      (jsperson \ "fullName") .as[String] shouldEqual "John Doe"
    }
  }
}
