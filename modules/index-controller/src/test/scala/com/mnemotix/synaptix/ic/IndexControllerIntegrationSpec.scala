/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic

import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.amqp.api.rabbitmq.{RabbitMQClient, RabbitMQConsumer, RabbitMQProducer, RabbitMQRPCCall}
import com.mnemotix.synaptix.IndexControllerTestSpec
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.rabbitmq.client.{AMQP, Channel, Connection, Envelope}
import play.api.libs.json.{JsArray, JsNumber, JsString, JsValue, Json}

import scala.collection.mutable
import scala.concurrent.duration._
import scala.util.Random

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 05/02/2021
  */

class IndexControllerIntegrationSpec extends IndexControllerTestSpec {

  override implicit val patienceConfig = PatienceConfig(Duration.Inf)

  val exchangeName = "default"
  val topic = "index.search"
  val indices: Seq[String] = Seq[String]("person-1612541542152")

  implicit val rabbit = new RabbitMQClient()

  implicit val conn: Connection = rabbit.getConnection()
  implicit val channel: Channel = rabbit.createChannel()

  val replyBuffer = mutable.Buffer[String]()

  val nbMessages = 1
  val delay = 50

//  val random = new Random
//  val nbVect = 10
//  val vectSize = 100
//    val producer = new AmqpRpcTopicProducer(exchange, "Index Controller Integration Test") {
//      override def beforePublish(topic: String, messages: Vector[AmqpMessage]): Unit = {
//        println(s"Messages sent:${messages.map(m => Json.stringify(Json.toJson(m))).mkString("\nMessages sent:")}")
//      }
//      override def onReply(msg: ByteString): Unit = {
//        println(msg.utf8String)
//        println(Json.prettyPrint(Json.parse(msg.utf8String)))
//        replyBuffer += msg.utf8String
//      }
//    }

  var messageOK:Boolean = false

  val rpc = new RabbitMQRPCCall(topic, exchangeName) {
    override def onReply(message: AmqpMessage): Unit = {
      println(s"onReply : ${message.toString}")
      messageOK = true
      replyBuffer += message.toString
    }
  }

  def headers(topic: String) = Map[String, JsValue](
    "routing.key" -> JsString(topic),
    "indices" -> JsArray(indices.map(v => JsString(v)))
  )

  def msg(topic: String): AmqpMessage = AmqpMessage(
    headers = headers(topic),
    body = Json.toJsObject(RawQuery(
      indices = indices,
      source = Json.obj("query" -> Json.obj("match_all" -> Json.obj()))
    ))
  )

  def messages(nb: Int = 100) = {
    //    val headers = Map.empty[String, JsValue]
    (1 to nb).map(i => msg(topic)).toVector
  }

  //  def malformedQueryMessage(topic: String) = AmqpMessage(
  //    headers = headers(topic),
  //    body = Json.toJsObject(RawQuery(
  //      indices = indices,
  //      source = Json.obj("query" -> Json.obj("mach_all" -> Json.obj()))
  //    ))
  //  )

  //  def wrongHeaderMessage(topic: String) = AmqpMessage(
  //    headers = Map[String, JsValue](
  //      "routing.key" -> JsString(topic),
  //      "indices" -> JsString("person-1612541542152")
  //    ),
  //    body = Json.toJsObject(RawQuery(
  //      indices = indices,
  //      source = Json.obj("query" -> Json.obj("match_all" -> Json.obj()))
  //    ))
  //  )

  "IndexController" should {
    "start listening on its reply queue" in {
      rpc.init()
      rpc.start()
    }
    "execute an RPC call" in {
      (1 to nbMessages).foreach { i =>
        rpc.call(messages(1))
//        Thread.sleep(delay)
      }

      //      val thread = controller.start()
      //      try {
      //        thread.futureValue
      //      } catch {
      //        case _:TestFailedException => logger.info("No exception before timeout, everything's OK...")
      //        case t:Throwable =>
      //          logger.error("Oops! Something went wrong.", t)
      //          println(t.getClass.getName)
      //          t.printStackTrace()
      //      }

      //      Thread.sleep(5000)
    }
    "close connection & channel to broker" in {
      while(!messageOK){}
      replyBuffer.size shouldEqual 100
      // Send the termination signal
      rpc.shutdown()
    }
  }
}
