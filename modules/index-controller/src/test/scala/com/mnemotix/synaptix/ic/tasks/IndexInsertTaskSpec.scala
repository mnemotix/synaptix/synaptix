/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic.tasks

import com.mnemotix.amqp.api._
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.synaptix.{Fixtures, IndexControllerTestSpec, IndexHelper}

import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-15
  */


class IndexInsertTaskSpec extends IndexControllerTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val task = new IndexInsertDocTask("index.insert", AmqpClientConfiguration.exchangeName)
  IndexHelper.bootstrapIndex(true, false)

  "IndexInsertTask" should {
    "insert a document" in {
      val indexable = Fixtures.getPerson("person:0", "John Doe", "john.doe@fake.com")
      val message = AmqpMessage(Map.empty, indexable.toJson())
//      val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
//      val msg = Json.parse(resultMessage.bytes.utf8String).as[AmqpMessage]
//      println(msg)
    }
  }
}
