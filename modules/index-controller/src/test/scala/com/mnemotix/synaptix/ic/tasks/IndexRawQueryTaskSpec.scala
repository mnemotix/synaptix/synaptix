/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic.tasks

import com.mnemotix.amqp.api._
import com.mnemotix.amqp.api.models.{AmqpMessage, MessageProcessResult}
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.mnemotix.synaptix.{IndexControllerTestSpec, IndexHelper}
import play.api.libs.json._

import scala.concurrent.duration._


/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-15
  */

class IndexRawQueryTaskSpec extends IndexControllerTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val task = new IndexSearchTask("index.search", AmqpClientConfiguration.exchangeName)
  val indices:Seq[String] = Seq("person-1612541542152")
  val topic = "index.search"
  val qryStr = "email:\"john.doe@fake.com\""

  def headers(topic: String) = Map[String, JsValue](
    "routing.key" -> JsString(topic),
    "indices" -> JsArray(indices.map(v => JsString(v)))
  )

  def msg(topic: String):AmqpMessage = AmqpMessage(
    headers = headers(topic),
    body = Json.toJsObject(RawQuery(
      indices= indices,
      source= Json.obj("query" -> Json.obj("match_all" -> Json.obj()))
    ))
  )


  //  IndexHelper.bootstrapIndex()

  "IndexRawQueryTask" should {
    "execute a raw query" in {
      val amqpQuery = msg(topic)
      println(Json.prettyPrint(amqpQuery.toJson()))

      val resultMessage:MessageProcessResult = task.processMessage(amqpQuery).futureValue
      println(Json.prettyPrint(resultMessage.content))

//      val qry =
//        """
//          |{
//          | "query": {
//          |  "match_all": {}
//          | },
//          | "_source": {
//          |  "includes": "*"
//          | },
//          | "from": 0,
//          | "size": 11,
//          | "sort": [
//          |   {
//          |   "birthdate": "asc"
//          |  },
//          |  "_score"
//          | ]
//          |}
//          |""".stripMargin
//      val rq = RawQuery(Seq(IndexHelper.indexName), Json.parse(qry).as[JsObject])
//      println(Json.prettyPrint(Json.toJson(rq)))
//      val message = AmqpMessage(Map.empty, Json.toJson(rq))
//      val resultMessage = task.onMessage(message.toReadResult()).futureValue
//      val msg = Json.parse(resultMessage.bytes.utf8String)
//      println(Json.prettyPrint(msg))
//      val parsing = msg.validate[AmqpMessage]
//      parsing.isSuccess shouldBe true
//      (parsing.get.body \ "hits" \ "total").as[Int] shouldEqual 1
//      val jsperson = (parsing.get.body \ "hits" \ "hits" \\ "_source") (0).as[JsValue]
//      println(Json.prettyPrint(jsperson))
//      (jsperson \ "nickName").as[String] shouldEqual "mrogelja"
    }
  }
}
