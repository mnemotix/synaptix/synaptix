/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix

import akka.dispatch.ExecutionContexts
import com.mnemotix.synaptix.index.elasticsearch.ESClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import org.scalatest.concurrent.ScalaFutures
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext
import scala.util.Try

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-05-15
  */

object IndexHelper extends ScalaFutures {

  implicit lazy val ec: ExecutionContext = ExecutionContexts.global()

  val indexName = "person-" + System.currentTimeMillis

  lazy val client = new ESClient

  def init() = if (!client.connected) client.init()

  def dropIndexIfExists() = {
    init()
    Try(client.deleteIndex(indexName).futureValue)
  }

  def createIndexWithDefaultMapping() = {
    val mappngDef = ESMappingDefinitions(
      Json.parse(
        """
          |{
          |    "properties": {
          |      "bio": {
          |        "type": "text",
          |        "analyzer": "standard"
          |      },
          |      "birthdate": {
          |        "type": "date"
          |      },
          |      "email": {
          |        "type": "keyword"
          |      },
          |      "fullName": {
          |        "type": "text",
          |        "analyzer": "standard"
          |      }
          |    }
          |  }
          |""".stripMargin)
    )

    init()
    client.createIndex(indexName, mappngDef.toMappingDefinition())
  }

  def bootstrapIndex(deleteIfExists: Boolean = false, loadData:Boolean = true) = {
    init()
    if (!client.indexExists(indexName).futureValue.result.exists || deleteIfExists) {
      dropIndexIfExists()
      createIndexWithDefaultMapping().futureValue
      Thread.sleep(2000)
      if(loadData){
        client.insert(indexName, Fixtures.getPerson("person:0", "John Doe", "john.doe@fake.com"))
        client.bulkInsert(indexName, Fixtures.getPersons(200): _*)
        Thread.sleep(2000)
      }
    }
  }
}
