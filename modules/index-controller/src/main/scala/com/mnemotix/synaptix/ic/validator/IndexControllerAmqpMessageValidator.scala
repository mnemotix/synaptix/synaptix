/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.ic.validator

import com.mnemotix.amqp.api.exceptions.{MalformedBodyException, MessageValidationException, MissingHeaderException}
import com.mnemotix.amqp.api.models.AmqpMessage
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{Json, Reads}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 12/02/2021
 */

object IndexControllerAmqpMessageValidator extends LazyLogging {

  @throws(classOf[MessageValidationException])
  def validate[T](message: AmqpMessage)(implicit reads:Reads[T]): (Seq[String], T) = {
    val indices: Seq[String] = message.headers.get("indices").map(_.as[Seq[String]]).getOrElse {
      throw MissingHeaderException(s"""Index name not found.\nAt least one index name should be given into the "indices" header.""")
    }
    if(indices.size == 0) throw new IllegalArgumentException(s"""Index name not found.\nAt least one index name should be given into the "indices" header.""")
    if(message.body.validate[T].isSuccess){
      val indexable = message.body.as[T]
      (indices, indexable)
    }
    else throw MalformedBodyException(s"""Message body was malformed. ${Json.prettyPrint(message.body)}""")
  }

}
