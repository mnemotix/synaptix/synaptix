/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ic.tasks

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.SynaptixRPCTask
import com.mnemotix.amqp.api.exceptions.MissingHeaderException
import com.mnemotix.amqp.api.models.{AmqpMessage, ErrorMessage, MessageProcessResult, OkMessage}
import com.mnemotix.synaptix.ic.validator.IndexControllerAmqpMessageValidator
import com.mnemotix.synaptix.index.elasticsearch.models.ESSearchResponse
import com.sksamuel.elastic4s.Response
import play.api.libs.json.{JsString, JsValue}

import scala.concurrent.ExecutionContext

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 12/02/2021
  */

trait IndexControllerTask extends SynaptixRPCTask[Seq[String]] {

  val system: ActorSystem
  val ec: ExecutionContext

  override def beforeProcess(msg: AmqpMessage): Option[Seq[String]] = {
    if(isVerbose) logger.debug(s"AMQP Message received : ${msg.toString}")
    val (indices: Seq[String], _) = IndexControllerAmqpMessageValidator.validate[JsValue](msg)
    Some(indices)
  }

  def getIndexName(context: Option[Seq[String]]):String = {
    if(context.isDefined) context.get.head
    else throw MissingHeaderException(s"""Index name not found.\nAt least one index name should be given into the "indices" header.""")
  }

  def mapESClientResponseToString(resp: Response[_]): MessageProcessResult = {
    if (resp.isError) {
      logger.warn(s"""Server returned an error : ${resp.error.`type`} ${resp.error.reason}""")
      ErrorMessage(resp.error.asException)
    }
    else {
      OkMessage(JsString(resp.body.getOrElse("")))
    }
  }

  def mapESClientResponseToJson(resp: Response[_]): MessageProcessResult = {
    if (resp.isError) {
      logger.warn(s"""Server returned an error : ${resp.error.`type`} ${resp.error.reason}""")
      ErrorMessage(resp.error.asException)
    }
    else {
      OkMessage(ESSearchResponse(resp.body).toJson)
    }
  }
}
