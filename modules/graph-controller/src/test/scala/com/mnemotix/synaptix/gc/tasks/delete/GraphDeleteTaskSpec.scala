/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.gc.tasks.delete

import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.synaptix.GraphControllerTestSpec
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.{JsBoolean, JsString}

import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-04-15
  */

class GraphDeleteTaskSpec extends GraphControllerTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  "GraphDeleteTask" should {
    val task = new GraphDeleteTask("graph.delete.triples", AmqpClientConfiguration.exchangeName)
    RDFClient.init()
    "execute a delete query" in {
      val qryStr ="""PREFIX dc: <http://purl.org/dc/elements/1.1/>
          |
          |DELETE DATA
          |{
          |  <http://example/book2> dc:title "David Copperfield" ;
          |                         dc:creator "Edmund Wells" .
          |}
          |""".stripMargin
      val message = AmqpMessage(Map.empty, JsString(qryStr))
      val context = task.beforeProcess(message)
      val future = task.processMessage(message, context)
      future.onComplete {
        case Success(_) => task.afterProcess(context)
        case Failure(err) => err.printStackTrace()
      }
      val resultMessage = future.futureValue
      val msg = resultMessage.content
      msg shouldBe JsBoolean(true)
    }

    "send an error message on a malformed query" in {
      val qryStr ="""PREFIX dc: <http://purl.org/dc/elements/1.1/>
                    |
                    |DELEDATA
                    |{
                    |  <http://example/book2> dc:title "David Copperfield" ;
                    |                         dc:creator "Edmund Wells" .
                    |}
                    |""".stripMargin
//      val message = AmqpMessage(Map.empty, JsString(qryStr))
//      val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
//      println(resultMessage.bytes.utf8String)
//      val msg = Json.parse(resultMessage.bytes.utf8String).as[AmqpMessage]
//      msg.headers("status") shouldEqual JsString("ERROR")
    }
  }
}
