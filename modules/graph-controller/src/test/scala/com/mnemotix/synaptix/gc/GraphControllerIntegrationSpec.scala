/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gc

import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.amqp.api.rabbitmq.RabbitMQProducer
import com.mnemotix.synaptix.GraphControllerTestSpec
import play.api.libs.json.{JsArray, JsString, JsValue, Json}

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Random

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/02/2021
 */

class GraphControllerIntegrationSpec extends GraphControllerTestSpec {

  override implicit val patienceConfig:PatienceConfig = PatienceConfig(60.seconds)

  override protected def afterAll(): Unit = {
//    producer.stop()
    Await.result(system.terminate(), Duration.Inf)
  }

  val exchange = "default"
  val topic = "graph.select"
  val repositories = Seq[String]("test")
  val replyBuffer = mutable.Buffer[String]()

//  val producer = new RabbitMQProducer(topic, exchange)
  //    override def beforePublish(topic: String, messages: Vector[AmqpMessage]): Unit = {
//      println(s"Messages sent:${messages.map(m => Json.stringify(Json.toJson(m))).mkString("\nMessages sent:")}")
//    }
//    override def onReply(msg: ByteString): Unit = {
//      println(msg.utf8String)
//      //        println(Json.prettyPrint(Json.parse(msg.utf8String)))
//      replyBuffer += msg.utf8String
//    }

  def headers(topic: String) = Map[String, JsValue](
    "routing.key" -> JsString(topic),
    "repositories" -> JsArray(repositories.map(v => JsString(v)))
  )

  def wrongHeaders(topic: String) = Map[String, JsValue](
    "routing.key" -> JsString(topic),
    "repository" -> JsArray(repositories.map(v => JsString(v)))
  )

  def query(limit: Int) = JsString(s"""SELECT * WHERE {?p ?r ?q} LIMIT ${limit}""")

  def malformedQuery(limit: Int) = JsString(s"""SELCT * WHERE {?p ?r ?q} LIMIT ${limit}""")

  val random = new Random
  val nbVect = 5
  val vectSize = 1

  def okMessage(topic: String) = AmqpMessage(
    headers = headers(topic),
    body = query(random.nextInt(100))
  )

  def malformedQueryMessage(topic: String) = AmqpMessage(
    headers = headers(topic),
    body = malformedQuery(random.nextInt(100))
  )

  def wrongHeaderMessage(topic: String) = AmqpMessage(
    headers = wrongHeaders(topic),
    body = query(random.nextInt(100))
  )

//  def publish(topic: String, vectorSize:Int) = producer.publish(topic, (1 to vectorSize).map { i => okMessage(topic) }.toVector)

  "IndexController" should {

    "process AMQP messages" in {
      val start = System.currentTimeMillis()
//      val futures:Seq[Future[Done]] = (1 to nbVect ).map(_ => publish(topic, vectSize))
//      val isOk: Seq[Done] = Future.sequence(futures).futureValue
//      isOk shouldEqual Done
      val duration = System.currentTimeMillis() - start
      val messagesSent = nbVect * vectSize
      val messagesReceived = replyBuffer.size
      println(s"$messagesReceived/$messagesSent messages processed in $duration ms ")
      messagesReceived shouldEqual messagesSent
    }

    "send error on malformed queries" in {
//      val isOk:Done = producer.publish(topic, Seq(malformedQueryMessage(topic)).toVector).futureValue
//      isOk shouldEqual Done
      val messageReceived = Json.parse(replyBuffer.head).as[AmqpMessage]
      messageReceived.headers("status").as[String] shouldEqual "ERROR"
    }

    "send error on wrong headers" in {
//      val isOk:Done = producer.publish(topic, Seq(wrongHeaderMessage(topic)).toVector).futureValue
//      isOk shouldEqual Done
      val messageReceived = Json.parse(replyBuffer.head).as[AmqpMessage]
      messageReceived.headers("status").as[String] shouldEqual "ERROR"
    }
  }
}
