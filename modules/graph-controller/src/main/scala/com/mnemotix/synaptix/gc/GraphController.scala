/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.gc

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.amqp.api.{AmqpClientConfiguration, SynaptixRPCController}
import com.mnemotix.synaptix.gc.tasks.create.GraphCreateFromJsonLdTask
import com.mnemotix.synaptix.gc.tasks.delete.GraphDeleteTask
import com.mnemotix.synaptix.gc.tasks.read.{GraphAskTask, GraphConstructTask, GraphDescribeTask, GraphSelectTask}
import com.mnemotix.synaptix.gc.tasks.update.GraphUpdateTask
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-12
  */

object GraphController extends App with LazyLogging {

  implicit val system = ActorSystem("GraphControllerSystem")
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val client: RabbitMQClient = new RabbitMQClient

  val controller = new SynaptixRPCController("Graph Controller")

  logger.info("Graph controller starting...")

  RDFClient.mute()

  RDFClient.init()

  RDFClient.verbose()

  controller.registerTask("graph.create.triples", new GraphCreateFromJsonLdTask("graph.create.triples", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.insert.triples", new GraphUpdateTask("graph.insert.triples", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.select", new GraphSelectTask("graph.select", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.construct", new GraphConstructTask("graph.construct", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.ask", new GraphAskTask("graph.ask", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.describe", new GraphDescribeTask("graph.describe", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.update.triples", new GraphUpdateTask("graph.update.triples", AmqpClientConfiguration.exchangeName))
  controller.registerTask("graph.delete.triples", new GraphDeleteTask("graph.delete.triples", AmqpClientConfiguration.exchangeName))

  controller.start()

  sys addShutdownHook {
    logger.info("Terminating...")
    controller.shutdown
    // Terminate ActorSystem
    Await.result(system.terminate, Duration.Inf)
    logger.info("Terminated... Bye")
  }
}
