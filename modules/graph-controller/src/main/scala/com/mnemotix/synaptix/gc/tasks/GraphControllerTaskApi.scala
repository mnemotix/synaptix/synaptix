/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gc.tasks

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.SynaptixRPCTask
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.synaptix.gc.validator.GraphControllerAmqpMessageValidator
import com.mnemotix.synaptix.rdf.client.models.{RDFClientConnection, RDFClientReadConnection, RDFClientWriteConnection}
import com.mnemotix.synaptix.rdf.client.{RDFClient, RDFClientConnectException}

import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 12/02/2021
 */

trait GraphControllerTask[T <: RDFClientConnection] extends SynaptixRPCTask[T] {

  val system: ActorSystem
  val ec: ExecutionContext

  override def beforeProcess(msg: AmqpMessage): Option[T] = {
    if(isVerbose) logger.debug(s"AMQP Message received : ${msg.toString}")
    val (repo, _) = GraphControllerAmqpMessageValidator.validate(msg)
    val conn: Option[T] = getDBConnection(repo.head)
    conn
  }

  override def afterProcess(context: Option[T]): Unit = {
    context.map(c => if (c.get.isOpen) c.close())
  }

  def getDBConnection(repository: String): Option[T]

  def checkConnection(connection: Option[T]): T = {
    if (connection.isDefined) {
      if (connection.get.get.isOpen) {
        connection.get
      }
      else {
        logger.error("Database connection was closed, trying to reconnect...")
        Thread.sleep(1000)
        checkConnection(getDBConnection(connection.get.repositoryName))
      }
    }
    else {
      throw RDFClientConnectException("Database connection handler is empty", null)
    }
  }
}

abstract class GraphControllerReadingTask()(implicit override val system: ActorSystem, override val ec: ExecutionContext) extends GraphControllerTask[RDFClientReadConnection] {
  override def getDBConnection(repository: String): Option[RDFClientReadConnection] = Some(RDFClient.getReadConnection(repository))
}

abstract class GraphControllerWritingTask()(implicit override val system: ActorSystem, override val ec: ExecutionContext) extends GraphControllerTask[RDFClientWriteConnection] {
  override def getDBConnection(repository: String): Option[RDFClientWriteConnection] = Some(RDFClient.getWriteConnection(repository))
}
