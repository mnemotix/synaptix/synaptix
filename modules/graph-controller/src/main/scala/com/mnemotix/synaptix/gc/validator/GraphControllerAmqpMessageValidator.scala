/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.gc.validator

import com.mnemotix.amqp.api.exceptions.{MalformedBodyException, MessageValidationException, MissingHeaderException}
import com.mnemotix.amqp.api.models.AmqpMessage

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 12/02/2021
 */

object GraphControllerAmqpMessageValidator {

  @throws(classOf[MessageValidationException])
  def validate(message: AmqpMessage): (Seq[String], String) = {
    val repo = message.headers.get("repositories").map(_.as[Seq[String]]).getOrElse {
      throw MissingHeaderException("The message should contain a \"repositories\" field header to specify repositories the query should be executed into.")
    }
    val qry = message.body.as[String]
    if(qry.length == 0) throw MalformedBodyException("The message body should not be empty.")
    (repo, qry)
  }

}
