/**
  * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package com.mnemotix.synaptix.gc.tasks.read

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.models.{AmqpMessage, MessageProcessResult, OkMessage}
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.synaptix.gc.tasks.GraphControllerReadingTask
import com.mnemotix.synaptix.gc.validator.GraphControllerAmqpMessageValidator
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFModel}
import com.mnemotix.synaptix.rdf.client.{RDFClient, RDFFormats}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-12
  */

class GraphDescribeTask(override val topic: String, override val exchangeName: String)(implicit override val client:RabbitMQClient, override val system: ActorSystem, override val ec: ExecutionContext) extends GraphControllerReadingTask {

  override def processMessage(msg: AmqpMessage, context: Option[RDFClientReadConnection]): Future[MessageProcessResult] = {
    val (_, qry) = GraphControllerAmqpMessageValidator.validate(msg)
    RDFClient.describe(qry)(ec, checkConnection(context)).map { m: RDFModel => OkMessage(m.toJsonLd(), RDFFormats.JSONLD.name)}
  }

}