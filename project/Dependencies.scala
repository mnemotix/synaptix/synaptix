import sbt._

object Version {
  lazy val scalaVersion = "2.13.7"
  lazy val scalaTest = "3.2.9"
  lazy val scalaLogging = "3.9.4"
  lazy val logback = "1.2.5"
  lazy val jodaTime = "2.10.10"

  lazy val tsConfig = "1.4.1"
  lazy val akkaVersion = "2.6.15"
  lazy val playJson = "2.9.2"

  lazy val elastic4sVersion = "7.13.0"
  lazy val rdf4jVersion = "3.7.2"
  lazy val sttp = "1.7.2"
//  lazy val sttp = "3.1.7"
  lazy val scalaXml = "2.0.0-M2"
  lazy val amqpClient = "5.13.0"
  lazy val commonsCompress = "1.21"
  lazy val akkaHttp = "10.2.6"
  lazy val rocksdb = "8.5.3"
  lazy val jsoup = "1.15.3"

  lazy val sangria = "3.3.0"
  lazy val sangriaSpray = "1.0.3"
  lazy val sangriaRelay = "3.0.0"
  lazy val scalaz = "7.3.6"


}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val scalaCollectionCompat = "org.scala-lang.modules" %% "scala-collection-compat" % "2.8.1"
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val typesafeConfig = "com.typesafe" % "config" % Version.tsConfig

  lazy val jodaTime = "joda-time" % "joda-time" % Version.jodaTime
  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % Version.akkaVersion
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akkaVersion
  lazy val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Version.akkaVersion
  lazy val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % Version.akkaVersion

  lazy val playJson = "com.typesafe.play" %% "play-json" % Version.playJson
  lazy val amqpClient = "com.rabbitmq" % "amqp-client" % Version.amqpClient

  lazy val commonsCompress = "org.apache.commons" % "commons-compress" % Version.commonsCompress

  // elastic4s
  lazy val elastic4sCore = "com.sksamuel.elastic4s" %% "elastic4s-core" % Version.elastic4sVersion
  lazy val elastic4sClient = "com.sksamuel.elastic4s" %% "elastic4s-client-esjava" % Version.elastic4sVersion
  lazy val elastic4sHttpStreams = "com.sksamuel.elastic4s" %% "elastic4s-http-streams" % Version.elastic4sVersion excludeAll(ExclusionRule(organization = "com.typesafe.akka"))
  lazy val elastic4sTestkit = "com.sksamuel.elastic4s" %% "elastic4s-testkit" % Version.elastic4sVersion

  lazy val sttpCore = "com.softwaremill.sttp" %% "core" % Version.sttp
  // https://mvnrepository.com/artifact/com.softwaremill.sttp.client3/core
//  lazy val sttpCore = "com.softwaremill.sttp.client3" %% "core" % Version.sttp

  lazy val rdf4jRuntime = "org.eclipse.rdf4j" % "rdf4j-runtime" % Version.rdf4jVersion pomOnly()
  lazy val rdf4jSparqlBuilder = "org.eclipse.rdf4j" % "rdf4j-sparqlbuilder" % Version.rdf4jVersion
  lazy val scalaXml = "org.scala-lang.modules" %% "scala-xml" % Version.scalaXml

  lazy val akkaHttp = "com.typesafe.akka" %% "akka-http" % Version.akkaHttp
  lazy val rocksdb = "org.rocksdb" % "rocksdbjni" % Version.rocksdb
  lazy val jsoup = "org.jsoup" % "jsoup" % Version.jsoup

  lazy val sangria = "org.sangria-graphql" %% "sangria" % Version.sangria
  lazy val sangriaRelay = "org.sangria-graphql" %% "sangria-relay" % Version.sangriaRelay
  lazy val sangriaSpray = "org.sangria-graphql" %% "sangria-spray-json" % Version.sangriaSpray

}
