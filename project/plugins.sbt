addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.8.0")
addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.10.1")