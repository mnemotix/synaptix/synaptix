ThisBuild / scalaVersion := Version.scalaVersion
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"
ThisBuild / licenses := List("Apache 2" -> new URL("https://www.apache.org/licenses/LICENSE-2.0.txt"))

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / useCoursier := false

ThisBuild / developers := List(
  Developer(
    id = "ndelaforge",
    name = "Nicolas Delaforge",
    email = "nicolas.delaforge@mnemotix.com",
    url = url("https://www.mnemotix.com")
  ),
  Developer(
    id = "prlherisson",
    name = "Pierre-René Lherisson",
    email = "pr.lherisson@mnemotix.com",
    url = url("https://www.mnemotix.com")
  ),
  Developer(
    id = "mrogelja",
    name = "Mathieu Rogelja",
    email = "mathieu.rogelja@mnemotix.com",
    url = url("https://www.mnemotix.com")
  )
)

// Define the plugin's settings(build.sbt):
val glHost        = "gitlab.com"
val glGroup       = sys.env.getOrElse("GROUP_ID", 5299945)
val glGrpRegistry  = s"https://$glHost/api/v4/groups/$glGroup/-/packages/maven"

ThisBuild / resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("public"),
  Resolver.typesafeRepo("releases"),
  Resolver.sbtPluginRepo("releases"),
//  Resolver.bintrayRepo("owner", "repo"),
//  "MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
//  "MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/",
  "gitlab group" at glGrpRegistry
)




