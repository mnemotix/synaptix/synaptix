import Dependencies._
import com.gilcloud.sbt.gitlab.GitlabPlugin
import sbt.Keys._
import sbt.url

lazy val scala212 = "2.12.16"
lazy val supportedScalaVersions = List(Version.scalaVersion, scala212)


lazy val commonSettings = Seq(
  crossScalaVersions := supportedScalaVersions,
  homepage := Some(url("https://gitlab.com/mnemotix/synaptix/synaptix")),
  scmInfo := Some(
    ScmInfo(
      url("https://gitlab.com/mnemotix/synaptix/synaptix.git"),
      "scm:git@gitlab.com:mnemotix/synaptix/synaptix.git"
    )
  ),
  GitlabPlugin.autoImport.gitlabProjectId :=  Some(21727073),  // Project ID
  GitlabPlugin.autoImport.gitlabGroupId   :=  Some(5299945),
  GitlabPlugin.autoImport.gitlabDomain    :=  "gitlab.com",
  credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
  semanticdbEnabled := true,
  semanticdbVersion := scalafixSemanticdb.revision
)

lazy val testDependencies = Seq(
  scalaTest % Test,
  logbackClassic % Test,
  akkaStreamTestkit % Test
)

val meta = """META.INF(.)*""".r

lazy val core = (project in file("modules/core")).settings(
  name := "synaptix-core",
  description := "Synaptix Core Module",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    jodaTime,
    typesafeConfig,
    playJson,
    scalaLogging,
    akkaSlf4j,
    akkaStream,
    commonsCompress,
    scalaCollectionCompat
  )
).enablePlugins(GitlabPlugin)   // Enable the plugin (build.sbt)

lazy val amqp = (project in file("modules/amqp")).settings(
  name := "synaptix-amqp-toolkit",
  description := "Synaptix AMQP Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    amqpClient,
    sttpCore
  )
).dependsOn(core)
  .aggregate(core)
  .enablePlugins(GitlabPlugin)




lazy val indexing = (project in file("modules/indexing")).settings(
  name := "synaptix-indexing-toolkit",
  description := "Synaptix Indexing Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    elastic4sTestkit % Test,
    elastic4sCore,
    elastic4sClient,
    elastic4sHttpStreams
  )
)
  .dependsOn(core)
  .aggregate(core)
  .enablePlugins(GitlabPlugin)

lazy val rdf = (project in file("modules/rdf")).settings(
  name := "synaptix-rdf-toolkit",
  description := "Synaptix RDF Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    logbackClassic,
    rdf4jRuntime,
    sttpCore,
    scalaXml
  )
)
  .dependsOn(core)
  .aggregate(core)
  .enablePlugins(GitlabPlugin)

lazy val http = (project in file("modules/http")).settings(
  name := "synaptix-http-toolkit",
  description := "Synaptix HTTP Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    akkaHttp,
    jsoup
  )
)
  .dependsOn(core, cache)
  .aggregate(core, cache)
  .enablePlugins(GitlabPlugin)

lazy val cache = (project in file("modules/cache")).settings(
  name := "synaptix-cache-toolkit",
  description := "Synaptix Cache Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    rocksdb,
    scalaCollectionCompat
  )
)
  .dependsOn(core)
  .aggregate(core)
  .enablePlugins(GitlabPlugin)

lazy val oai = (project in file("modules/oai")).settings(
  name := "synaptix-oai-toolkit",
  description := "Synaptix OAI Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    logbackClassic,
    rdf4jRuntime,
    sttpCore,
    scalaXml,
    scalaCollectionCompat
  )
)
  .dependsOn(cache)
  .aggregate(cache)
  .enablePlugins(GitlabPlugin)

lazy val jobs = (project in file("modules/jobs")).settings(
  name := "synaptix-jobs-toolkit",
  description := "Synaptix Jobs Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies
)
  .dependsOn(core)
  .aggregate(core)
  .enablePlugins(GitlabPlugin)

lazy val graphql = (project in file("modules/graphql")).settings(
  name := "synaptix-graphql-toolkit",
  description := "Synaptix graphql Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    sangria,
    sangriaRelay,
    sangriaSpray
  )
)
  .dependsOn(core)
  .aggregate(core)
  .enablePlugins(GitlabPlugin)

lazy val `graph-indexing` = (project in file("modules/graph-indexing")).settings(
  name := "synaptix-graph-indexing-toolkit",
  description := "Synaptix Graph Indexing Toolkit",
  commonSettings,
  libraryDependencies ++= testDependencies ++ Seq(
    rdf4jSparqlBuilder
  )
)
  .dependsOn(rdf, indexing)
  .aggregate(rdf, indexing)
  .enablePlugins(GitlabPlugin)

/**
  * DOCKER IMAGES
  */
lazy val `graph-controller` = (project in file("modules/graph-controller")).settings(
  name := "synaptix-graph-controller",
  description := "Synaptix Graph Controller",
  commonSettings,
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
    case n if n.endsWith(".conf") => MergeStrategy.concat
    case n if n.endsWith(".properties") => MergeStrategy.concat
    case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
    case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
    case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case meta(_) => MergeStrategy.discard
    case x => MergeStrategy.first
  },
  mainClass in(Compile, run) := Some("com.mnemotix.synaptix.gc.GraphController"),
  mainClass in assembly := Some("com.mnemotix.synaptix.gc.GraphController"),
  assemblyJarName in assembly := "synaptix-graph-controller.jar",
  libraryDependencies ++= testDependencies ++ Seq(
    logbackClassic
  ),
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("registry.gitlab.com/mnemotix/synaptix"),
      repository = artifact.value.name,
      tag = Some(version.value)
    )
  ),
  buildOptions in docker := BuildOptions(cache = false),
  dockerfile in docker := {
    // The assembly task generates a fat JAR file
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"

    new Dockerfile {
      from("openjdk:11-jre-slim-buster")
      add(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  }
)
  .dependsOn(amqp, `graph-indexing`)
  .aggregate(amqp, `graph-indexing`)
  .enablePlugins(DockerPlugin, GitlabPlugin)

lazy val `index-controller` = (project in file("modules/index-controller")).settings(
  name := "synaptix-index-controller",
  description := "Synaptix Index Controller",
  commonSettings,
  mainClass in(Compile, run) := Some("com.mnemotix.synaptix.ic.IndexController"),
  mainClass in assembly := Some("com.mnemotix.synaptix.ic.IndexController"),
  assemblyJarName in assembly := "synaptix-index-controller.jar",
  libraryDependencies ++= testDependencies ++ Seq(
    logbackClassic,
    elastic4sTestkit % Test
  ),
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
    case n if n.endsWith(".conf") => MergeStrategy.concat
    case n if n.endsWith(".properties") => MergeStrategy.concat
    case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
    case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
    case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case meta(_) => MergeStrategy.discard
    case x => MergeStrategy.first
  },
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("registry.gitlab.com/mnemotix/synaptix"),
      repository = artifact.value.name,
      tag = Some(version.value)
    )
  ),
  buildOptions in docker := BuildOptions(cache = false),
  dockerfile in docker := {
    // The assembly task generates a fat JAR file
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"

    new Dockerfile {
      from("adoptopenjdk/openjdk11:alpine-slim")
      add(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  }
)
  .dependsOn(amqp, indexing)
  .aggregate(amqp, indexing)
  .enablePlugins(DockerPlugin, GitlabPlugin)

lazy val `jobs-controller` = (project in file("modules/jobs-controller")).settings(
  name := "synaptix-jobs-controller",
  description := "Synaptix Jobs Controller",
  commonSettings,
  mainClass in(Compile, run) := Some("com.mnemotix.synaptix.jc.JobsController"),
  mainClass in assembly := Some("com.mnemotix.synaptix.jc.JobsController"),
  assemblyJarName in assembly := "synaptix-jobs-controller.jar",
  libraryDependencies ++= testDependencies ++ Seq(
    logbackClassic
  ),
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
    case n if n.endsWith(".conf") => MergeStrategy.concat
    case n if n.endsWith(".properties") => MergeStrategy.concat
    case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
    case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
    case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case meta(_) => MergeStrategy.discard
    case x => MergeStrategy.first
  },
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("registry.gitlab.com/mnemotix/synaptix"),
      repository = artifact.value.name,
      tag = Some(version.value)
    )
  ),
  buildOptions in docker := BuildOptions(cache = false),
  dockerfile in docker := {
    // The assembly task generates a fat JAR file
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"

    new Dockerfile {
      from("adoptopenjdk/openjdk11:alpine-slim")
      add(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  }
)
  .dependsOn(amqp, jobs)
  .aggregate(amqp, jobs)
  .enablePlugins(DockerPlugin, GitlabPlugin)
